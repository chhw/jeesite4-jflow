/*
Navicat MySQL Data Transfer

Source Server         : SQL
Source Server Version : 50610
Source Host           : localhost:3306
Source Database       : jflow3

Target Server Type    : MYSQL
Target Server Version : 50610
File Encoding         : 65001

Date: 2018-03-31 11:53:58
*/

SET FOREIGN_KEY_CHECKS=0;
 

-- ----------------------------
-- Table structure for `port_dept`
-- ----------------------------
DROP TABLE IF EXISTS `port_dept`;
CREATE TABLE `port_dept` (
  `No` varchar(50) NOT NULL COMMENT '编号 - 主键',
  `Name` varchar(100) DEFAULT NULL COMMENT '名称',
  `NameOfPath` varchar(300) DEFAULT NULL COMMENT '部门路径',
  `ParentNo` varchar(100) DEFAULT NULL COMMENT '父节点编号',
  `TreeNo` varchar(100) DEFAULT NULL COMMENT '树编号',
  `Leader` varchar(100) DEFAULT NULL COMMENT '领导',
  `Tel` varchar(100) DEFAULT NULL COMMENT '联系电话',
  `Idx` int(11) DEFAULT NULL COMMENT 'Idx',
  `IsDir` int(11) DEFAULT NULL COMMENT '是否是目录',
  PRIMARY KEY (`No`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='部门';

-- ----------------------------
-- Records of port_dept
-- ----------------------------
INSERT INTO `port_dept` VALUES ('100', '集团总部', null, '0', null, 'zhoupeng', null, null, '0');
INSERT INTO `port_dept` VALUES ('1001', '集团市场部', null, '100', null, 'zhanghaicheng', null, null, '1');
INSERT INTO `port_dept` VALUES ('1002', '集团研发部', null, '100', null, 'qifenglin', null, null, '1');
INSERT INTO `port_dept` VALUES ('1003', '集团服务部', null, '100', null, 'zhanghaicheng', null, null, '1');
INSERT INTO `port_dept` VALUES ('1004', '集团财务部', null, '100', null, 'yangyilei', null, null, '1');
INSERT INTO `port_dept` VALUES ('1005', '集团人力资源部', null, '100', null, 'liping', null, null, '1');
INSERT INTO `port_dept` VALUES ('1060', '南方分公司', null, '100', null, 'wangwenying', null, null, '0');
INSERT INTO `port_dept` VALUES ('1061', '市场部', null, '1060', null, 'ranqingxin', null, null, '1');
INSERT INTO `port_dept` VALUES ('1062', '财务部', null, '1060', null, 'randun', null, null, '1');
INSERT INTO `port_dept` VALUES ('1063', '销售部', null, '1060', null, 'randun', null, null, '1');
INSERT INTO `port_dept` VALUES ('1070', '北方分公司', null, '100', null, 'lining', null, null, '0');
INSERT INTO `port_dept` VALUES ('1071', '市场部', null, '1070', null, 'lichao', null, null, '1');
INSERT INTO `port_dept` VALUES ('1072', '财务部', null, '1070', null, 'linyangyang', null, null, '1');
INSERT INTO `port_dept` VALUES ('1073', '销售部', null, '1070', null, 'tianyi', null, null, '1');
INSERT INTO `port_dept` VALUES ('1099', '外来单位', null, '100', null, 'Guest', null, null, '1');

-- ----------------------------
-- Table structure for `port_deptduty`
-- ----------------------------
DROP TABLE IF EXISTS `port_deptduty`;
CREATE TABLE `port_deptduty` (
  `FK_Dept` varchar(15) NOT NULL COMMENT '部门 - 主键',
  `FK_Duty` varchar(100) NOT NULL COMMENT '职务,主外键:对应物理表:Port_Duty,表描述:职务',
  PRIMARY KEY (`FK_Dept`,`FK_Duty`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='部门职务';

-- ----------------------------
-- Records of port_deptduty
-- ----------------------------
INSERT INTO `port_deptduty` VALUES ('100', '01');
INSERT INTO `port_deptduty` VALUES ('100', '02');
INSERT INTO `port_deptduty` VALUES ('1001', '04');
INSERT INTO `port_deptduty` VALUES ('1002', '04');
INSERT INTO `port_deptduty` VALUES ('1003', '04');
INSERT INTO `port_deptduty` VALUES ('1004', '04');
INSERT INTO `port_deptduty` VALUES ('1005', '04');

-- ----------------------------
-- Table structure for `port_deptemp`
-- ----------------------------
DROP TABLE IF EXISTS `port_deptemp`;
CREATE TABLE `port_deptemp` (
  `MyPK` varchar(100) NOT NULL COMMENT 'MyPK - 主键',
  `FK_Emp` varchar(50) DEFAULT NULL COMMENT '操作员',
  `FK_Dept` varchar(50) DEFAULT NULL COMMENT '部门',
  `FK_Duty` varchar(50) DEFAULT NULL COMMENT '职务',
  `DutyLevel` int(11) DEFAULT NULL COMMENT '职务级别',
  `Leader` varchar(50) DEFAULT NULL COMMENT '领导',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='部门人员信息';

-- ----------------------------
-- Records of port_deptemp
-- ----------------------------
INSERT INTO `port_deptemp` VALUES ('1001_zhanghaicheng', 'zhanghaicheng', '1001', '03', '20', 'zhoupeng');
INSERT INTO `port_deptemp` VALUES ('1001_zhangyifan', 'zhangyifan', '1001', '04', '20', 'zhanghaicheng');
INSERT INTO `port_deptemp` VALUES ('1001_zhoushengyu', 'zhoushengyu', '1001', '04', '20', 'zhanghaicheng');
INSERT INTO `port_deptemp` VALUES ('1002_qifenglin', 'qifenglin', '1002', '03', '20', 'zhoupeng');
INSERT INTO `port_deptemp` VALUES ('1002_zhoutianjiao', 'zhoutianjiao', '1002', '04', '20', 'qifenglin');
INSERT INTO `port_deptemp` VALUES ('1003_fuhui', 'fuhui', '1003', '04', '20', 'guoxiangbin');
INSERT INTO `port_deptemp` VALUES ('1003_guoxiangbin', 'guoxiangbin', '1003', '03', '20', 'zhoupeng');
INSERT INTO `port_deptemp` VALUES ('1004_guobaogeng', 'guobaogeng', '1004', '04', '20', 'yangyilei');
INSERT INTO `port_deptemp` VALUES ('1004_yangyilei', 'yangyilei', '1004', '03', '20', 'zhoupeng');
INSERT INTO `port_deptemp` VALUES ('1005_liping', 'liping', '1005', '03', '20', 'zhoupeng');
INSERT INTO `port_deptemp` VALUES ('1005_liyan', 'liyan', '1005', '04', '20', 'liping');
INSERT INTO `port_deptemp` VALUES ('100_zhoupeng', 'zhoupeng', '100', '02', '10', 'zhoupeng');

-- ----------------------------
-- Table structure for `port_deptempstation`
-- ----------------------------
DROP TABLE IF EXISTS `port_deptempstation`;
CREATE TABLE `port_deptempstation` (
  `MyPK` varchar(100) NOT NULL COMMENT 'MyPK - 主键',
  `FK_Dept` varchar(50) DEFAULT NULL COMMENT '部门',
  `FK_Station` varchar(50) DEFAULT NULL COMMENT '岗位',
  `FK_Emp` varchar(50) DEFAULT NULL COMMENT '操作员',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='部门岗位人员对应';

-- ----------------------------
-- Records of port_deptempstation
-- ----------------------------
INSERT INTO `port_deptempstation` VALUES ('1001_zhanghaicheng_02', '1001', '02', 'zhanghaicheng');
INSERT INTO `port_deptempstation` VALUES ('1001_zhangyifan_07', '1001', '07', 'zhangyifan');
INSERT INTO `port_deptempstation` VALUES ('1001_zhoushengyu_07', '1001', '07', 'zhoushengyu');
INSERT INTO `port_deptempstation` VALUES ('1002_qifenglin_03', '1002', '03', 'qifenglin');
INSERT INTO `port_deptempstation` VALUES ('1002_zhoutianjiao_08', '1002', '08', 'zhoutianjiao');
INSERT INTO `port_deptempstation` VALUES ('1003_fuhui_09', '1003', '09', 'fuhui');
INSERT INTO `port_deptempstation` VALUES ('1003_guoxiangbin_04', '1003', '04', 'guoxiangbin');
INSERT INTO `port_deptempstation` VALUES ('1004_guobaogeng_10', '1004', '10', 'guobaogeng');
INSERT INTO `port_deptempstation` VALUES ('1004_yangyilei_05', '1004', '05', 'yangyilei');
INSERT INTO `port_deptempstation` VALUES ('1005_liping_06', '1005', '06', 'liping');
INSERT INTO `port_deptempstation` VALUES ('1005_liyan_11', '1005', '11', 'liyan');
INSERT INTO `port_deptempstation` VALUES ('100_zhoupeng_01', '100', '01', 'zhoupeng');
INSERT INTO `port_deptempstation` VALUES ('1099_Guest_12', '1005', '12', 'Guest');

-- ----------------------------
-- Table structure for `port_deptsearchscorp`
-- ----------------------------
DROP TABLE IF EXISTS `port_deptsearchscorp`;
CREATE TABLE `port_deptsearchscorp` (
  `FK_Emp` varchar(50) NOT NULL COMMENT '操作员 - 主键',
  `FK_Dept` varchar(100) NOT NULL COMMENT '部门,主外键:对应物理表:Port_Dept,表描述:部门',
  PRIMARY KEY (`FK_Emp`,`FK_Dept`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='部门查询权限';

-- ----------------------------
-- Records of port_deptsearchscorp
-- ----------------------------

-- ----------------------------
-- Table structure for `port_deptstation`
-- ----------------------------
DROP TABLE IF EXISTS `port_deptstation`;
CREATE TABLE `port_deptstation` (
  `FK_Dept` varchar(15) NOT NULL COMMENT '部门 - 主键',
  `FK_Station` varchar(100) NOT NULL COMMENT '岗位,主外键:对应物理表:Port_Station,表描述:岗位',
  PRIMARY KEY (`FK_Dept`,`FK_Station`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='部门岗位对应';

-- ----------------------------
-- Records of port_deptstation
-- ----------------------------
INSERT INTO `port_deptstation` VALUES ('100', '01');
INSERT INTO `port_deptstation` VALUES ('1001', '07');
INSERT INTO `port_deptstation` VALUES ('1002', '08');
INSERT INTO `port_deptstation` VALUES ('1003', '09');
INSERT INTO `port_deptstation` VALUES ('1004', '10');
INSERT INTO `port_deptstation` VALUES ('1005', '11');

-- ----------------------------
-- Table structure for `port_depttype`
-- ----------------------------
DROP TABLE IF EXISTS `port_depttype`;
CREATE TABLE `port_depttype` (
  `No` varchar(2) NOT NULL COMMENT '编号 - 主键',
  `Name` varchar(50) DEFAULT NULL COMMENT '名称',
  PRIMARY KEY (`No`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='部门类型';

-- ----------------------------
-- Records of port_depttype
-- ----------------------------

-- ----------------------------
-- Table structure for `port_duty`
-- ----------------------------
DROP TABLE IF EXISTS `port_duty`;
CREATE TABLE `port_duty` (
  `No` varchar(2) NOT NULL COMMENT '编号 - 主键',
  `Name` varchar(50) DEFAULT NULL COMMENT '名称',
  PRIMARY KEY (`No`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='职务';

-- ----------------------------
-- Records of port_duty
-- ----------------------------
INSERT INTO `port_duty` VALUES ('01', '董事长');
INSERT INTO `port_duty` VALUES ('02', '总经理');
INSERT INTO `port_duty` VALUES ('03', '科长');
INSERT INTO `port_duty` VALUES ('04', '科员');
INSERT INTO `port_duty` VALUES ('05', '分公司总经理');
INSERT INTO `port_duty` VALUES ('20', '其他');

-- ----------------------------
-- Table structure for `port_emp`
-- ----------------------------
DROP TABLE IF EXISTS `port_emp`;
CREATE TABLE `port_emp` (
  `No` varchar(50) NOT NULL COMMENT '编号 - 主键',
  `EmpNo` varchar(50) DEFAULT NULL COMMENT '职工编号',
  `Name` varchar(200) DEFAULT NULL COMMENT '名称',
  `Pass` varchar(100) DEFAULT NULL COMMENT '密码',
  `FK_Dept` varchar(100) DEFAULT NULL COMMENT '部门,外键:对应物理表:Port_Dept,表描述:部门',
  `FK_Duty` varchar(20) DEFAULT NULL COMMENT '当前职务',
  `Leader` varchar(50) DEFAULT NULL COMMENT '当前领导',
  `SID` varchar(36) DEFAULT NULL COMMENT 'SID',
  `Tel` varchar(20) DEFAULT NULL COMMENT '电话',
  `Email` varchar(100) DEFAULT NULL COMMENT '邮箱',
  `NumOfDept` int(11) DEFAULT NULL COMMENT '部门数量',
  `Idx` int(11) DEFAULT NULL COMMENT '序号',
  PRIMARY KEY (`No`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户';

-- ----------------------------
-- Records of port_emp
-- ----------------------------
INSERT INTO `port_emp` VALUES ('admin', null, 'admin', '123', '100', '01', 'admin', '765b8fa91722471ea70f8c1cb427fe09', '0531-82374939', 'zhoupeng@ccflow.org', '1', null);
INSERT INTO `port_emp` VALUES ('fuhui', null, '福惠', '123', '1003', '04', 'guoxiangbin', null, '0531-82374939', 'fuhui@ccflow.org', '1', null);
INSERT INTO `port_emp` VALUES ('guobaogeng', null, '郭宝庚', '123', '1004', '04', 'yangyilei', null, '0531-82374939', 'guobaogeng@ccflow.org', '1', null);
INSERT INTO `port_emp` VALUES ('guoxiangbin', null, '郭祥斌', '123', '1003', '03', 'zhoupeng', null, '0531-82374939', 'guoxiangbin@ccflow.org', '1', null);
INSERT INTO `port_emp` VALUES ('liping', null, '李萍', '123', '1005', '03', 'zhoupeng', null, '0531-82374939', 'liping@ccflow.org', '1', null);
INSERT INTO `port_emp` VALUES ('liyan', null, '李言', '123', '1005', '04', 'liping', null, '0531-82374939', 'liyan@ccflow.org', '1', null);
INSERT INTO `port_emp` VALUES ('qifenglin', null, '祁凤林', '123', '1002', '03', 'zhoupeng', null, '0531-82374939', 'qifenglin@ccflow.org', '1', null);
INSERT INTO `port_emp` VALUES ('yangyilei', null, '杨依雷', '123', '1004', '03', 'zhoupeng', null, '0531-82374939', 'yangyilei@ccflow.org', '1', null);
INSERT INTO `port_emp` VALUES ('zhanghaicheng', null, '张海成', '123', '1001', '03', 'zhoupeng', null, '0531-82374939', 'zhanghaicheng@ccflow.org', '1', null);
INSERT INTO `port_emp` VALUES ('zhangyifan', null, '张一帆', '123', '1001', '04', 'zhanghaicheng', null, '0531-82374939', 'zhangyifan@ccflow.org', '1', null);
INSERT INTO `port_emp` VALUES ('zhoupeng', null, '周朋', '123', '100', '02', 'admin', null, '0531-82374939', 'zhoupeng@ccflow.org', '1', null);
INSERT INTO `port_emp` VALUES ('zhoushengyu', null, '周升雨', '123', '1001', '04', 'zhanghaicheng', null, '0531-82374939', 'zhoushengyu@ccflow.org', '1', null);
INSERT INTO `port_emp` VALUES ('zhoutianjiao', null, '周天娇', '123', '1002', '04', 'qifenglin', null, '0531-82374939', 'zhoutianjiao@ccflow.org', '1', null);

-- ----------------------------
-- Table structure for `port_empstation`
-- ----------------------------
DROP TABLE IF EXISTS `port_empstation`;
CREATE TABLE `port_empstation` (
  `FK_Emp` varchar(100) NOT NULL COMMENT '操作员 - 主键',
  `FK_Station` varchar(100) NOT NULL COMMENT '工作岗位,主外键:对应物理表:Port_Station,表描述:岗位',
  PRIMARY KEY (`FK_Emp`,`FK_Station`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='人员岗位';

-- ----------------------------
-- Records of port_empstation
-- ----------------------------

-- ----------------------------
-- Table structure for `port_inc`
-- ----------------------------
DROP TABLE IF EXISTS `port_inc`;
CREATE TABLE `port_inc` (
  `No` varchar(30) NOT NULL COMMENT '编号 - 主键',
  `Name` varchar(60) DEFAULT NULL COMMENT '名称',
  `ParentNo` varchar(30) DEFAULT NULL COMMENT '父节点编号',
  PRIMARY KEY (`No`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='独立组织';

-- ----------------------------
-- Records of port_inc
-- ----------------------------

-- ----------------------------
-- Table structure for `port_station`
-- ----------------------------
DROP TABLE IF EXISTS `port_station`;
CREATE TABLE `port_station` (
  `No` varchar(20) NOT NULL COMMENT '编号 - 主键',
  `Name` varchar(100) DEFAULT NULL COMMENT '名称',
  `FK_StationType` varchar(100) DEFAULT NULL COMMENT '岗位类型,外键:对应物理表:Port_StationType,表描述:岗位类型',
  `DutyReq` text COMMENT '职责要求',
  `Makings` text COMMENT '素质要求',
  PRIMARY KEY (`No`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='岗位';

-- ----------------------------
-- Records of port_station
-- ----------------------------
INSERT INTO `port_station` VALUES ('01', '总经理', '1', null, null);
INSERT INTO `port_station` VALUES ('02', '市场部经理', '2', null, null);
INSERT INTO `port_station` VALUES ('03', '研发部经理', '2', null, null);
INSERT INTO `port_station` VALUES ('04', '客服部经理', '2', null, null);
INSERT INTO `port_station` VALUES ('05', '财务部经理', '2', null, null);
INSERT INTO `port_station` VALUES ('06', '人力资源部经理', '2', null, null);
INSERT INTO `port_station` VALUES ('07', '销售人员岗', '3', null, null);
INSERT INTO `port_station` VALUES ('08', '程序员岗', '3', null, null);
INSERT INTO `port_station` VALUES ('09', '技术服务岗', '3', null, null);
INSERT INTO `port_station` VALUES ('10', '出纳岗', '3', null, null);
INSERT INTO `port_station` VALUES ('11', '人力资源助理岗', '3', null, null);
INSERT INTO `port_station` VALUES ('12', '外来人员岗', '3', null, null);

-- ----------------------------
-- Table structure for `port_stationtype`
-- ----------------------------
DROP TABLE IF EXISTS `port_stationtype`;
CREATE TABLE `port_stationtype` (
  `No` varchar(2) NOT NULL COMMENT '编号 - 主键',
  `Name` varchar(50) DEFAULT NULL COMMENT '名称',
  `Idx` int(11) DEFAULT NULL COMMENT '顺序',
  PRIMARY KEY (`No`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='岗位类型';

-- ----------------------------
-- Records of port_stationtype
-- ----------------------------
INSERT INTO `port_stationtype` VALUES ('1', '高层', null);
INSERT INTO `port_stationtype` VALUES ('2', '中层', null);
INSERT INTO `port_stationtype` VALUES ('3', '基层', null);

-- ----------------------------
-- Table structure for `pub_day`
-- ----------------------------
DROP TABLE IF EXISTS `pub_day`;
CREATE TABLE `pub_day` (
  `No` varchar(30) NOT NULL COMMENT '编号 - 主键',
  `Name` varchar(60) DEFAULT NULL COMMENT '名称',
  PRIMARY KEY (`No`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='日期';

-- ----------------------------
-- Records of pub_day
-- ----------------------------

-- ----------------------------
-- Table structure for `pub_nd`
-- ----------------------------
DROP TABLE IF EXISTS `pub_nd`;
CREATE TABLE `pub_nd` (
  `No` varchar(30) NOT NULL COMMENT '编号 - 主键',
  `Name` varchar(60) DEFAULT NULL COMMENT '名称',
  PRIMARY KEY (`No`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='年度';

-- ----------------------------
-- Records of pub_nd
-- ----------------------------

-- ----------------------------
-- Table structure for `pub_ny`
-- ----------------------------
DROP TABLE IF EXISTS `pub_ny`;
CREATE TABLE `pub_ny` (
  `No` varchar(30) NOT NULL COMMENT '编号 - 主键',
  `Name` varchar(60) DEFAULT NULL COMMENT '名称',
  PRIMARY KEY (`No`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='年月';

-- ----------------------------
-- Records of pub_ny
-- ----------------------------
INSERT INTO `pub_ny` VALUES ('2018-03', '2018-03');

-- ----------------------------
-- Table structure for `pub_yf`
-- ----------------------------
DROP TABLE IF EXISTS `pub_yf`;
CREATE TABLE `pub_yf` (
  `No` varchar(30) NOT NULL COMMENT '编号 - 主键',
  `Name` varchar(60) DEFAULT NULL COMMENT '名称',
  PRIMARY KEY (`No`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='月份';

-- ----------------------------
-- Records of pub_yf
-- ----------------------------

-- ----------------------------
-- Table structure for `sys_cfield`
-- ----------------------------
DROP TABLE IF EXISTS `sys_cfield`;
CREATE TABLE `sys_cfield` (
  `MyPK` varchar(100) NOT NULL COMMENT 'MyPK - 主键',
  `EnsName` varchar(100) DEFAULT NULL COMMENT '实体类名称',
  `FK_Emp` varchar(100) DEFAULT NULL COMMENT '工作人员',
  `Attrs` text COMMENT '属性s',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='列选择';

-- ----------------------------
-- Records of sys_cfield
-- ----------------------------

-- ----------------------------
-- Table structure for `sys_contrast`
-- ----------------------------
DROP TABLE IF EXISTS `sys_contrast`;
CREATE TABLE `sys_contrast` (
  `MyPK` varchar(100) NOT NULL COMMENT 'MyPK - 主键',
  `ContrastKey` varchar(20) DEFAULT NULL COMMENT '对比项目',
  `KeyVal1` varchar(20) DEFAULT NULL COMMENT 'KeyVal1',
  `KeyVal2` varchar(20) DEFAULT NULL COMMENT 'KeyVal2',
  `SortBy` varchar(20) DEFAULT NULL COMMENT 'SortBy',
  `KeyOfNum` varchar(20) DEFAULT NULL COMMENT 'KeyOfNum',
  `GroupWay` int(11) DEFAULT NULL COMMENT '求什么?SumAvg',
  `OrderWay` varchar(10) DEFAULT NULL COMMENT 'OrderWay',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='对比状态存储';

-- ----------------------------
-- Records of sys_contrast
-- ----------------------------

-- ----------------------------
-- Table structure for `sys_datarpt`
-- ----------------------------
DROP TABLE IF EXISTS `sys_datarpt`;
CREATE TABLE `sys_datarpt` (
  `MyPK` varchar(100) NOT NULL COMMENT 'MyPK - 主键',
  `ColCount` varchar(50) DEFAULT NULL COMMENT '列',
  `RowCount` varchar(50) DEFAULT NULL COMMENT '行',
  `Val` float DEFAULT NULL COMMENT '值',
  `RefOID` float DEFAULT NULL COMMENT '关联的值',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='报表数据存储模版';

-- ----------------------------
-- Records of sys_datarpt
-- ----------------------------

-- ----------------------------
-- Table structure for `sys_defval`
-- ----------------------------
DROP TABLE IF EXISTS `sys_defval`;
CREATE TABLE `sys_defval` (
  `OID` int(11) NOT NULL COMMENT 'OID - 主键',
  `FK_MapData` varchar(100) DEFAULT NULL COMMENT '实体',
  `AttrKey` varchar(50) DEFAULT NULL COMMENT '节点对应字段',
  `LB` int(11) DEFAULT NULL COMMENT '类别',
  `FK_Emp` varchar(100) DEFAULT NULL COMMENT '人员',
  `CurValue` text COMMENT '文本',
  PRIMARY KEY (`OID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='选择词汇';

-- ----------------------------
-- Records of sys_defval
-- ----------------------------

-- ----------------------------
-- Table structure for `sys_docfile`
-- ----------------------------
DROP TABLE IF EXISTS `sys_docfile`;
CREATE TABLE `sys_docfile` (
  `MyPK` varchar(100) NOT NULL COMMENT 'MyPK - 主键',
  `FileName` varchar(200) DEFAULT NULL COMMENT '名称',
  `FileSize` int(11) DEFAULT NULL COMMENT '大小',
  `FileType` varchar(50) DEFAULT NULL COMMENT '文件类型',
  `D1` text COMMENT 'D1',
  `D2` text COMMENT 'D2',
  `D3` text COMMENT 'D3',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='备注字段文件管理者';

-- ----------------------------
-- Records of sys_docfile
-- ----------------------------

-- ----------------------------
-- Table structure for `sys_domain`
-- ----------------------------
DROP TABLE IF EXISTS `sys_domain`;
CREATE TABLE `sys_domain` (
  `No` varchar(30) NOT NULL COMMENT '编号 - 主键',
  `Name` varchar(30) DEFAULT NULL COMMENT 'Name',
  `DBLink` varchar(130) DEFAULT NULL COMMENT 'DBLink',
  PRIMARY KEY (`No`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='域';

-- ----------------------------
-- Records of sys_domain
-- ----------------------------

-- ----------------------------
-- Table structure for `sys_encfg`
-- ----------------------------
DROP TABLE IF EXISTS `sys_encfg`;
CREATE TABLE `sys_encfg` (
  `No` varchar(100) NOT NULL COMMENT '实体名称 - 主键',
  `GroupTitle` varchar(2000) DEFAULT NULL COMMENT '分组标签',
  `FJSavePath` varchar(100) DEFAULT NULL COMMENT '保存路径',
  `FJWebPath` varchar(100) DEFAULT NULL COMMENT '附件Web路径',
  `Datan` varchar(200) DEFAULT NULL COMMENT '字段数据分析方式',
  `UI` varchar(2000) DEFAULT NULL COMMENT 'UI设置',
  PRIMARY KEY (`No`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='实体配置';

-- ----------------------------
-- Records of sys_encfg
-- ----------------------------
INSERT INTO `sys_encfg` VALUES ('BP.Sys.FrmUI.FrmAttachmentExt', '@MyPK=基础信息,附件的基本配置.\n@DeleteWay=权限控制,控制附件的下载与上传权限.@IsRowLock=WebOffice属性,设置与公文有关系的属性配置.\n@IsToHeLiuHZ=流程相关,控制节点附件的分合流.\n@FastKeyIsEnable=快捷键,为附件生成快捷键放入指定的目录.', null, null, null, null);
INSERT INTO `sys_encfg` VALUES ('BP.WF.Template.FlowExt', '@No=基本配置', null, null, null, null);
INSERT INTO `sys_encfg` VALUES ('BP.WF.Template.FlowSheet', '@No=基本配置@FlowRunWay=启动方式,配置工作流程如何自动发起，该选项要与流程服务一起工作才有效.@StartLimitRole=启动限制规则@StartGuideWay=发起前置导航@DTSWay=流程数据与业务数据同步@PStarter=轨迹查看权限', null, null, null, null);
INSERT INTO `sys_encfg` VALUES ('BP.WF.Template.FrmNodeComponent', '@NodeID=审核组件,适用于sdk表单审核组件与ccform上的审核组件属性设置.@SFLab=父子流程组件,在该节点上配置与显示父子流程.@FrmThreadLab=子线程组件,对合流节点有效，用于配置与现实子线程运行的情况。@FrmTrackLab=轨迹组件,用于显示流程运行的轨迹图.@FTCLab=流转自定义,在每个节点上自己控制节点的处理人.', null, null, null, null);
INSERT INTO `sys_encfg` VALUES ('BP.WF.Template.MapDataExt', '@No=基本属性@Designer=设计者信息', null, null, null, null);
INSERT INTO `sys_encfg` VALUES ('BP.WF.Template.MapDtlExt', '@No=基础信息,基础信息权限信息.@IsExp=数据导入导出,数据导入导出.@MTR=多表头,实现多表头.@FrmThreadLab=超连接,对合流节点有效用于配置与现实子线程运行的情况。@FrmTrackLab=流程相关,用于显示流程运行的轨迹图.', null, null, null, null);
INSERT INTO `sys_encfg` VALUES ('BP.WF.Template.MapFoolForm', '@No=基础属性,基础属性.@Designer=设计者信息,设计者的单位信息，人员信息，可以上传到表单云.', null, null, null, null);
INSERT INTO `sys_encfg` VALUES ('BP.WF.Template.NodeExt', '@NodeID=基本配置@FWCSta=审核组件,适用于sdk表单审核组件与ccform上的审核组件属性设置.@SendLab=按钮权限,控制工作节点可操作按钮.@RunModel=运行模式,分合流,父子流程@AutoJumpRole0=跳转,自动跳转规则当遇到该节点时如何让其自动的执行下一步.@MPhone_WorkModel=移动,与手机平板电脑相关的应用设置.@TSpanDay=考核,时效考核,质量考核.', null, null, null, null);
INSERT INTO `sys_encfg` VALUES ('BP.WF.Template.NodeSheet', '@NodeID=基本配置@FormType=表单@FWCSta=审核组件,适用于sdk表单审核组件与ccform上的审核组件属性设置.@SFSta=父子流程,对启动，查看父子流程的控件设置.@SendLab=按钮权限,控制工作节点可操作按钮.@RunModel=运行模式,分合流,父子流程@AutoJumpRole0=跳转,自动跳转规则当遇到该节点时如何让其自动的执行下一步.@MPhone_WorkModel=移动,与手机平板电脑相关的应用设置.@TSpanDay=考核,时效考核,质量考核.@OfficeOpen=公文按钮,只有当该节点是公文流程时候有效', null, null, null, null);

-- ----------------------------
-- Table structure for `sys_enum`
-- ----------------------------
DROP TABLE IF EXISTS `sys_enum`;
CREATE TABLE `sys_enum` (
  `MyPK` varchar(100) NOT NULL COMMENT 'MyPK - 主键',
  `Lab` varchar(200) DEFAULT NULL COMMENT 'Lab',
  `EnumKey` varchar(40) DEFAULT NULL COMMENT 'EnumKey',
  `IntKey` int(11) DEFAULT NULL COMMENT 'Val',
  `Lang` varchar(10) DEFAULT NULL COMMENT '语言',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='枚举数据';

-- ----------------------------
-- Records of sys_enum
-- ----------------------------
INSERT INTO `sys_enum` VALUES ('AlertType_CH_0', '短信', 'AlertType', '0', 'CH');
INSERT INTO `sys_enum` VALUES ('AlertType_CH_1', '邮件', 'AlertType', '1', 'CH');
INSERT INTO `sys_enum` VALUES ('AlertType_CH_2', '邮件与短信', 'AlertType', '2', 'CH');
INSERT INTO `sys_enum` VALUES ('AlertType_CH_3', '系统(内部)消息', 'AlertType', '3', 'CH');
INSERT INTO `sys_enum` VALUES ('AlertWay_CH_0', '不接收', 'AlertWay', '0', 'CH');
INSERT INTO `sys_enum` VALUES ('AlertWay_CH_1', '短信', 'AlertWay', '1', 'CH');
INSERT INTO `sys_enum` VALUES ('AlertWay_CH_2', '邮件', 'AlertWay', '2', 'CH');
INSERT INTO `sys_enum` VALUES ('AlertWay_CH_3', '内部消息', 'AlertWay', '3', 'CH');
INSERT INTO `sys_enum` VALUES ('AlertWay_CH_4', 'QQ消息', 'AlertWay', '4', 'CH');
INSERT INTO `sys_enum` VALUES ('AlertWay_CH_5', 'RTX消息', 'AlertWay', '5', 'CH');
INSERT INTO `sys_enum` VALUES ('AlertWay_CH_6', 'MSN消息', 'AlertWay', '6', 'CH');
INSERT INTO `sys_enum` VALUES ('AppType_CH_0', '外部Url连接', 'AppType', '0', 'CH');
INSERT INTO `sys_enum` VALUES ('AppType_CH_1', '本地可执行文件', 'AppType', '1', 'CH');
INSERT INTO `sys_enum` VALUES ('AthRunModel_CH_0', '流水模式', 'AthRunModel', '0', 'CH');
INSERT INTO `sys_enum` VALUES ('AthRunModel_CH_1', '固定模式', 'AthRunModel', '1', 'CH');
INSERT INTO `sys_enum` VALUES ('AthUploadWay_CH_0', '继承模式', 'AthUploadWay', '0', 'CH');
INSERT INTO `sys_enum` VALUES ('AthUploadWay_CH_1', '协作模式', 'AthUploadWay', '1', 'CH');
INSERT INTO `sys_enum` VALUES ('AuthorWay_CH_0', '不授权', 'AuthorWay', '0', 'CH');
INSERT INTO `sys_enum` VALUES ('AuthorWay_CH_1', '全部流程授权', 'AuthorWay', '1', 'CH');
INSERT INTO `sys_enum` VALUES ('AuthorWay_CH_2', '指定流程授权', 'AuthorWay', '2', 'CH');
INSERT INTO `sys_enum` VALUES ('BillFileType_CH_0', 'Word', 'BillFileType', '0', 'CH');
INSERT INTO `sys_enum` VALUES ('BillFileType_CH_1', 'PDF', 'BillFileType', '1', 'CH');
INSERT INTO `sys_enum` VALUES ('BillFileType_CH_2', 'Excel(未完成)', 'BillFileType', '2', 'CH');
INSERT INTO `sys_enum` VALUES ('BillFileType_CH_3', 'Html(未完成)', 'BillFileType', '3', 'CH');
INSERT INTO `sys_enum` VALUES ('BillFileType_CH_5', '锐浪报表', 'BillFileType', '5', 'CH');
INSERT INTO `sys_enum` VALUES ('CancelRole_CH_0', '上一步可以撤销', 'CancelRole', '0', 'CH');
INSERT INTO `sys_enum` VALUES ('CancelRole_CH_1', '不能撤销', 'CancelRole', '1', 'CH');
INSERT INTO `sys_enum` VALUES ('CancelRole_CH_2', '上一步与开始节点可以撤销', 'CancelRole', '2', 'CH');
INSERT INTO `sys_enum` VALUES ('CancelRole_CH_3', '指定的节点可以撤销', 'CancelRole', '3', 'CH');
INSERT INTO `sys_enum` VALUES ('CCWriteTo_CH_0', '写入抄送列表', 'CCWriteTo', '0', 'CH');
INSERT INTO `sys_enum` VALUES ('CCWriteTo_CH_1', '写入待办', 'CCWriteTo', '1', 'CH');
INSERT INTO `sys_enum` VALUES ('CCWriteTo_CH_2', '写入待办与抄送列表', 'CCWriteTo', '2', 'CH');
INSERT INTO `sys_enum` VALUES ('CHAlertRole_CH_0', '不提示', 'CHAlertRole', '0', 'CH');
INSERT INTO `sys_enum` VALUES ('CHAlertRole_CH_1', '每天1次', 'CHAlertRole', '1', 'CH');
INSERT INTO `sys_enum` VALUES ('CHAlertRole_CH_2', '每天2次', 'CHAlertRole', '2', 'CH');
INSERT INTO `sys_enum` VALUES ('CHAlertWay_CH_0', '邮件', 'CHAlertWay', '0', 'CH');
INSERT INTO `sys_enum` VALUES ('CHAlertWay_CH_1', '短信', 'CHAlertWay', '1', 'CH');
INSERT INTO `sys_enum` VALUES ('CHAlertWay_CH_2', 'CCIM即时通讯', 'CHAlertWay', '2', 'CH');
INSERT INTO `sys_enum` VALUES ('ChartType_CH_0', '几何图形', 'ChartType', '0', 'CH');
INSERT INTO `sys_enum` VALUES ('ChartType_CH_1', '肖像图片', 'ChartType', '1', 'CH');
INSERT INTO `sys_enum` VALUES ('CHSta_CH_0', '及时完成', 'CHSta', '0', 'CH');
INSERT INTO `sys_enum` VALUES ('CHSta_CH_1', '按期完成', 'CHSta', '1', 'CH');
INSERT INTO `sys_enum` VALUES ('CHSta_CH_2', '逾期完成', 'CHSta', '2', 'CH');
INSERT INTO `sys_enum` VALUES ('CHSta_CH_3', '超期完成', 'CHSta', '3', 'CH');
INSERT INTO `sys_enum` VALUES ('CHWay_CH_0', '不考核', 'CHWay', '0', 'CH');
INSERT INTO `sys_enum` VALUES ('CHWay_CH_1', '按时效', 'CHWay', '1', 'CH');
INSERT INTO `sys_enum` VALUES ('CHWay_CH_2', '按工作量', 'CHWay', '2', 'CH');
INSERT INTO `sys_enum` VALUES ('CodeStruct_CH_0', '普通的编码表(具有No,Name)', 'CodeStruct', '0', 'CH');
INSERT INTO `sys_enum` VALUES ('CodeStruct_CH_1', '树结构(具有No,Name,ParentNo)', 'CodeStruct', '1', 'CH');
INSERT INTO `sys_enum` VALUES ('CodeStruct_CH_2', '行政机构编码表(编码以两位编号标识级次树形关系)', 'CodeStruct', '2', 'CH');
INSERT INTO `sys_enum` VALUES ('ColSpanAttrDT_CH_1', '跨1个单元格', 'ColSpanAttrDT', '1', 'CH');
INSERT INTO `sys_enum` VALUES ('ColSpanAttrDT_CH_3', '跨3个单元格', 'ColSpanAttrDT', '3', 'CH');
INSERT INTO `sys_enum` VALUES ('ColSpanAttrString_CH_1', '跨1个单元格', 'ColSpanAttrString', '1', 'CH');
INSERT INTO `sys_enum` VALUES ('ColSpanAttrString_CH_3', '跨3个单元格', 'ColSpanAttrString', '3', 'CH');
INSERT INTO `sys_enum` VALUES ('ColSpanAttrString_CH_4', '跨4个单元格', 'ColSpanAttrString', '4', 'CH');
INSERT INTO `sys_enum` VALUES ('ColSpan_CH_1', '跨1个单元格', 'ColSpan', '1', 'CH');
INSERT INTO `sys_enum` VALUES ('ColSpan_CH_3', '跨3个单元格', 'ColSpan', '3', 'CH');
INSERT INTO `sys_enum` VALUES ('ColSpan_CH_4', '跨4个单元格', 'ColSpan', '4', 'CH');
INSERT INTO `sys_enum` VALUES ('ConnJudgeWay_CH_0', 'or', 'ConnJudgeWay', '0', 'CH');
INSERT INTO `sys_enum` VALUES ('ConnJudgeWay_CH_1', 'and', 'ConnJudgeWay', '1', 'CH');
INSERT INTO `sys_enum` VALUES ('CtrlWayAth_CH_0', 'PK-主键', 'CtrlWayAth', '0', 'CH');
INSERT INTO `sys_enum` VALUES ('CtrlWayAth_CH_1', 'FID-流程ID', 'CtrlWayAth', '1', 'CH');
INSERT INTO `sys_enum` VALUES ('CtrlWayAth_CH_2', 'ParentID-父流程ID', 'CtrlWayAth', '2', 'CH');
INSERT INTO `sys_enum` VALUES ('CtrlWayAth_CH_3', '仅能查看自己上传的数据', 'CtrlWayAth', '3', 'CH');
INSERT INTO `sys_enum` VALUES ('CtrlWay_CH_0', '按岗位', 'CtrlWay', '0', 'CH');
INSERT INTO `sys_enum` VALUES ('CtrlWay_CH_1', '按部门', 'CtrlWay', '1', 'CH');
INSERT INTO `sys_enum` VALUES ('CtrlWay_CH_2', '按人员', 'CtrlWay', '2', 'CH');
INSERT INTO `sys_enum` VALUES ('CtrlWay_CH_3', '按SQL', 'CtrlWay', '3', 'CH');
INSERT INTO `sys_enum` VALUES ('DataStoreModel_CH_0', '数据轨迹模式', 'DataStoreModel', '0', 'CH');
INSERT INTO `sys_enum` VALUES ('DataStoreModel_CH_1', '数据合并模式', 'DataStoreModel', '1', 'CH');
INSERT INTO `sys_enum` VALUES ('DBSrcType_CH_0', '应用系统主数据库(默认)', 'DBSrcType', '0', 'CH');
INSERT INTO `sys_enum` VALUES ('DBSrcType_CH_1', 'SQLServer数据库', 'DBSrcType', '1', 'CH');
INSERT INTO `sys_enum` VALUES ('DBSrcType_CH_100', 'WebService数据源', 'DBSrcType', '100', 'CH');
INSERT INTO `sys_enum` VALUES ('DBSrcType_CH_2', 'Oracle数据库', 'DBSrcType', '2', 'CH');
INSERT INTO `sys_enum` VALUES ('DBSrcType_CH_3', 'MySQL数据库', 'DBSrcType', '3', 'CH');
INSERT INTO `sys_enum` VALUES ('DBSrcType_CH_4', 'Informix数据库', 'DBSrcType', '4', 'CH');
INSERT INTO `sys_enum` VALUES ('DBSrcType_CH_50', 'Dubbo服务', 'DBSrcType', '50', 'CH');
INSERT INTO `sys_enum` VALUES ('DelEnable_CH_0', '不能删除', 'DelEnable', '0', 'CH');
INSERT INTO `sys_enum` VALUES ('DelEnable_CH_1', '逻辑删除', 'DelEnable', '1', 'CH');
INSERT INTO `sys_enum` VALUES ('DelEnable_CH_2', '记录日志方式删除', 'DelEnable', '2', 'CH');
INSERT INTO `sys_enum` VALUES ('DelEnable_CH_3', '彻底删除', 'DelEnable', '3', 'CH');
INSERT INTO `sys_enum` VALUES ('DelEnable_CH_4', '让用户决定删除方式', 'DelEnable', '4', 'CH');
INSERT INTO `sys_enum` VALUES ('DeleteWay_CH_0', '不能删除', 'DeleteWay', '0', 'CH');
INSERT INTO `sys_enum` VALUES ('DeleteWay_CH_1', '删除所有', 'DeleteWay', '1', 'CH');
INSERT INTO `sys_enum` VALUES ('DeleteWay_CH_2', '只能删除自己上传的', 'DeleteWay', '2', 'CH');
INSERT INTO `sys_enum` VALUES ('DocType_CH_0', '正式公文', 'DocType', '0', 'CH');
INSERT INTO `sys_enum` VALUES ('DocType_CH_1', '便函', 'DocType', '1', 'CH');
INSERT INTO `sys_enum` VALUES ('Draft_CH_0', '无(不设草稿)', 'Draft', '0', 'CH');
INSERT INTO `sys_enum` VALUES ('Draft_CH_1', '保存到待办', 'Draft', '1', 'CH');
INSERT INTO `sys_enum` VALUES ('Draft_CH_2', '保存到草稿箱', 'Draft', '2', 'CH');
INSERT INTO `sys_enum` VALUES ('DtlAddRecModel_CH_0', '按设置的数量初始化空白行', 'DtlAddRecModel', '0', 'CH');
INSERT INTO `sys_enum` VALUES ('DtlAddRecModel_CH_1', '用按钮增加空白行', 'DtlAddRecModel', '1', 'CH');
INSERT INTO `sys_enum` VALUES ('DtlOpenType_CH_0', '操作员', 'DtlOpenType', '0', 'CH');
INSERT INTO `sys_enum` VALUES ('DtlOpenType_CH_1', '工作ID', 'DtlOpenType', '1', 'CH');
INSERT INTO `sys_enum` VALUES ('DtlOpenType_CH_2', '流程ID', 'DtlOpenType', '2', 'CH');
INSERT INTO `sys_enum` VALUES ('DtlSaveModel_CH_0', '自动存盘(失去焦点自动存盘)', 'DtlSaveModel', '0', 'CH');
INSERT INTO `sys_enum` VALUES ('DtlSaveModel_CH_1', '手动存盘(保存按钮触发存盘)', 'DtlSaveModel', '1', 'CH');
INSERT INTO `sys_enum` VALUES ('DtlShowModel_CH_0', '表格', 'DtlShowModel', '0', 'CH');
INSERT INTO `sys_enum` VALUES ('DtlShowModel_CH_1', '卡片(自由模式)', 'DtlShowModel', '1', 'CH');
INSERT INTO `sys_enum` VALUES ('DtlShowModel_CH_2', '卡片(傻瓜模式)', 'DtlShowModel', '2', 'CH');
INSERT INTO `sys_enum` VALUES ('DTSearchWay_CH_0', '不启用', 'DTSearchWay', '0', 'CH');
INSERT INTO `sys_enum` VALUES ('DTSearchWay_CH_1', '按日期', 'DTSearchWay', '1', 'CH');
INSERT INTO `sys_enum` VALUES ('DTSearchWay_CH_2', '按日期时间', 'DTSearchWay', '2', 'CH');
INSERT INTO `sys_enum` VALUES ('DTSField_CH_0', '字段名相同', 'DTSField', '0', 'CH');
INSERT INTO `sys_enum` VALUES ('DTSField_CH_1', '按设置的字段匹配', 'DTSField', '1', 'CH');
INSERT INTO `sys_enum` VALUES ('DTSTime_CH_0', '所有的节点发送后', 'DTSTime', '0', 'CH');
INSERT INTO `sys_enum` VALUES ('DTSTime_CH_1', '指定的节点发送后', 'DTSTime', '1', 'CH');
INSERT INTO `sys_enum` VALUES ('DTSTime_CH_2', '当流程结束时', 'DTSTime', '2', 'CH');
INSERT INTO `sys_enum` VALUES ('DTSWay_CH_0', '不考核', 'DTSWay', '0', 'CH');
INSERT INTO `sys_enum` VALUES ('DTSWay_CH_1', '按照时效考核', 'DTSWay', '1', 'CH');
INSERT INTO `sys_enum` VALUES ('DTSWay_CH_2', '按照工作量考核', 'DTSWay', '2', 'CH');
INSERT INTO `sys_enum` VALUES ('EditModel_CH_0', '无', 'EditModel', '0', 'CH');
INSERT INTO `sys_enum` VALUES ('EditModel_CH_1', '傻瓜表单', 'EditModel', '1', 'CH');
INSERT INTO `sys_enum` VALUES ('EditModel_CH_2', '自由表单', 'EditModel', '2', 'CH');
INSERT INTO `sys_enum` VALUES ('EnumUIContralType_CH_1', '下拉框', 'EnumUIContralType', '1', 'CH');
INSERT INTO `sys_enum` VALUES ('EnumUIContralType_CH_3', '单选按钮', 'EnumUIContralType', '3', 'CH');
INSERT INTO `sys_enum` VALUES ('ExcelType_CH_0', '普通文件数据提取', 'ExcelType', '0', 'CH');
INSERT INTO `sys_enum` VALUES ('ExcelType_CH_1', '流程附件数据提取', 'ExcelType', '1', 'CH');
INSERT INTO `sys_enum` VALUES ('ExpType_CH_3', '按照SQL计算', 'ExpType', '3', 'CH');
INSERT INTO `sys_enum` VALUES ('ExpType_CH_4', '按照参数计算', 'ExpType', '4', 'CH');
INSERT INTO `sys_enum` VALUES ('FJOpen_CH_0', '关闭附件', 'FJOpen', '0', 'CH');
INSERT INTO `sys_enum` VALUES ('FJOpen_CH_1', '操作员', 'FJOpen', '1', 'CH');
INSERT INTO `sys_enum` VALUES ('FJOpen_CH_2', '工作ID', 'FJOpen', '2', 'CH');
INSERT INTO `sys_enum` VALUES ('FJOpen_CH_3', '流程ID', 'FJOpen', '3', 'CH');
INSERT INTO `sys_enum` VALUES ('FlowDeleteRole_CH_0', '超级管理员可以删除', 'FlowDeleteRole', '0', 'CH');
INSERT INTO `sys_enum` VALUES ('FlowDeleteRole_CH_1', '分级管理员可以删除', 'FlowDeleteRole', '1', 'CH');
INSERT INTO `sys_enum` VALUES ('FlowDeleteRole_CH_2', '发起人可以删除', 'FlowDeleteRole', '2', 'CH');
INSERT INTO `sys_enum` VALUES ('FlowDeleteRole_CH_3', '节点启动删除按钮的操作员', 'FlowDeleteRole', '3', 'CH');
INSERT INTO `sys_enum` VALUES ('FlowRunWay_CH_0', '手工启动', 'FlowRunWay', '0', 'CH');
INSERT INTO `sys_enum` VALUES ('FlowRunWay_CH_1', '指定人员按时启动', 'FlowRunWay', '1', 'CH');
INSERT INTO `sys_enum` VALUES ('FlowRunWay_CH_2', '数据集按时启动', 'FlowRunWay', '2', 'CH');
INSERT INTO `sys_enum` VALUES ('FlowRunWay_CH_3', '触发式启动', 'FlowRunWay', '3', 'CH');
INSERT INTO `sys_enum` VALUES ('FLRole_CH_0', '按接受人', 'FLRole', '0', 'CH');
INSERT INTO `sys_enum` VALUES ('FLRole_CH_1', '按部门', 'FLRole', '1', 'CH');
INSERT INTO `sys_enum` VALUES ('FLRole_CH_2', '按岗位', 'FLRole', '2', 'CH');
INSERT INTO `sys_enum` VALUES ('FormType_CH_0', '傻瓜表单(ccflow6取消支持)', 'FormType', '0', 'CH');
INSERT INTO `sys_enum` VALUES ('FormType_CH_1', '自由表单', 'FormType', '1', 'CH');
INSERT INTO `sys_enum` VALUES ('FormType_CH_100', '禁用(对多表单流程有效)', 'FormType', '100', 'CH');
INSERT INTO `sys_enum` VALUES ('FormType_CH_2', '嵌入式表单', 'FormType', '2', 'CH');
INSERT INTO `sys_enum` VALUES ('FormType_CH_3', 'SDK表单', 'FormType', '3', 'CH');
INSERT INTO `sys_enum` VALUES ('FormType_CH_4', 'SL表单(ccflow6取消支持)', 'FormType', '4', 'CH');
INSERT INTO `sys_enum` VALUES ('FormType_CH_5', '表单树', 'FormType', '5', 'CH');
INSERT INTO `sys_enum` VALUES ('FormType_CH_6', '动态表单树', 'FormType', '6', 'CH');
INSERT INTO `sys_enum` VALUES ('FormType_CH_7', '公文表单(WebOffice)', 'FormType', '7', 'CH');
INSERT INTO `sys_enum` VALUES ('FormType_CH_8', 'Excel表单(测试中)', 'FormType', '8', 'CH');
INSERT INTO `sys_enum` VALUES ('FormType_CH_9', 'Word表单(测试中)', 'FormType', '9', 'CH');
INSERT INTO `sys_enum` VALUES ('FrmEnableRole_CH_0', '始终启用', 'FrmEnableRole', '0', 'CH');
INSERT INTO `sys_enum` VALUES ('FrmEnableRole_CH_1', '有数据时启用', 'FrmEnableRole', '1', 'CH');
INSERT INTO `sys_enum` VALUES ('FrmEnableRole_CH_2', '有参数时启用', 'FrmEnableRole', '2', 'CH');
INSERT INTO `sys_enum` VALUES ('FrmEnableRole_CH_3', '按表单的字段表达式', 'FrmEnableRole', '3', 'CH');
INSERT INTO `sys_enum` VALUES ('FrmEnableRole_CH_4', '按SQL表达式', 'FrmEnableRole', '4', 'CH');
INSERT INTO `sys_enum` VALUES ('FrmEnableRole_CH_5', '不启用', 'FrmEnableRole', '5', 'CH');
INSERT INTO `sys_enum` VALUES ('FrmSln_CH_0', '默认方案', 'FrmSln', '0', 'CH');
INSERT INTO `sys_enum` VALUES ('FrmSln_CH_1', '只读方案', 'FrmSln', '1', 'CH');
INSERT INTO `sys_enum` VALUES ('FrmSln_CH_2', '自定义方案', 'FrmSln', '2', 'CH');
INSERT INTO `sys_enum` VALUES ('FrmThreadSta_CH_0', '禁用', 'FrmThreadSta', '0', 'CH');
INSERT INTO `sys_enum` VALUES ('FrmThreadSta_CH_1', '启用', 'FrmThreadSta', '1', 'CH');
INSERT INTO `sys_enum` VALUES ('FrmUrlShowWay_CH_0', '不显示', 'FrmUrlShowWay', '0', 'CH');
INSERT INTO `sys_enum` VALUES ('FrmUrlShowWay_CH_1', '自动大小', 'FrmUrlShowWay', '1', 'CH');
INSERT INTO `sys_enum` VALUES ('FrmUrlShowWay_CH_2', '指定大小', 'FrmUrlShowWay', '2', 'CH');
INSERT INTO `sys_enum` VALUES ('FrmUrlShowWay_CH_3', '新窗口', 'FrmUrlShowWay', '3', 'CH');
INSERT INTO `sys_enum` VALUES ('FTCWorkModel_CH_0', '简洁模式', 'FTCWorkModel', '0', 'CH');
INSERT INTO `sys_enum` VALUES ('FTCWorkModel_CH_1', '高级模式', 'FTCWorkModel', '1', 'CH');
INSERT INTO `sys_enum` VALUES ('FWCAth_CH_0', '不启用', 'FWCAth', '0', 'CH');
INSERT INTO `sys_enum` VALUES ('FWCAth_CH_1', '多附件', 'FWCAth', '1', 'CH');
INSERT INTO `sys_enum` VALUES ('FWCAth_CH_2', '单附件(暂不支持)', 'FWCAth', '2', 'CH');
INSERT INTO `sys_enum` VALUES ('FWCAth_CH_3', '图片附件(暂不支持)', 'FWCAth', '3', 'CH');
INSERT INTO `sys_enum` VALUES ('FWCOrderModel_CH_0', '按审批时间先后排序', 'FWCOrderModel', '0', 'CH');
INSERT INTO `sys_enum` VALUES ('FWCOrderModel_CH_1', '按照接受人员列表先后顺序(官职大小)', 'FWCOrderModel', '1', 'CH');
INSERT INTO `sys_enum` VALUES ('FWCShowModel_CH_0', '表格方式', 'FWCShowModel', '0', 'CH');
INSERT INTO `sys_enum` VALUES ('FWCShowModel_CH_1', '自由模式', 'FWCShowModel', '1', 'CH');
INSERT INTO `sys_enum` VALUES ('FWCSta_CH_0', '禁用', 'FWCSta', '0', 'CH');
INSERT INTO `sys_enum` VALUES ('FWCSta_CH_1', '启用', 'FWCSta', '1', 'CH');
INSERT INTO `sys_enum` VALUES ('FWCSta_CH_2', '只读', 'FWCSta', '2', 'CH');
INSERT INTO `sys_enum` VALUES ('HuiQianRole_CH_0', '不启用', 'HuiQianRole', '0', 'CH');
INSERT INTO `sys_enum` VALUES ('HuiQianRole_CH_1', '协作模式', 'HuiQianRole', '1', 'CH');
INSERT INTO `sys_enum` VALUES ('HuiQianRole_CH_4', '组长模式', 'HuiQianRole', '4', 'CH');
INSERT INTO `sys_enum` VALUES ('HungUpWay_CH_0', '无限挂起', 'HungUpWay', '0', 'CH');
INSERT INTO `sys_enum` VALUES ('HungUpWay_CH_1', '按指定的时间解除挂起并通知我自己', 'HungUpWay', '1', 'CH');
INSERT INTO `sys_enum` VALUES ('HungUpWay_CH_2', '按指定的时间解除挂起并通知所有人', 'HungUpWay', '2', 'CH');
INSERT INTO `sys_enum` VALUES ('ImgSrcType_CH_0', '本地', 'ImgSrcType', '0', 'CH');
INSERT INTO `sys_enum` VALUES ('ImgSrcType_CH_1', 'URL', 'ImgSrcType', '1', 'CH');
INSERT INTO `sys_enum` VALUES ('ImpModel_CH_0', '不导入', 'ImpModel', '0', 'CH');
INSERT INTO `sys_enum` VALUES ('ImpModel_CH_1', '按SQL设置导入', 'ImpModel', '1', 'CH');
INSERT INTO `sys_enum` VALUES ('ImpModel_CH_2', '按JSON模式导入', 'ImpModel', '2', 'CH');
INSERT INTO `sys_enum` VALUES ('ImpModel_CH_3', '按照xls文件模版导入', 'ImpModel', '3', 'CH');
INSERT INTO `sys_enum` VALUES ('IsAutoSendSubFlowOver_CH_0', '不处理', 'IsAutoSendSubFlowOver', '0', 'CH');
INSERT INTO `sys_enum` VALUES ('IsAutoSendSubFlowOver_CH_1', '让父流程自动运行下一步', 'IsAutoSendSubFlowOver', '1', 'CH');
INSERT INTO `sys_enum` VALUES ('IsAutoSendSubFlowOver_CH_2', '结束父流程', 'IsAutoSendSubFlowOver', '2', 'CH');
INSERT INTO `sys_enum` VALUES ('IsSigan_CH_0', '无', 'IsSigan', '0', 'CH');
INSERT INTO `sys_enum` VALUES ('IsSigan_CH_1', '图片签名', 'IsSigan', '1', 'CH');
INSERT INTO `sys_enum` VALUES ('IsSigan_CH_2', '山东CA', 'IsSigan', '2', 'CH');
INSERT INTO `sys_enum` VALUES ('IsSigan_CH_3', '广东CA', 'IsSigan', '3', 'CH');
INSERT INTO `sys_enum` VALUES ('JMCD_CH_0', '一般', 'JMCD', '0', 'CH');
INSERT INTO `sys_enum` VALUES ('JMCD_CH_1', '保密', 'JMCD', '1', 'CH');
INSERT INTO `sys_enum` VALUES ('JMCD_CH_2', '秘密', 'JMCD', '2', 'CH');
INSERT INTO `sys_enum` VALUES ('JMCD_CH_3', '机密', 'JMCD', '3', 'CH');
INSERT INTO `sys_enum` VALUES ('JumpWay_CH_0', '不能跳转', 'JumpWay', '0', 'CH');
INSERT INTO `sys_enum` VALUES ('JumpWay_CH_1', '只能向后跳转', 'JumpWay', '1', 'CH');
INSERT INTO `sys_enum` VALUES ('JumpWay_CH_2', '只能向前跳转', 'JumpWay', '2', 'CH');
INSERT INTO `sys_enum` VALUES ('JumpWay_CH_3', '任意节点跳转', 'JumpWay', '3', 'CH');
INSERT INTO `sys_enum` VALUES ('JumpWay_CH_4', '按指定规则跳转', 'JumpWay', '4', 'CH');
INSERT INTO `sys_enum` VALUES ('LGType_CH_0', '普通', 'LGType', '0', 'CH');
INSERT INTO `sys_enum` VALUES ('LGType_CH_1', '枚举', 'LGType', '1', 'CH');
INSERT INTO `sys_enum` VALUES ('LGType_CH_2', '外键', 'LGType', '2', 'CH');
INSERT INTO `sys_enum` VALUES ('LGType_CH_3', '打开系统页面', 'LGType', '3', 'CH');
INSERT INTO `sys_enum` VALUES ('ListShowModel_CH_0', '表格', 'ListShowModel', '0', 'CH');
INSERT INTO `sys_enum` VALUES ('ListShowModel_CH_1', '卡片', 'ListShowModel', '1', 'CH');
INSERT INTO `sys_enum` VALUES ('Model_CH_0', '普通', 'Model', '0', 'CH');
INSERT INTO `sys_enum` VALUES ('Model_CH_1', '固定行', 'Model', '1', 'CH');
INSERT INTO `sys_enum` VALUES ('MPad_SrcModel_CH_0', '强制横屏', 'MPad_SrcModel', '0', 'CH');
INSERT INTO `sys_enum` VALUES ('MPad_SrcModel_CH_1', '强制竖屏', 'MPad_SrcModel', '1', 'CH');
INSERT INTO `sys_enum` VALUES ('MPad_SrcModel_CH_2', '由重力感应决定', 'MPad_SrcModel', '2', 'CH');
INSERT INTO `sys_enum` VALUES ('MPad_WorkModel_CH_0', '原生态', 'MPad_WorkModel', '0', 'CH');
INSERT INTO `sys_enum` VALUES ('MPad_WorkModel_CH_1', '浏览器', 'MPad_WorkModel', '1', 'CH');
INSERT INTO `sys_enum` VALUES ('MPad_WorkModel_CH_2', '禁用', 'MPad_WorkModel', '2', 'CH');
INSERT INTO `sys_enum` VALUES ('MPhone_SrcModel_CH_0', '强制横屏', 'MPhone_SrcModel', '0', 'CH');
INSERT INTO `sys_enum` VALUES ('MPhone_SrcModel_CH_1', '强制竖屏', 'MPhone_SrcModel', '1', 'CH');
INSERT INTO `sys_enum` VALUES ('MPhone_SrcModel_CH_2', '由重力感应决定', 'MPhone_SrcModel', '2', 'CH');
INSERT INTO `sys_enum` VALUES ('MPhone_WorkModel_CH_0', '原生态', 'MPhone_WorkModel', '0', 'CH');
INSERT INTO `sys_enum` VALUES ('MPhone_WorkModel_CH_1', '浏览器', 'MPhone_WorkModel', '1', 'CH');
INSERT INTO `sys_enum` VALUES ('MPhone_WorkModel_CH_2', '禁用', 'MPhone_WorkModel', '2', 'CH');
INSERT INTO `sys_enum` VALUES ('MsgCtrl_CH_0', '不发送', 'MsgCtrl', '0', 'CH');
INSERT INTO `sys_enum` VALUES ('MsgCtrl_CH_1', '按设置的下一步接受人自动发送（默认）', 'MsgCtrl', '1', 'CH');
INSERT INTO `sys_enum` VALUES ('MsgCtrl_CH_2', '由本节点表单系统字段(IsSendEmail,IsSendSMS)来决定', 'MsgCtrl', '2', 'CH');
INSERT INTO `sys_enum` VALUES ('MsgCtrl_CH_3', '由SDK开发者参数(IsSendEmail,IsSendSMS)来决定', 'MsgCtrl', '3', 'CH');
INSERT INTO `sys_enum` VALUES ('MyDataType_CH_1', '字符串String', 'MyDataType', '1', 'CH');
INSERT INTO `sys_enum` VALUES ('MyDataType_CH_2', '整数类型Int', 'MyDataType', '2', 'CH');
INSERT INTO `sys_enum` VALUES ('MyDataType_CH_3', '浮点类型AppFloat', 'MyDataType', '3', 'CH');
INSERT INTO `sys_enum` VALUES ('MyDataType_CH_4', '判断类型Boolean', 'MyDataType', '4', 'CH');
INSERT INTO `sys_enum` VALUES ('MyDataType_CH_5', '双精度类型Double', 'MyDataType', '5', 'CH');
INSERT INTO `sys_enum` VALUES ('MyDataType_CH_6', '日期型Date', 'MyDataType', '6', 'CH');
INSERT INTO `sys_enum` VALUES ('MyDataType_CH_7', '时间类型Datetime', 'MyDataType', '7', 'CH');
INSERT INTO `sys_enum` VALUES ('MyDataType_CH_8', '金额类型AppMoney', 'MyDataType', '8', 'CH');
INSERT INTO `sys_enum` VALUES ('MyDeptRole_CH_0', '仅部门领导可以查看', 'MyDeptRole', '0', 'CH');
INSERT INTO `sys_enum` VALUES ('MyDeptRole_CH_1', '部门下所有的人都可以查看', 'MyDeptRole', '1', 'CH');
INSERT INTO `sys_enum` VALUES ('MyDeptRole_CH_2', '本部门里指定岗位的人可以查看', 'MyDeptRole', '2', 'CH');
INSERT INTO `sys_enum` VALUES ('PopValFormat_CH_0', 'No(仅编号)', 'PopValFormat', '0', 'CH');
INSERT INTO `sys_enum` VALUES ('PopValFormat_CH_1', 'Name(仅名称)', 'PopValFormat', '1', 'CH');
INSERT INTO `sys_enum` VALUES ('PopValFormat_CH_2', 'No,Name(编号与名称,比如zhangsan,张三;lisi,李四;)', 'PopValFormat', '2', 'CH');
INSERT INTO `sys_enum` VALUES ('PrintDocEnable_CH_0', '不打印', 'PrintDocEnable', '0', 'CH');
INSERT INTO `sys_enum` VALUES ('PrintDocEnable_CH_1', '打印网页', 'PrintDocEnable', '1', 'CH');
INSERT INTO `sys_enum` VALUES ('PrintDocEnable_CH_2', '打印RTF模板', 'PrintDocEnable', '2', 'CH');
INSERT INTO `sys_enum` VALUES ('PrintDocEnable_CH_3', '打印Word模版', 'PrintDocEnable', '3', 'CH');
INSERT INTO `sys_enum` VALUES ('PRI_CH_0', '低', 'PRI', '0', 'CH');
INSERT INTO `sys_enum` VALUES ('PRI_CH_1', '中', 'PRI', '1', 'CH');
INSERT INTO `sys_enum` VALUES ('PRI_CH_2', '高', 'PRI', '2', 'CH');
INSERT INTO `sys_enum` VALUES ('PushWay_CH_0', '按照指定节点的工作人员', 'PushWay', '0', 'CH');
INSERT INTO `sys_enum` VALUES ('PushWay_CH_1', '按照指定的工作人员', 'PushWay', '1', 'CH');
INSERT INTO `sys_enum` VALUES ('PushWay_CH_2', '按照指定的工作岗位', 'PushWay', '2', 'CH');
INSERT INTO `sys_enum` VALUES ('PushWay_CH_3', '按照指定的部门', 'PushWay', '3', 'CH');
INSERT INTO `sys_enum` VALUES ('PushWay_CH_4', '按照指定的SQL', 'PushWay', '4', 'CH');
INSERT INTO `sys_enum` VALUES ('PushWay_CH_5', '按照系统指定的字段', 'PushWay', '5', 'CH');
INSERT INTO `sys_enum` VALUES ('RBShowModel_CH_0', '竖向', 'RBShowModel', '0', 'CH');
INSERT INTO `sys_enum` VALUES ('RBShowModel_CH_3', '横向', 'RBShowModel', '3', 'CH');
INSERT INTO `sys_enum` VALUES ('ReadReceipts_CH_0', '不回执', 'ReadReceipts', '0', 'CH');
INSERT INTO `sys_enum` VALUES ('ReadReceipts_CH_1', '自动回执', 'ReadReceipts', '1', 'CH');
INSERT INTO `sys_enum` VALUES ('ReadReceipts_CH_2', '由上一节点表单字段决定', 'ReadReceipts', '2', 'CH');
INSERT INTO `sys_enum` VALUES ('ReadReceipts_CH_3', '由SDK开发者参数决定', 'ReadReceipts', '3', 'CH');
INSERT INTO `sys_enum` VALUES ('ReturnRole_CH_0', '不能退回', 'ReturnRole', '0', 'CH');
INSERT INTO `sys_enum` VALUES ('ReturnRole_CH_1', '只能退回上一个节点', 'ReturnRole', '1', 'CH');
INSERT INTO `sys_enum` VALUES ('ReturnRole_CH_2', '可退回以前任意节点', 'ReturnRole', '2', 'CH');
INSERT INTO `sys_enum` VALUES ('ReturnRole_CH_3', '可退回指定的节点', 'ReturnRole', '3', 'CH');
INSERT INTO `sys_enum` VALUES ('ReturnRole_CH_4', '由流程图设计的退回路线决定', 'ReturnRole', '4', 'CH');
INSERT INTO `sys_enum` VALUES ('ReturnSendModel_CH_0', '从退回节点正常执行', 'ReturnSendModel', '0', 'CH');
INSERT INTO `sys_enum` VALUES ('ReturnSendModel_CH_1', '直接发送到当前节点', 'ReturnSendModel', '1', 'CH');
INSERT INTO `sys_enum` VALUES ('ReturnSendModel_CH_2', '直接发送到当前节点的下一个节点', 'ReturnSendModel', '2', 'CH');
INSERT INTO `sys_enum` VALUES ('SaveModel_CH_0', '仅节点表', 'SaveModel', '0', 'CH');
INSERT INTO `sys_enum` VALUES ('SaveModel_CH_1', '节点表与Rpt表', 'SaveModel', '1', 'CH');
INSERT INTO `sys_enum` VALUES ('SelectAccepterEnable_CH_0', '不启用', 'SelectAccepterEnable', '0', 'CH');
INSERT INTO `sys_enum` VALUES ('SelectAccepterEnable_CH_1', '单独启用', 'SelectAccepterEnable', '1', 'CH');
INSERT INTO `sys_enum` VALUES ('SelectAccepterEnable_CH_2', '在发送前打开', 'SelectAccepterEnable', '2', 'CH');
INSERT INTO `sys_enum` VALUES ('SelectAccepterEnable_CH_3', '转入新页面', 'SelectAccepterEnable', '3', 'CH');
INSERT INTO `sys_enum` VALUES ('SelectorModel_CH_0', '按岗位', 'SelectorModel', '0', 'CH');
INSERT INTO `sys_enum` VALUES ('SelectorModel_CH_1', '按部门', 'SelectorModel', '1', 'CH');
INSERT INTO `sys_enum` VALUES ('SelectorModel_CH_2', '按人员', 'SelectorModel', '2', 'CH');
INSERT INTO `sys_enum` VALUES ('SelectorModel_CH_3', '按SQL', 'SelectorModel', '3', 'CH');
INSERT INTO `sys_enum` VALUES ('SelectorModel_CH_4', '按SQL模版计算', 'SelectorModel', '4', 'CH');
INSERT INTO `sys_enum` VALUES ('SelectorModel_CH_5', '使用通用人员选择器', 'SelectorModel', '5', 'CH');
INSERT INTO `sys_enum` VALUES ('SelectorModel_CH_6', '部门与岗位的交集', 'SelectorModel', '6', 'CH');
INSERT INTO `sys_enum` VALUES ('SelectorModel_CH_7', '自定义Url', 'SelectorModel', '7', 'CH');
INSERT INTO `sys_enum` VALUES ('SFOpenType_CH_0', '工作查看器', 'SFOpenType', '0', 'CH');
INSERT INTO `sys_enum` VALUES ('SFOpenType_CH_1', '傻瓜表单轨迹查看器', 'SFOpenType', '1', 'CH');
INSERT INTO `sys_enum` VALUES ('SFShowCtrl_CH_0', '可以看所有的子流程', 'SFShowCtrl', '0', 'CH');
INSERT INTO `sys_enum` VALUES ('SFShowCtrl_CH_1', '仅仅可以看自己发起的子流程', 'SFShowCtrl', '1', 'CH');
INSERT INTO `sys_enum` VALUES ('SFShowModel_CH_0', '表格方式', 'SFShowModel', '0', 'CH');
INSERT INTO `sys_enum` VALUES ('SFShowModel_CH_1', '自由模式', 'SFShowModel', '1', 'CH');
INSERT INTO `sys_enum` VALUES ('SFSta_CH_0', '禁用', 'SFSta', '0', 'CH');
INSERT INTO `sys_enum` VALUES ('SFSta_CH_1', '启用', 'SFSta', '1', 'CH');
INSERT INTO `sys_enum` VALUES ('SFSta_CH_2', '只读', 'SFSta', '2', 'CH');
INSERT INTO `sys_enum` VALUES ('SharingType_CH_0', '共享', 'SharingType', '0', 'CH');
INSERT INTO `sys_enum` VALUES ('SharingType_CH_1', '私有', 'SharingType', '1', 'CH');
INSERT INTO `sys_enum` VALUES ('ShowWhere_CH_0', '树形表单', 'ShowWhere', '0', 'CH');
INSERT INTO `sys_enum` VALUES ('ShowWhere_CH_1', '工具栏', 'ShowWhere', '1', 'CH');
INSERT INTO `sys_enum` VALUES ('SQLType_CH_0', '方向条件', 'SQLType', '0', 'CH');
INSERT INTO `sys_enum` VALUES ('SQLType_CH_1', '接受人规则', 'SQLType', '1', 'CH');
INSERT INTO `sys_enum` VALUES ('SQLType_CH_2', '下拉框数据过滤', 'SQLType', '2', 'CH');
INSERT INTO `sys_enum` VALUES ('SQLType_CH_3', '级联下拉框', 'SQLType', '3', 'CH');
INSERT INTO `sys_enum` VALUES ('SQLType_CH_4', 'PopVal开窗返回值', 'SQLType', '4', 'CH');
INSERT INTO `sys_enum` VALUES ('SQLType_CH_5', '人员选择器人员选择范围', 'SQLType', '5', 'CH');
INSERT INTO `sys_enum` VALUES ('SubFlowStartWay_CH_0', '不启动', 'SubFlowStartWay', '0', 'CH');
INSERT INTO `sys_enum` VALUES ('SubFlowStartWay_CH_1', '指定的字段启动', 'SubFlowStartWay', '1', 'CH');
INSERT INTO `sys_enum` VALUES ('SubFlowStartWay_CH_2', '按明细表启动', 'SubFlowStartWay', '2', 'CH');
INSERT INTO `sys_enum` VALUES ('SubThreadType_CH_0', '同表单', 'SubThreadType', '0', 'CH');
INSERT INTO `sys_enum` VALUES ('SubThreadType_CH_1', '异表单', 'SubThreadType', '1', 'CH');
INSERT INTO `sys_enum` VALUES ('TabType_CH_0', '本地表或视图', 'TabType', '0', 'CH');
INSERT INTO `sys_enum` VALUES ('TabType_CH_1', '通过一个SQL确定的一个外部数据源', 'TabType', '1', 'CH');
INSERT INTO `sys_enum` VALUES ('TabType_CH_2', '通过WebServices获得的一个数据源', 'TabType', '2', 'CH');
INSERT INTO `sys_enum` VALUES ('Target_CH_0', '新窗口', 'Target', '0', 'CH');
INSERT INTO `sys_enum` VALUES ('Target_CH_1', '本窗口', 'Target', '1', 'CH');
INSERT INTO `sys_enum` VALUES ('Target_CH_2', '父窗口', 'Target', '2', 'CH');
INSERT INTO `sys_enum` VALUES ('TaskSta_CH_0', '未开始', 'TaskSta', '0', 'CH');
INSERT INTO `sys_enum` VALUES ('TaskSta_CH_1', '进行中', 'TaskSta', '1', 'CH');
INSERT INTO `sys_enum` VALUES ('TaskSta_CH_2', '完成', 'TaskSta', '2', 'CH');
INSERT INTO `sys_enum` VALUES ('TaskSta_CH_3', '推迟', 'TaskSta', '3', 'CH');
INSERT INTO `sys_enum` VALUES ('ThreadKillRole_CH_0', '不能删除', 'ThreadKillRole', '0', 'CH');
INSERT INTO `sys_enum` VALUES ('ThreadKillRole_CH_1', '手工删除', 'ThreadKillRole', '1', 'CH');
INSERT INTO `sys_enum` VALUES ('ThreadKillRole_CH_2', '自动删除', 'ThreadKillRole', '2', 'CH');
INSERT INTO `sys_enum` VALUES ('TimelineRole_CH_0', '按节点(由节点属性来定义)', 'TimelineRole', '0', 'CH');
INSERT INTO `sys_enum` VALUES ('TimelineRole_CH_1', '按发起人(开始节点SysSDTOfFlow字段计算)', 'TimelineRole', '1', 'CH');
INSERT INTO `sys_enum` VALUES ('TSpan_CH_0', '本周', 'TSpan', '0', 'CH');
INSERT INTO `sys_enum` VALUES ('TSpan_CH_1', '上周', 'TSpan', '1', 'CH');
INSERT INTO `sys_enum` VALUES ('TSpan_CH_2', '两周以前', 'TSpan', '2', 'CH');
INSERT INTO `sys_enum` VALUES ('TSpan_CH_3', '三周以前', 'TSpan', '3', 'CH');
INSERT INTO `sys_enum` VALUES ('TSpan_CH_4', '更早', 'TSpan', '4', 'CH');
INSERT INTO `sys_enum` VALUES ('TurnToDeal_CH_0', '提示ccflow默认信息', 'TurnToDeal', '0', 'CH');
INSERT INTO `sys_enum` VALUES ('TurnToDeal_CH_1', '提示指定信息', 'TurnToDeal', '1', 'CH');
INSERT INTO `sys_enum` VALUES ('TurnToDeal_CH_2', '转向指定的url', 'TurnToDeal', '2', 'CH');
INSERT INTO `sys_enum` VALUES ('TurnToDeal_CH_3', '按照条件转向', 'TurnToDeal', '3', 'CH');
INSERT INTO `sys_enum` VALUES ('UIContralType_CH_1', '下拉框', 'UIContralType', '1', 'CH');
INSERT INTO `sys_enum` VALUES ('UIContralType_CH_3', '单选按钮', 'UIContralType', '3', 'CH');
INSERT INTO `sys_enum` VALUES ('UploadFileCheck_CH_0', '不控制', 'UploadFileCheck', '0', 'CH');
INSERT INTO `sys_enum` VALUES ('UploadFileCheck_CH_1', '上传附件个数不能为0', 'UploadFileCheck', '1', 'CH');
INSERT INTO `sys_enum` VALUES ('UploadFileCheck_CH_2', '每个类别下面的个数不能为0', 'UploadFileCheck', '2', 'CH');
INSERT INTO `sys_enum` VALUES ('UploadType_CH_0', '单个', 'UploadType', '0', 'CH');
INSERT INTO `sys_enum` VALUES ('UploadType_CH_1', '多个', 'UploadType', '1', 'CH');
INSERT INTO `sys_enum` VALUES ('UploadType_CH_2', '指定', 'UploadType', '2', 'CH');
INSERT INTO `sys_enum` VALUES ('UserType_CH_0', '普通用户', 'UserType', '0', 'CH');
INSERT INTO `sys_enum` VALUES ('UserType_CH_1', '管理员用户', 'UserType', '1', 'CH');
INSERT INTO `sys_enum` VALUES ('UseSta_CH_0', '禁用', 'UseSta', '0', 'CH');
INSERT INTO `sys_enum` VALUES ('UseSta_CH_1', '启用', 'UseSta', '1', 'CH');
INSERT INTO `sys_enum` VALUES ('WFStateApp_CH_10', '批处理', 'WFStateApp', '10', 'CH');
INSERT INTO `sys_enum` VALUES ('WFStateApp_CH_2', '运行中', 'WFStateApp', '2', 'CH');
INSERT INTO `sys_enum` VALUES ('WFStateApp_CH_3', '已完成', 'WFStateApp', '3', 'CH');
INSERT INTO `sys_enum` VALUES ('WFStateApp_CH_4', '挂起', 'WFStateApp', '4', 'CH');
INSERT INTO `sys_enum` VALUES ('WFStateApp_CH_5', '退回', 'WFStateApp', '5', 'CH');
INSERT INTO `sys_enum` VALUES ('WFStateApp_CH_6', '转发', 'WFStateApp', '6', 'CH');
INSERT INTO `sys_enum` VALUES ('WFStateApp_CH_7', '删除', 'WFStateApp', '7', 'CH');
INSERT INTO `sys_enum` VALUES ('WFStateApp_CH_8', '加签', 'WFStateApp', '8', 'CH');
INSERT INTO `sys_enum` VALUES ('WFStateApp_CH_9', '冻结', 'WFStateApp', '9', 'CH');
INSERT INTO `sys_enum` VALUES ('WFState_CH_0', '空白', 'WFState', '0', 'CH');
INSERT INTO `sys_enum` VALUES ('WFState_CH_1', '草稿', 'WFState', '1', 'CH');
INSERT INTO `sys_enum` VALUES ('WFState_CH_10', '批处理', 'WFState', '10', 'CH');
INSERT INTO `sys_enum` VALUES ('WFState_CH_11', '加签回复状态', 'WFState', '11', 'CH');
INSERT INTO `sys_enum` VALUES ('WFState_CH_2', '运行中', 'WFState', '2', 'CH');
INSERT INTO `sys_enum` VALUES ('WFState_CH_3', '已完成', 'WFState', '3', 'CH');
INSERT INTO `sys_enum` VALUES ('WFState_CH_4', '挂起', 'WFState', '4', 'CH');
INSERT INTO `sys_enum` VALUES ('WFState_CH_5', '退回', 'WFState', '5', 'CH');
INSERT INTO `sys_enum` VALUES ('WFState_CH_6', '转发', 'WFState', '6', 'CH');
INSERT INTO `sys_enum` VALUES ('WFState_CH_7', '删除', 'WFState', '7', 'CH');
INSERT INTO `sys_enum` VALUES ('WFState_CH_8', '加签', 'WFState', '8', 'CH');
INSERT INTO `sys_enum` VALUES ('WFState_CH_9', '冻结', 'WFState', '9', 'CH');
INSERT INTO `sys_enum` VALUES ('WFSta_CH_0', '运行中', 'WFSta', '0', 'CH');
INSERT INTO `sys_enum` VALUES ('WFSta_CH_1', '已完成', 'WFSta', '1', 'CH');
INSERT INTO `sys_enum` VALUES ('WFSta_CH_2', '其他', 'WFSta', '2', 'CH');
INSERT INTO `sys_enum` VALUES ('WhenOverSize_CH_0', '不处理', 'WhenOverSize', '0', 'CH');
INSERT INTO `sys_enum` VALUES ('WhenOverSize_CH_1', '向下顺增行', 'WhenOverSize', '1', 'CH');
INSERT INTO `sys_enum` VALUES ('WhenOverSize_CH_2', '次页显示', 'WhenOverSize', '2', 'CH');
INSERT INTO `sys_enum` VALUES ('WhoExeIt_CH_0', '操作员执行', 'WhoExeIt', '0', 'CH');
INSERT INTO `sys_enum` VALUES ('WhoExeIt_CH_1', '机器执行', 'WhoExeIt', '1', 'CH');
INSERT INTO `sys_enum` VALUES ('WhoExeIt_CH_2', '混合执行', 'WhoExeIt', '2', 'CH');
INSERT INTO `sys_enum` VALUES ('WhoIsPK_CH_0', 'WorkID是主键', 'WhoIsPK', '0', 'CH');
INSERT INTO `sys_enum` VALUES ('WhoIsPK_CH_1', 'FID是主键(干流程的WorkID)', 'WhoIsPK', '1', 'CH');
INSERT INTO `sys_enum` VALUES ('WhoIsPK_CH_2', '父流程ID是主键', 'WhoIsPK', '2', 'CH');
INSERT INTO `sys_enum` VALUES ('WhoIsPK_CH_3', '延续流程ID是主键', 'WhoIsPK', '3', 'CH');
INSERT INTO `sys_enum` VALUES ('YBFlowReturnRole_CH_0', '不能退回', 'YBFlowReturnRole', '0', 'CH');
INSERT INTO `sys_enum` VALUES ('YBFlowReturnRole_CH_1', '退回到父流程的开始节点', 'YBFlowReturnRole', '1', 'CH');
INSERT INTO `sys_enum` VALUES ('YBFlowReturnRole_CH_2', '退回到父流程的任何节点', 'YBFlowReturnRole', '2', 'CH');
INSERT INTO `sys_enum` VALUES ('YBFlowReturnRole_CH_3', '退回父流程的启动节点', 'YBFlowReturnRole', '3', 'CH');
INSERT INTO `sys_enum` VALUES ('YBFlowReturnRole_CH_4', '可退回到指定的节点', 'YBFlowReturnRole', '4', 'CH');

-- ----------------------------
-- Table structure for `sys_enummain`
-- ----------------------------
DROP TABLE IF EXISTS `sys_enummain`;
CREATE TABLE `sys_enummain` (
  `No` varchar(40) NOT NULL COMMENT '编号 - 主键',
  `Name` varchar(40) DEFAULT NULL COMMENT '名称',
  `CfgVal` varchar(1500) DEFAULT NULL COMMENT '配置信息',
  `Lang` varchar(10) DEFAULT NULL COMMENT '语言',
  PRIMARY KEY (`No`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='枚举';

-- ----------------------------
-- Records of sys_enummain
-- ----------------------------

-- ----------------------------
-- Table structure for `sys_enver`
-- ----------------------------
DROP TABLE IF EXISTS `sys_enver`;
CREATE TABLE `sys_enver` (
  `MyPK` varchar(100) NOT NULL COMMENT 'MyPK - 主键',
  `No` varchar(50) DEFAULT NULL COMMENT '实体类',
  `Name` varchar(100) DEFAULT NULL COMMENT '实体名',
  `PKValue` varchar(300) DEFAULT NULL COMMENT '主键值',
  `EVer` int(11) DEFAULT NULL COMMENT '版本号',
  `Rec` varchar(100) DEFAULT NULL COMMENT '修改人',
  `RDT` varchar(50) DEFAULT NULL COMMENT '修改日期',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='实体版本号';

-- ----------------------------
-- Records of sys_enver
-- ----------------------------

-- ----------------------------
-- Table structure for `sys_enverdtl`
-- ----------------------------
DROP TABLE IF EXISTS `sys_enverdtl`;
CREATE TABLE `sys_enverdtl` (
  `MyPK` varchar(100) NOT NULL COMMENT 'MyPK - 主键',
  `EnName` varchar(200) DEFAULT NULL COMMENT '实体名',
  `EnVerPK` varchar(100) DEFAULT NULL COMMENT '版本主表PK',
  `AttrKey` varchar(100) DEFAULT NULL COMMENT '字段',
  `AttrName` varchar(200) DEFAULT NULL COMMENT '字段名',
  `OldVal` varchar(100) DEFAULT NULL COMMENT '旧值',
  `NewVal` varchar(100) DEFAULT NULL COMMENT '新值',
  `EnVer` int(11) DEFAULT NULL COMMENT '版本号(日期)',
  `RDT` varchar(50) DEFAULT NULL COMMENT '日期',
  `Rec` varchar(100) DEFAULT NULL COMMENT '版本号',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='实体修改明细';

-- ----------------------------
-- Records of sys_enverdtl
-- ----------------------------

-- ----------------------------
-- Table structure for `sys_excelfile`
-- ----------------------------
DROP TABLE IF EXISTS `sys_excelfile`;
CREATE TABLE `sys_excelfile` (
  `No` varchar(36) NOT NULL COMMENT '编号 - 主键',
  `Name` varchar(50) DEFAULT NULL COMMENT '名称',
  `Mark` varchar(50) DEFAULT NULL COMMENT '标识',
  `ExcelType` int(11) DEFAULT NULL COMMENT ' 类型,枚举类型:0 普通文件数据提取 1 流程附件数据提取',
  `Note` text COMMENT '上传说明',
  PRIMARY KEY (`No`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Excel模板';

-- ----------------------------
-- Records of sys_excelfile
-- ----------------------------

-- ----------------------------
-- Table structure for `sys_exceltable`
-- ----------------------------
DROP TABLE IF EXISTS `sys_exceltable`;
CREATE TABLE `sys_exceltable` (
  `No` varchar(36) NOT NULL COMMENT '编号 - 主键',
  `Name` varchar(50) DEFAULT NULL COMMENT '数据表名',
  `FK_ExcelFile` varchar(100) DEFAULT NULL COMMENT 'Excel模板,外键:对应物理表:Sys_ExcelFile,表描述:Excel模板',
  `IsDtl` int(11) DEFAULT NULL COMMENT '是否明细表',
  `Note` text COMMENT '数据表说明',
  `SyncToTable` varchar(100) DEFAULT NULL COMMENT '同步到表',
  PRIMARY KEY (`No`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Excel数据表';

-- ----------------------------
-- Records of sys_exceltable
-- ----------------------------

-- ----------------------------
-- Table structure for `sys_filemanager`
-- ----------------------------
DROP TABLE IF EXISTS `sys_filemanager`;
CREATE TABLE `sys_filemanager` (
  `OID` int(11) NOT NULL COMMENT 'OID - 主键',
  `AttrFileName` varchar(50) DEFAULT NULL COMMENT '指定名称',
  `AttrFileNo` varchar(50) DEFAULT NULL COMMENT '指定编号',
  `EnName` varchar(50) DEFAULT NULL COMMENT '关联的表',
  `RefVal` varchar(50) DEFAULT NULL COMMENT '主键值',
  `WebPath` varchar(100) DEFAULT NULL COMMENT 'Web路径',
  `MyFileName` varchar(100) DEFAULT NULL COMMENT '文件名称',
  `MyFilePath` varchar(100) DEFAULT NULL COMMENT 'MyFilePath',
  `MyFileExt` varchar(10) DEFAULT NULL COMMENT 'MyFileExt',
  `MyFileH` int(11) DEFAULT NULL COMMENT 'MyFileH',
  `MyFileW` int(11) DEFAULT NULL COMMENT 'MyFileW',
  `MyFileSize` float DEFAULT NULL COMMENT 'MyFileSize',
  `RDT` varchar(50) DEFAULT NULL COMMENT '上传时间',
  `Rec` varchar(50) DEFAULT NULL COMMENT '上传人',
  `Doc` text COMMENT '内容',
  PRIMARY KEY (`OID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='文件管理者';

-- ----------------------------
-- Records of sys_filemanager
-- ----------------------------

-- ----------------------------
-- Table structure for `sys_formtree`
-- ----------------------------
DROP TABLE IF EXISTS `sys_formtree`;
CREATE TABLE `sys_formtree` (
  `No` varchar(10) NOT NULL COMMENT '编号 - 主键',
  `Name` varchar(100) DEFAULT NULL COMMENT '名称',
  `ParentNo` varchar(100) DEFAULT NULL COMMENT '父节点No',
  `DBSrc` varchar(100) DEFAULT NULL COMMENT 'DBSrc',
  `IsDir` int(11) DEFAULT NULL COMMENT '是否是目录?',
  `Idx` int(11) DEFAULT NULL COMMENT 'Idx',
  PRIMARY KEY (`No`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='表单树';

-- ----------------------------
-- Records of sys_formtree
-- ----------------------------
INSERT INTO `sys_formtree` VALUES ('01', '表单类别1', '1', null, '0', '0');
INSERT INTO `sys_formtree` VALUES ('1', '根目录', '0', null, null, null);

-- ----------------------------
-- Table structure for `sys_frmattachment`
-- ----------------------------
DROP TABLE IF EXISTS `sys_frmattachment`;
CREATE TABLE `sys_frmattachment` (
  `MyPK` varchar(100) NOT NULL COMMENT 'MyPK - 主键',
  `FK_MapData` varchar(100) DEFAULT NULL COMMENT '表单ID',
  `NoOfObj` varchar(50) DEFAULT NULL COMMENT '附件编号',
  `FK_Node` int(11) DEFAULT NULL COMMENT '节点控制(对sln有效)',
  `Name` varchar(50) DEFAULT NULL COMMENT '附件名称',
  `Exts` varchar(50) DEFAULT NULL COMMENT '文件格式(*.*,*.doc)',
  `SaveTo` varchar(150) DEFAULT NULL COMMENT '保存到',
  `Sort` varchar(500) DEFAULT NULL COMMENT '类别(可为空)',
  `X` float DEFAULT NULL COMMENT 'X',
  `Y` float DEFAULT NULL COMMENT 'Y',
  `W` float DEFAULT NULL COMMENT '宽度',
  `H` float DEFAULT NULL COMMENT '高度',
  `IsUpload` int(11) DEFAULT NULL COMMENT '是否可以上传',
  `IsDelete` int(11) DEFAULT NULL COMMENT '附件删除规则(0=不能删除1=删除所有2=只能删除自己上传的)',
  `DeleteWay` int(11) DEFAULT NULL COMMENT '  附件删除规则,枚举类型:0 不能删除 1 删除所有 2 只能删除自己上传的',
  `IsDownload` int(11) DEFAULT NULL COMMENT '是否可以下载',
  `IsOrder` int(11) DEFAULT NULL COMMENT '是否可以排序',
  `IsAutoSize` int(11) DEFAULT NULL COMMENT '自动控制大小',
  `IsNote` int(11) DEFAULT NULL COMMENT '是否增加备注',
  `IsShowTitle` int(11) DEFAULT NULL COMMENT '是否显示标题列',
  `UploadType` int(11) DEFAULT NULL COMMENT '  上传类型,枚举类型:0 单个 1 多个 2 指定',
  `CtrlWay` int(11) DEFAULT NULL COMMENT '  控制呈现控制方式,枚举类型:0 单个 1 多个 2 指定',
  `AthUploadWay` int(11) DEFAULT NULL COMMENT ' 控制上传控制方式,枚举类型:0 继承模式 1 协作模式',
  `IsWoEnableWF` int(11) DEFAULT NULL COMMENT '是否启用weboffice',
  `IsWoEnableSave` int(11) DEFAULT NULL COMMENT '是否启用保存',
  `IsWoEnableReadonly` int(11) DEFAULT NULL COMMENT '是否只读',
  `IsWoEnableRevise` int(11) DEFAULT NULL COMMENT '是否启用修订',
  `IsWoEnableViewKeepMark` int(11) DEFAULT NULL COMMENT '是否查看用户留痕',
  `IsWoEnablePrint` int(11) DEFAULT NULL COMMENT '是否打印',
  `IsWoEnableOver` int(11) DEFAULT NULL COMMENT '是否启用套红',
  `IsWoEnableSeal` int(11) DEFAULT NULL COMMENT '是否启用签章',
  `IsWoEnableTemplete` int(11) DEFAULT NULL COMMENT '是否启用公文模板',
  `IsWoEnableCheck` int(11) DEFAULT NULL COMMENT '是否自动写入审核信息',
  `IsWoEnableInsertFlow` int(11) DEFAULT NULL COMMENT '是否插入流程',
  `IsWoEnableInsertFengXian` int(11) DEFAULT NULL COMMENT '是否插入风险点',
  `IsWoEnableMarks` int(11) DEFAULT NULL COMMENT '是否启用留痕模式',
  `IsWoEnableDown` int(11) DEFAULT NULL COMMENT '是否启用下载',
  `AtPara` varchar(3000) DEFAULT NULL COMMENT 'AtPara',
  `GroupID` int(11) DEFAULT NULL COMMENT 'GroupID',
  `GUID` varchar(128) DEFAULT NULL COMMENT 'GUID',
  `IsTurn2Hmtl` int(11) DEFAULT NULL COMMENT '是否转换成html(方便手机浏览)',
  `IsRowLock` int(11) DEFAULT NULL COMMENT '是否启用锁定行',
  `IsToHeLiuHZ` int(11) DEFAULT NULL COMMENT '该附件是否要汇总到合流节点上去？(对子线程节点有效)',
  `IsHeLiuHuiZong` int(11) DEFAULT NULL COMMENT '是否是合流节点的汇总附件组件？(对合流节点有效)',
  `AthRunModel` int(11) DEFAULT NULL COMMENT ' 运行模式,枚举类型:0 流水模式 1 固定模式',
  `IsTurn2Html` int(11) DEFAULT NULL COMMENT '是否转换成html(方便手机浏览)',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='附件';

-- ----------------------------
-- Records of sys_frmattachment
-- ----------------------------

-- ----------------------------
-- Table structure for `sys_frmattachmentdb`
-- ----------------------------
DROP TABLE IF EXISTS `sys_frmattachmentdb`;
CREATE TABLE `sys_frmattachmentdb` (
  `MyPK` varchar(100) NOT NULL COMMENT 'MyPK - 主键',
  `FK_MapData` varchar(100) DEFAULT NULL COMMENT 'FK_MapData',
  `FK_FrmAttachment` varchar(500) DEFAULT NULL COMMENT '附件编号',
  `RefPKVal` varchar(50) DEFAULT NULL COMMENT '实体主键',
  `FID` int(11) DEFAULT NULL COMMENT 'FID',
  `Sort` varchar(200) DEFAULT NULL COMMENT '类别',
  `FileFullName` varchar(700) DEFAULT NULL COMMENT '文件路径',
  `FileName` varchar(500) DEFAULT NULL COMMENT '名称',
  `FileExts` varchar(50) DEFAULT NULL COMMENT '扩展',
  `FileSize` float DEFAULT NULL COMMENT '文件大小',
  `RDT` varchar(50) DEFAULT NULL COMMENT '记录日期',
  `Rec` varchar(50) DEFAULT NULL COMMENT '记录人',
  `RecName` varchar(50) DEFAULT NULL COMMENT '记录人名字',
  `MyNote` text COMMENT '备注',
  `NodeID` varchar(50) DEFAULT NULL COMMENT '节点ID',
  `IsRowLock` int(11) DEFAULT NULL COMMENT '是否锁定行',
  `Idx` int(11) DEFAULT NULL COMMENT '排序',
  `UploadGUID` varchar(500) DEFAULT NULL COMMENT '上传GUID',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='附件数据存储';

-- ----------------------------
-- Records of sys_frmattachmentdb
-- ----------------------------

-- ----------------------------
-- Table structure for `sys_frmbtn`
-- ----------------------------
DROP TABLE IF EXISTS `sys_frmbtn`;
CREATE TABLE `sys_frmbtn` (
  `MyPK` varchar(100) NOT NULL COMMENT 'MyPK - 主键',
  `FK_MapData` varchar(100) DEFAULT NULL COMMENT '表单ID',
  `Text` text COMMENT '标签',
  `X` float DEFAULT NULL COMMENT 'X',
  `Y` float DEFAULT NULL COMMENT 'Y',
  `IsView` int(11) DEFAULT NULL COMMENT '是否可见',
  `IsEnable` int(11) DEFAULT NULL COMMENT '是否起用',
  `BtnType` int(11) DEFAULT NULL COMMENT '类型',
  `UAC` int(11) DEFAULT NULL COMMENT '控制类型',
  `UACContext` text COMMENT '控制内容',
  `EventType` int(11) DEFAULT NULL COMMENT '事件类型',
  `EventContext` text COMMENT '事件内容',
  `MsgOK` varchar(500) DEFAULT NULL COMMENT '运行成功提示',
  `MsgErr` varchar(500) DEFAULT NULL COMMENT '运行失败提示',
  `GUID` varchar(128) DEFAULT NULL COMMENT 'GUID',
  `GroupID` varchar(50) DEFAULT NULL COMMENT '所在分组',
  `GroupIDText` varchar(50) DEFAULT NULL COMMENT '所在分组',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='按钮';

-- ----------------------------
-- Records of sys_frmbtn
-- ----------------------------

-- ----------------------------
-- Table structure for `sys_frmele`
-- ----------------------------
DROP TABLE IF EXISTS `sys_frmele`;
CREATE TABLE `sys_frmele` (
  `MyPK` varchar(100) NOT NULL COMMENT 'MyPK - 主键',
  `FK_MapData` varchar(100) DEFAULT NULL COMMENT '表单ID',
  `EleType` varchar(50) DEFAULT NULL COMMENT '类型',
  `EleID` varchar(50) DEFAULT NULL COMMENT '控件ID值(对部分控件有效)',
  `EleName` varchar(200) DEFAULT NULL COMMENT '控件名称(对部分控件有效)',
  `X` float DEFAULT NULL COMMENT 'X',
  `Y` float DEFAULT NULL COMMENT 'Y',
  `H` float DEFAULT NULL COMMENT 'H',
  `W` float DEFAULT NULL COMMENT 'W',
  `Tag1` varchar(50) DEFAULT NULL COMMENT 'Tag1',
  `Tag2` varchar(50) DEFAULT NULL COMMENT 'Tag2',
  `Tag3` varchar(50) DEFAULT NULL COMMENT 'Tag3',
  `Tag4` varchar(50) DEFAULT NULL COMMENT 'Tag4',
  `GUID` varchar(128) DEFAULT NULL COMMENT 'GUID',
  `IsEnable` int(11) DEFAULT NULL COMMENT '是否启用',
  `AtPara` text COMMENT 'AtPara',
  `URL` text COMMENT 'URL(支持ccbpm的表达式)',
  `GroupID` varchar(50) DEFAULT NULL COMMENT '所在分组',
  `GroupIDText` varchar(50) DEFAULT NULL COMMENT '所在分组',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='框架';

-- ----------------------------
-- Records of sys_frmele
-- ----------------------------

-- ----------------------------
-- Table structure for `sys_frmeledb`
-- ----------------------------
DROP TABLE IF EXISTS `sys_frmeledb`;
CREATE TABLE `sys_frmeledb` (
  `MyPK` varchar(100) NOT NULL COMMENT 'MyPK - 主键',
  `FK_MapData` varchar(100) DEFAULT NULL COMMENT 'FK_MapData',
  `EleID` varchar(50) DEFAULT NULL COMMENT 'EleID',
  `RefPKVal` varchar(50) DEFAULT NULL COMMENT 'RefPKVal',
  `FID` int(11) DEFAULT NULL COMMENT 'FID',
  `Tag1` varchar(1000) DEFAULT NULL COMMENT 'Tag1',
  `Tag2` varchar(1000) DEFAULT NULL COMMENT 'Tag2',
  `Tag3` varchar(1000) DEFAULT NULL COMMENT 'Tag3',
  `Tag4` varchar(1000) DEFAULT NULL COMMENT 'Tag4',
  `Tag5` varchar(1000) DEFAULT NULL COMMENT 'Tag5',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='表单元素扩展DB';

-- ----------------------------
-- Records of sys_frmeledb
-- ----------------------------

-- ----------------------------
-- Table structure for `sys_frmevent`
-- ----------------------------
DROP TABLE IF EXISTS `sys_frmevent`;
CREATE TABLE `sys_frmevent` (
  `MyPK` varchar(100) NOT NULL COMMENT 'MyPK - 主键',
  `FK_Event` varchar(400) DEFAULT NULL COMMENT '事件名称',
  `FK_MapData` varchar(100) DEFAULT NULL COMMENT 'FK_MapData',
  `DoType` int(11) DEFAULT NULL COMMENT '事件类型',
  `DoDoc` varchar(400) DEFAULT NULL COMMENT '执行内容',
  `MsgOK` varchar(400) DEFAULT NULL COMMENT '成功执行提示',
  `MsgError` varchar(400) DEFAULT NULL COMMENT '异常信息提示',
  `MsgCtrl` int(11) DEFAULT NULL COMMENT '   消息发送控制,枚举类型:0 不发送 1 按设置的下一步接受人自动发送（默认） 2 由本节点表单系统字段(IsSendEmail,IsSendSMS)来决定 3 由SDK开发者参数(IsSendEmail,IsSendSMS)来决定',
  `MailEnable` int(11) DEFAULT NULL COMMENT '是否启用邮件发送？(如果启用就要设置邮件模版，支持ccflow表达式。)',
  `MailTitle` varchar(200) DEFAULT NULL COMMENT '邮件标题模版',
  `MailDoc` text COMMENT '邮件内容模版',
  `SMSEnable` int(11) DEFAULT NULL COMMENT '是否启用短信发送？(如果启用就要设置短信模版，支持ccflow表达式。)',
  `SMSDoc` text COMMENT '短信内容模版',
  `MobilePushEnable` int(11) DEFAULT NULL COMMENT '是否推送到手机、pad端。',
  `AtPara` text COMMENT 'AtPara',
  `FK_Node` int(11) DEFAULT NULL COMMENT '节点ID',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='事件';

-- ----------------------------
-- Records of sys_frmevent
-- ----------------------------

-- ----------------------------
-- Table structure for `sys_frmimg`
-- ----------------------------
DROP TABLE IF EXISTS `sys_frmimg`;
CREATE TABLE `sys_frmimg` (
  `MyPK` varchar(100) NOT NULL COMMENT 'MyPK - 主键',
  `FK_MapData` varchar(100) DEFAULT NULL COMMENT 'FK_MapData',
  `ImgAppType` int(11) DEFAULT NULL COMMENT '应用类型',
  `X` float DEFAULT NULL COMMENT 'X',
  `Y` float DEFAULT NULL COMMENT 'Y',
  `H` float DEFAULT NULL COMMENT 'H',
  `W` float DEFAULT NULL COMMENT 'W',
  `ImgURL` varchar(200) DEFAULT NULL COMMENT '装饰图片URL',
  `ImgPath` varchar(200) DEFAULT NULL COMMENT '装饰图片路径',
  `LinkURL` varchar(200) DEFAULT NULL COMMENT '连接到URL',
  `LinkTarget` varchar(200) DEFAULT NULL COMMENT '连接目标',
  `GUID` varchar(128) DEFAULT NULL COMMENT 'GUID',
  `Tag0` varchar(500) DEFAULT NULL COMMENT '参数',
  `ImgSrcType` int(11) DEFAULT NULL COMMENT ' 装饰图片来源,枚举类型:0 本地 1 URL',
  `IsEdit` int(11) DEFAULT NULL COMMENT '是否可以编辑',
  `Name` varchar(500) DEFAULT NULL COMMENT '中文名称',
  `EnPK` varchar(500) DEFAULT NULL COMMENT '英文名称',
  `GroupID` varchar(50) DEFAULT NULL COMMENT '所在分组',
  `GroupIDText` varchar(50) DEFAULT NULL COMMENT '所在分组',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='装饰图片';

-- ----------------------------
-- Records of sys_frmimg
-- ----------------------------

-- ----------------------------
-- Table structure for `sys_frmimgath`
-- ----------------------------
DROP TABLE IF EXISTS `sys_frmimgath`;
CREATE TABLE `sys_frmimgath` (
  `MyPK` varchar(100) NOT NULL COMMENT 'MyPK - 主键',
  `FK_MapData` varchar(100) DEFAULT NULL COMMENT '表单ID',
  `CtrlID` varchar(200) DEFAULT NULL COMMENT '控件ID',
  `Name` varchar(200) DEFAULT NULL COMMENT '中文名称',
  `X` float DEFAULT NULL COMMENT 'X',
  `Y` float DEFAULT NULL COMMENT 'Y',
  `H` float DEFAULT NULL COMMENT 'H',
  `W` float DEFAULT NULL COMMENT 'W',
  `IsEdit` int(11) DEFAULT NULL COMMENT '是否可编辑',
  `IsRequired` int(11) DEFAULT NULL COMMENT '是否必填项',
  `GUID` varchar(128) DEFAULT NULL COMMENT 'GUID',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='图片附件';

-- ----------------------------
-- Records of sys_frmimgath
-- ----------------------------

-- ----------------------------
-- Table structure for `sys_frmimgathdb`
-- ----------------------------
DROP TABLE IF EXISTS `sys_frmimgathdb`;
CREATE TABLE `sys_frmimgathdb` (
  `MyPK` varchar(100) NOT NULL COMMENT 'MyPK - 主键',
  `FK_MapData` varchar(100) DEFAULT NULL COMMENT '附件ID',
  `FK_FrmImgAth` varchar(50) DEFAULT NULL COMMENT '图片附件编号',
  `RefPKVal` varchar(50) DEFAULT NULL COMMENT '实体主键',
  `FileFullName` varchar(700) DEFAULT NULL COMMENT '文件全路径',
  `FileName` varchar(500) DEFAULT NULL COMMENT '名称',
  `FileExts` varchar(50) DEFAULT NULL COMMENT '扩展名',
  `FileSize` float DEFAULT NULL COMMENT '文件大小',
  `RDT` varchar(50) DEFAULT NULL COMMENT '记录日期',
  `Rec` varchar(50) DEFAULT NULL COMMENT '记录人',
  `RecName` varchar(50) DEFAULT NULL COMMENT '记录人名字',
  `MyNote` text COMMENT '备注',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='剪切图片附件数据存储';

-- ----------------------------
-- Records of sys_frmimgathdb
-- ----------------------------

-- ----------------------------
-- Table structure for `sys_frmlab`
-- ----------------------------
DROP TABLE IF EXISTS `sys_frmlab`;
CREATE TABLE `sys_frmlab` (
  `MyPK` varchar(100) NOT NULL COMMENT 'MyPK - 主键',
  `FK_MapData` varchar(100) DEFAULT NULL COMMENT 'FK_MapData',
  `Text` text COMMENT 'Label',
  `X` float DEFAULT NULL COMMENT 'X',
  `Y` float DEFAULT NULL COMMENT 'Y',
  `FontSize` int(11) DEFAULT NULL COMMENT '字体大小',
  `FontColor` varchar(50) DEFAULT NULL COMMENT '颜色',
  `FontName` varchar(50) DEFAULT NULL COMMENT '字体名称',
  `FontStyle` varchar(200) DEFAULT NULL COMMENT '字体风格',
  `FontWeight` varchar(50) DEFAULT NULL COMMENT '字体宽度',
  `IsBold` int(11) DEFAULT NULL COMMENT '是否粗体',
  `IsItalic` int(11) DEFAULT NULL COMMENT '是否斜体',
  `GUID` varchar(128) DEFAULT NULL COMMENT 'GUID',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='标签';

-- ----------------------------
-- Records of sys_frmlab
-- ----------------------------

-- ----------------------------
-- Table structure for `sys_frmline`
-- ----------------------------
DROP TABLE IF EXISTS `sys_frmline`;
CREATE TABLE `sys_frmline` (
  `MyPK` varchar(100) NOT NULL COMMENT 'MyPK - 主键',
  `FK_MapData` varchar(100) DEFAULT NULL COMMENT '主表',
  `X1` float DEFAULT NULL COMMENT 'X1',
  `Y1` float DEFAULT NULL COMMENT 'Y1',
  `X2` float DEFAULT NULL COMMENT 'X2',
  `Y2` float DEFAULT NULL COMMENT 'Y2',
  `X` float DEFAULT NULL COMMENT 'X',
  `Y` float DEFAULT NULL COMMENT 'Y',
  `BorderWidth` float DEFAULT NULL COMMENT '宽度',
  `BorderColor` varchar(30) DEFAULT NULL COMMENT '颜色',
  `GUID` varchar(128) DEFAULT NULL COMMENT '初始的GUID',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='线';

-- ----------------------------
-- Records of sys_frmline
-- ----------------------------

-- ----------------------------
-- Table structure for `sys_frmlink`
-- ----------------------------
DROP TABLE IF EXISTS `sys_frmlink`;
CREATE TABLE `sys_frmlink` (
  `MyPK` varchar(100) NOT NULL COMMENT 'MyPK - 主键',
  `FK_MapData` varchar(100) DEFAULT NULL COMMENT '表单ID',
  `Text` varchar(500) DEFAULT NULL COMMENT '标签',
  `URL` varchar(500) DEFAULT NULL COMMENT 'URL',
  `Target` varchar(20) DEFAULT NULL COMMENT '连接目标(_blank,_parent,_self)',
  `X` float DEFAULT NULL COMMENT 'X',
  `Y` float DEFAULT NULL COMMENT 'Y',
  `FontSize` int(11) DEFAULT NULL COMMENT 'FontSize',
  `FontColor` varchar(50) DEFAULT NULL COMMENT 'FontColor',
  `FontName` varchar(50) DEFAULT NULL COMMENT 'FontName',
  `FontStyle` varchar(50) DEFAULT NULL COMMENT 'FontStyle',
  `IsBold` int(11) DEFAULT NULL COMMENT 'IsBold',
  `IsItalic` int(11) DEFAULT NULL COMMENT 'IsItalic',
  `GUID` varchar(128) DEFAULT NULL COMMENT 'GUID',
  `GroupID` varchar(50) DEFAULT NULL COMMENT '所在分组',
  `GroupIDText` varchar(50) DEFAULT NULL COMMENT '所在分组',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='超连接';

-- ----------------------------
-- Records of sys_frmlink
-- ----------------------------

-- ----------------------------
-- Table structure for `sys_frmrb`
-- ----------------------------
DROP TABLE IF EXISTS `sys_frmrb`;
CREATE TABLE `sys_frmrb` (
  `MyPK` varchar(100) NOT NULL COMMENT 'MyPK - 主键',
  `FK_MapData` varchar(100) DEFAULT NULL COMMENT '表单ID',
  `KeyOfEn` varchar(30) DEFAULT NULL COMMENT '字段',
  `EnumKey` varchar(30) DEFAULT NULL COMMENT '枚举值',
  `Lab` varchar(90) DEFAULT NULL COMMENT '标签',
  `IntKey` int(11) DEFAULT NULL COMMENT 'IntKey',
  `UIIsEnable` int(11) DEFAULT NULL COMMENT '是否启用',
  `X` float DEFAULT NULL COMMENT 'X',
  `Y` float DEFAULT NULL COMMENT 'Y',
  `Script` text COMMENT '要执行的脚本',
  `FieldsCfg` text COMMENT '配置信息@FieldName=Sta',
  `Tip` varchar(1000) DEFAULT NULL COMMENT '选择后提示的信息',
  `GUID` varchar(128) DEFAULT NULL COMMENT 'GUID',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='单选框';

-- ----------------------------
-- Records of sys_frmrb
-- ----------------------------

-- ----------------------------
-- Table structure for `sys_frmreportfield`
-- ----------------------------
DROP TABLE IF EXISTS `sys_frmreportfield`;
CREATE TABLE `sys_frmreportfield` (
  `MyPK` varchar(100) NOT NULL COMMENT 'MyPK - 主键',
  `FK_MapData` varchar(100) DEFAULT NULL COMMENT '表单编号',
  `KeyOfEn` varchar(100) DEFAULT NULL COMMENT '字段名',
  `Name` varchar(200) DEFAULT NULL COMMENT '显示中文名',
  `UIWidth` varchar(100) DEFAULT NULL COMMENT '宽度',
  `UIVisible` int(11) DEFAULT NULL COMMENT '是否显示',
  `Idx` int(11) DEFAULT NULL COMMENT '显示顺序',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='表单报表';

-- ----------------------------
-- Records of sys_frmreportfield
-- ----------------------------

-- ----------------------------
-- Table structure for `sys_frmrpt`
-- ----------------------------
DROP TABLE IF EXISTS `sys_frmrpt`;
CREATE TABLE `sys_frmrpt` (
  `No` varchar(20) NOT NULL COMMENT '编号 - 主键',
  `Name` varchar(50) DEFAULT NULL COMMENT '描述',
  `FK_MapData` varchar(100) DEFAULT NULL COMMENT '主表',
  `PTable` varchar(30) DEFAULT NULL COMMENT '物理表',
  `SQLOfColumn` varchar(300) DEFAULT NULL COMMENT '列的数据源',
  `SQLOfRow` varchar(300) DEFAULT NULL COMMENT '行数据源',
  `RowIdx` int(11) DEFAULT NULL COMMENT '位置',
  `GroupID` int(11) DEFAULT NULL COMMENT 'GroupID',
  `IsShowSum` int(11) DEFAULT NULL COMMENT 'IsShowSum',
  `IsShowIdx` int(11) DEFAULT NULL COMMENT 'IsShowIdx',
  `IsCopyNDData` int(11) DEFAULT NULL COMMENT 'IsCopyNDData',
  `IsHLDtl` int(11) DEFAULT NULL COMMENT '是否是合流汇总',
  `IsReadonly` int(11) DEFAULT NULL COMMENT 'IsReadonly',
  `IsShowTitle` int(11) DEFAULT NULL COMMENT 'IsShowTitle',
  `IsView` int(11) DEFAULT NULL COMMENT '是否可见',
  `IsExp` int(11) DEFAULT NULL COMMENT 'IsExp',
  `IsImp` int(11) DEFAULT NULL COMMENT 'IsImp',
  `IsInsert` int(11) DEFAULT NULL COMMENT 'IsInsert',
  `IsDelete` int(11) DEFAULT NULL COMMENT 'IsDelete',
  `IsUpdate` int(11) DEFAULT NULL COMMENT 'IsUpdate',
  `IsEnablePass` int(11) DEFAULT NULL COMMENT '是否启用通过审核功能?',
  `IsEnableAthM` int(11) DEFAULT NULL COMMENT '是否启用多附件',
  `IsEnableM2M` int(11) DEFAULT NULL COMMENT '是否启用M2M',
  `IsEnableM2MM` int(11) DEFAULT NULL COMMENT '是否启用M2M',
  `WhenOverSize` int(11) DEFAULT NULL COMMENT '  WhenOverSize,枚举类型:0 不处理 1 向下顺增行 2 次页显示',
  `DtlOpenType` int(11) DEFAULT NULL COMMENT '  数据开放类型,枚举类型:0 操作员 1 工作ID 2 流程ID',
  `DtlShowModel` int(11) DEFAULT NULL COMMENT '  显示格式,枚举类型:0 表格 1 卡片(自由模式) 2 卡片(傻瓜模式)',
  `X` float DEFAULT NULL COMMENT 'X',
  `Y` float DEFAULT NULL COMMENT 'Y',
  `H` float DEFAULT NULL COMMENT 'H',
  `W` float DEFAULT NULL COMMENT 'W',
  `FrmW` float DEFAULT NULL COMMENT 'FrmW',
  `FrmH` float DEFAULT NULL COMMENT 'FrmH',
  `MTR` varchar(3000) DEFAULT NULL COMMENT '多表头列',
  `GUID` varchar(128) DEFAULT NULL COMMENT 'GUID',
  PRIMARY KEY (`No`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='纬度报表';

-- ----------------------------
-- Records of sys_frmrpt
-- ----------------------------

-- ----------------------------
-- Table structure for `sys_frmsln`
-- ----------------------------
DROP TABLE IF EXISTS `sys_frmsln`;
CREATE TABLE `sys_frmsln` (
  `MyPK` varchar(100) NOT NULL COMMENT 'MyPK - 主键',
  `FK_Flow` varchar(4) DEFAULT NULL COMMENT '流程编号',
  `FK_Node` int(11) DEFAULT NULL COMMENT '节点',
  `FK_MapData` varchar(100) DEFAULT NULL COMMENT '表单ID',
  `KeyOfEn` varchar(200) DEFAULT NULL COMMENT '字段',
  `Name` varchar(500) DEFAULT NULL COMMENT '字段名',
  `EleType` varchar(20) DEFAULT NULL COMMENT '类型',
  `UIIsEnable` int(11) DEFAULT NULL COMMENT '是否可用',
  `UIVisible` int(11) DEFAULT NULL COMMENT '是否可见',
  `IsSigan` int(11) DEFAULT NULL COMMENT '是否签名',
  `IsNotNull` int(11) DEFAULT NULL COMMENT '是否为空',
  `RegularExp` varchar(500) DEFAULT NULL COMMENT '正则表达式',
  `IsWriteToFlowTable` int(11) DEFAULT NULL COMMENT '是否写入流程表',
  `DefVal` varchar(200) DEFAULT NULL COMMENT '默认值',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='表单字段方案';

-- ----------------------------
-- Records of sys_frmsln
-- ----------------------------

-- ----------------------------
-- Table structure for `sys_glovar`
-- ----------------------------
DROP TABLE IF EXISTS `sys_glovar`;
CREATE TABLE `sys_glovar` (
  `No` varchar(30) NOT NULL COMMENT '键 - 主键',
  `Name` varchar(120) DEFAULT NULL COMMENT '名称',
  `Val` varchar(120) DEFAULT NULL COMMENT '值',
  `GroupKey` varchar(120) DEFAULT NULL COMMENT '分组值',
  `Note` text COMMENT '说明',
  PRIMARY KEY (`No`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='全局变量';

-- ----------------------------
-- Records of sys_glovar
-- ----------------------------
INSERT INTO `sys_glovar` VALUES ('0', '选择系统约定默认值', null, 'DefVal', null);
INSERT INTO `sys_glovar` VALUES ('@CurrWorker', '当前工作可处理人员', null, 'DefVal', null);
INSERT INTO `sys_glovar` VALUES ('@FK_ND', '当前年度', null, 'DefVal', null);
INSERT INTO `sys_glovar` VALUES ('@FK_YF', '当前月份', null, 'DefVal', null);
INSERT INTO `sys_glovar` VALUES ('@WebUser.FK_Dept', '登陆人员部门编号', null, 'DefVal', null);
INSERT INTO `sys_glovar` VALUES ('@WebUser.FK_DeptFullName', '登陆人员部门全称', null, 'DefVal', null);
INSERT INTO `sys_glovar` VALUES ('@WebUser.FK_DeptName', '登陆人员部门名称', null, 'DefVal', null);
INSERT INTO `sys_glovar` VALUES ('@WebUser.Name', '登陆人员名称', null, 'DefVal', null);
INSERT INTO `sys_glovar` VALUES ('@WebUser.No', '登陆人员账号', null, 'DefVal', null);
INSERT INTO `sys_glovar` VALUES ('@yyyy年MM月dd日', '当前日期(yyyy年MM月dd日)', null, 'DefVal', null);
INSERT INTO `sys_glovar` VALUES ('@yyyy年MM月dd日HH时mm分', '当前日期(yyyy年MM月dd日HH时mm分)', null, 'DefVal', null);
INSERT INTO `sys_glovar` VALUES ('@yy年MM月dd日', '当前日期(yy年MM月dd日)', null, 'DefVal', null);
INSERT INTO `sys_glovar` VALUES ('@yy年MM月dd日HH时mm分', '当前日期(yy年MM月dd日HH时mm分)', null, 'DefVal', null);

-- ----------------------------
-- Table structure for `sys_groupenstemplate`
-- ----------------------------
DROP TABLE IF EXISTS `sys_groupenstemplate`;
CREATE TABLE `sys_groupenstemplate` (
  `OID` int(11) NOT NULL COMMENT 'OID - 主键',
  `EnName` varchar(500) DEFAULT NULL COMMENT '表称',
  `Name` varchar(500) DEFAULT NULL COMMENT '报表名',
  `EnsName` varchar(90) DEFAULT NULL COMMENT '报表类名',
  `OperateCol` varchar(90) DEFAULT NULL COMMENT '操作属性',
  `Attrs` varchar(90) DEFAULT NULL COMMENT '运算属性',
  `Rec` varchar(90) DEFAULT NULL COMMENT '记录人',
  PRIMARY KEY (`OID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='报表模板';

-- ----------------------------
-- Records of sys_groupenstemplate
-- ----------------------------

-- ----------------------------
-- Table structure for `sys_groupfield`
-- ----------------------------
DROP TABLE IF EXISTS `sys_groupfield`;
CREATE TABLE `sys_groupfield` (
  `OID` int(11) NOT NULL COMMENT 'OID - 主键',
  `Lab` varchar(500) DEFAULT NULL COMMENT '标签',
  `EnName` varchar(200) DEFAULT NULL COMMENT '类',
  `Idx` int(11) DEFAULT NULL COMMENT '顺序号',
  `GUID` varchar(128) DEFAULT NULL COMMENT 'GUID',
  `CtrlType` varchar(50) DEFAULT NULL COMMENT '控件类型',
  `CtrlID` varchar(500) DEFAULT NULL COMMENT '控件ID',
  `AtPara` varchar(3000) DEFAULT NULL COMMENT 'AtPara',
  PRIMARY KEY (`OID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='傻瓜表单分组';

-- ----------------------------
-- Records of sys_groupfield
-- ----------------------------
INSERT INTO `sys_groupfield` VALUES ('101', '基础信息', '', '1', '', '', '', '');

-- ----------------------------
-- Table structure for `sys_m2m`
-- ----------------------------
DROP TABLE IF EXISTS `sys_m2m`;
CREATE TABLE `sys_m2m` (
  `MyPK` varchar(100) NOT NULL COMMENT 'MyPK - 主键',
  `FK_MapData` varchar(100) DEFAULT NULL COMMENT 'FK_MapData',
  `M2MNo` varchar(20) DEFAULT NULL COMMENT 'M2MNo',
  `EnOID` int(11) DEFAULT NULL COMMENT '实体OID',
  `DtlObj` varchar(20) DEFAULT NULL COMMENT 'DtlObj(对于m2mm有效)',
  `Doc` text COMMENT '内容',
  `ValsName` text COMMENT 'ValsName',
  `ValsSQL` text COMMENT 'ValsSQL',
  `NumSelected` int(11) DEFAULT NULL COMMENT '选择数',
  `GUID` varchar(128) DEFAULT NULL COMMENT 'GUID',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='M2M数据存储';

-- ----------------------------
-- Records of sys_m2m
-- ----------------------------

-- ----------------------------
-- Table structure for `sys_mapattr`
-- ----------------------------
DROP TABLE IF EXISTS `sys_mapattr`;
CREATE TABLE `sys_mapattr` (
  `MyPK` varchar(200) NOT NULL COMMENT '实体标识 - 主键',
  `FK_MapData` varchar(100) DEFAULT NULL COMMENT '实体标识',
  `Name` varchar(200) DEFAULT NULL COMMENT '描述',
  `KeyOfEn` varchar(200) DEFAULT NULL COMMENT '属性',
  `MyDataType` int(11) DEFAULT NULL COMMENT '数据类型',
  `UIVisible` int(11) DEFAULT NULL COMMENT '是否可见',
  `UIIsEnable` int(11) DEFAULT NULL COMMENT '是否启用',
  `DefVal` varchar(4000) DEFAULT NULL COMMENT '默认值',
  `Tip` varchar(4000) DEFAULT NULL COMMENT '激活提示',
  `ColSpan` int(11) DEFAULT NULL COMMENT '单元格数量',
  `GroupID` varchar(50) DEFAULT NULL COMMENT '显示的分组',
  `GroupIDText` varchar(50) DEFAULT NULL COMMENT '显示的分组',
  `UIIsInput` int(11) DEFAULT NULL COMMENT '是否必填字段',
  `DefValText` varchar(50) DEFAULT NULL COMMENT '默认值（选中）',
  `UIContralType` int(11) DEFAULT NULL COMMENT '控件',
  `RBShowModel` int(11) DEFAULT NULL COMMENT ' 单选按钮的展现方式,枚举类型:0 竖向 3 横向',
  `UIWidth` float(50,2) DEFAULT NULL COMMENT '宽度',
  `UIHeight` float(50,2) DEFAULT NULL COMMENT '高度',
  `UIBindKey` varchar(100) DEFAULT NULL COMMENT '绑定的信息',
  `IsEnableJS` int(11) DEFAULT NULL COMMENT '是否启用JS高级设置？',
  `ExtIsSum` int(11) DEFAULT NULL COMMENT '是否显示合计(对从表有效)',
  `LGType` int(11) DEFAULT NULL COMMENT '   逻辑类型,枚举类型:0 普通 1 枚举 2 外键 3 打开系统页面',
  `ExtDefVal` varchar(50) DEFAULT NULL COMMENT '系统默认值',
  `ExtDefValText` varchar(50) DEFAULT NULL COMMENT '系统默认值',
  `MinLen` float(50,2) DEFAULT NULL COMMENT '最小长度',
  `MaxLen` float(50,2) DEFAULT NULL COMMENT '最大长度',
  `ExtRows` float(50,2) DEFAULT NULL COMMENT '文本框行数(决定高度)',
  `IsRichText` int(11) DEFAULT NULL COMMENT '富文本',
  `IsSupperText` int(11) DEFAULT NULL COMMENT '富文本',
  `IsSigan` int(11) DEFAULT NULL COMMENT '签字？',
  `UIRefKey` varchar(30) DEFAULT NULL COMMENT '绑定的Key',
  `UIRefKeyText` varchar(30) DEFAULT NULL COMMENT '绑定的Text',
  `UIIsLine` int(11) DEFAULT NULL COMMENT '是否单独栏显示',
  `FontSize` int(11) DEFAULT NULL COMMENT '富文本',
  `X` float(50,2) DEFAULT NULL COMMENT 'X',
  `Y` float(50,2) DEFAULT NULL COMMENT 'Y',
  `GUID` varchar(128) DEFAULT NULL COMMENT 'GUID',
  `Tag` varchar(100) DEFAULT NULL COMMENT '标识（存放临时数据）',
  `EditType` int(11) DEFAULT NULL COMMENT '编辑类型',
  `IsEnableInAPP` int(11) DEFAULT NULL COMMENT '是否在移动端中显示',
  `Idx` int(11) DEFAULT NULL COMMENT '序号',
  `AtPara` text COMMENT 'AtPara',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='实体属性';

-- ----------------------------
-- Records of sys_mapattr
-- ----------------------------
INSERT INTO `sys_mapattr` VALUES ('_OID', '', 'OID', 'OID', '2', '0', '0', '0', '', '1', '0', null, '0', '0', '0', '0', '100.00', '23.00', '', '0', '0', '0', '0', '0', '0.00', '300.00', '1.00', '0', '0', '0', '', '', '0', '0', '5.00', '5.00', '', '', '2', '1', '999', '');
INSERT INTO `sys_mapattr` VALUES ('_RDT', '', '更新时间', 'RDT', '7', '0', '0', '@RDT', '', '1', '0', null, '0', '0', '0', '0', '100.00', '23.00', '', '0', '0', '0', '0', '0', '0.00', '300.00', '1.00', '0', '0', '0', '', '', '0', '0', '5.00', '5.00', '', '1', '1', '1', '999', '');

-- ----------------------------
-- Table structure for `sys_mapdata`
-- ----------------------------
DROP TABLE IF EXISTS `sys_mapdata`;
CREATE TABLE `sys_mapdata` (
  `No` varchar(200) NOT NULL COMMENT 'null - 主键',
  `Name` varchar(500) DEFAULT NULL COMMENT 'null',
  `FormEventEntity` varchar(100) DEFAULT NULL COMMENT '事件实体',
  `EnPK` varchar(200) DEFAULT NULL COMMENT '实体主键',
  `PTable` varchar(500) DEFAULT NULL COMMENT '物理表',
  `PTableModel` int(11) DEFAULT NULL COMMENT '表存储模式',
  `Url` varchar(500) DEFAULT NULL COMMENT 'URL连接',
  `Dtls` varchar(500) DEFAULT NULL COMMENT '从表',
  `FrmW` int(11) DEFAULT NULL COMMENT '宽度',
  `FrmH` int(11) DEFAULT NULL COMMENT '高度',
  `TableCol` int(11) DEFAULT NULL COMMENT '表单显示列数',
  `TableWidth` int(11) DEFAULT NULL COMMENT '傻瓜表单宽度',
  `Tag` varchar(500) DEFAULT NULL COMMENT 'Tag',
  `FK_FrmSort` varchar(500) DEFAULT NULL COMMENT '表单类别',
  `FK_FormTree` varchar(500) DEFAULT NULL COMMENT '表单类别,外键:对应物理表:Sys_FormTree,表描述:表单树',
  `FrmType` int(11) DEFAULT NULL COMMENT '表单类型',
  `AppType` int(11) DEFAULT NULL COMMENT '应用类型',
  `DBSrc` varchar(100) DEFAULT NULL COMMENT '数据源,外键:对应物理表:Sys_SFDBSrc,表描述:数据源',
  `BodyAttr` varchar(100) DEFAULT NULL COMMENT '表单Body属性',
  `Note` varchar(4000) DEFAULT NULL COMMENT '备注',
  `Designer` varchar(500) DEFAULT NULL COMMENT '设计者',
  `DesignerUnit` varchar(500) DEFAULT NULL COMMENT '单位',
  `DesignerContact` varchar(500) DEFAULT NULL COMMENT '联系方式',
  `Idx` int(11) DEFAULT NULL COMMENT '顺序号',
  `GUID` varchar(128) DEFAULT NULL COMMENT 'GUID',
  `Ver` varchar(30) DEFAULT NULL COMMENT '版本号',
  `FlowCtrls` varchar(200) DEFAULT NULL COMMENT '流程控件',
  `AtPara` text COMMENT 'AtPara',
  `OfficeOpenLab` varchar(50) DEFAULT NULL COMMENT '打开本地标签',
  `OfficeOpenEnable` int(11) DEFAULT NULL COMMENT '是否启用',
  `OfficeOpenTemplateLab` varchar(50) DEFAULT NULL COMMENT '打开模板标签',
  `OfficeOpenTemplateEnable` int(11) DEFAULT NULL COMMENT '是否启用',
  `OfficeSaveLab` varchar(50) DEFAULT NULL COMMENT '保存标签',
  `OfficeSaveEnable` int(11) DEFAULT NULL COMMENT '是否启用',
  `OfficeAcceptLab` varchar(50) DEFAULT NULL COMMENT '接受修订标签',
  `OfficeAcceptEnable` int(11) DEFAULT NULL COMMENT '是否启用',
  `OfficeRefuseLab` varchar(50) DEFAULT NULL COMMENT '拒绝修订标签',
  `OfficeRefuseEnable` int(11) DEFAULT NULL COMMENT '是否启用',
  `OfficeOverLab` varchar(50) DEFAULT NULL COMMENT '套红按钮标签',
  `OfficeOverEnable` int(11) DEFAULT NULL COMMENT '是否启用',
  `OfficeMarksEnable` int(11) DEFAULT NULL COMMENT '是否查看用户留痕',
  `OfficePrintLab` varchar(50) DEFAULT NULL COMMENT '打印按钮标签',
  `OfficePrintEnable` int(11) DEFAULT NULL COMMENT '是否启用',
  `OfficeSealLab` varchar(50) DEFAULT NULL COMMENT '签章按钮标签',
  `OfficeSealEnable` int(11) DEFAULT NULL COMMENT '是否启用',
  `OfficeInsertFlowLab` varchar(50) DEFAULT NULL COMMENT '插入流程标签',
  `OfficeInsertFlowEnable` int(11) DEFAULT NULL COMMENT '是否启用',
  `OfficeNodeInfo` int(11) DEFAULT NULL COMMENT '是否记录节点信息',
  `OfficeReSavePDF` int(11) DEFAULT NULL COMMENT '是否该自动保存为PDF',
  `OfficeDownLab` varchar(50) DEFAULT NULL COMMENT '下载按钮标签',
  `OfficeDownEnable` int(11) DEFAULT NULL COMMENT '是否启用',
  `OfficeIsMarks` int(11) DEFAULT NULL COMMENT '是否进入留痕模式',
  `OfficeTemplate` varchar(100) DEFAULT NULL COMMENT '指定文档模板',
  `OfficeIsParent` int(11) DEFAULT NULL COMMENT '是否使用父流程的文档',
  `OfficeTHEnable` int(11) DEFAULT NULL COMMENT '是否自动套红',
  `OfficeTHTemplate` varchar(200) DEFAULT NULL COMMENT '自动套红模板',
  `FK_Flow` varchar(50) DEFAULT NULL COMMENT '独立表单属性:FK_Flow',
  `RightViewWay` int(11) DEFAULT NULL COMMENT '报表查看权限控制方式',
  `RightViewTag` text COMMENT '报表查看权限控制Tag',
  `RightDeptWay` int(11) DEFAULT NULL COMMENT '部门数据查看控制方式',
  `RightDeptTag` text COMMENT '部门数据查看控制Tag',
  `DBURL` int(11) DEFAULT NULL COMMENT 'DBURL',
  `TableHeight` int(11) DEFAULT NULL COMMENT '傻瓜表单高度',
  `TemplaterVer` varchar(30) DEFAULT NULL COMMENT '模版编号',
  `MyFileName` varchar(100) DEFAULT NULL COMMENT '表单模版',
  `MyFilePath` varchar(100) DEFAULT NULL COMMENT 'MyFilePath',
  `MyFileExt` varchar(10) DEFAULT NULL COMMENT 'MyFileExt',
  `WebPath` varchar(200) DEFAULT NULL COMMENT 'WebPath',
  `MyFileH` int(11) DEFAULT NULL COMMENT 'MyFileH',
  `MyFileW` int(11) DEFAULT NULL COMMENT 'MyFileW',
  `MyFileSize` float(50,2) DEFAULT NULL COMMENT 'MyFileSize',
  `FormJson` mediumblob,
  PRIMARY KEY (`No`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='系统表单';

-- ----------------------------
-- Records of sys_mapdata
-- ----------------------------

-- ----------------------------
-- Table structure for `sys_mapdtl`
-- ----------------------------
DROP TABLE IF EXISTS `sys_mapdtl`;
CREATE TABLE `sys_mapdtl` (
  `No` varchar(100) NOT NULL COMMENT '编号 - 主键',
  `Name` varchar(200) DEFAULT NULL COMMENT '名称',
  `Alias` varchar(200) DEFAULT NULL COMMENT '别名',
  `FK_MapData` varchar(100) DEFAULT NULL COMMENT '表单ID',
  `PTable` varchar(200) DEFAULT NULL COMMENT '存储表',
  `GroupField` varchar(300) DEFAULT NULL COMMENT '分组字段',
  `RefPK` varchar(100) DEFAULT NULL COMMENT '关联的主键',
  `FEBD` varchar(100) DEFAULT NULL COMMENT '事件类实体类',
  `Model` int(11) DEFAULT NULL COMMENT ' 工作模式,枚举类型:0 普通 1 固定行',
  `RowsOfList` int(11) DEFAULT NULL COMMENT '初始化行数',
  `IsEnableGroupField` int(11) DEFAULT NULL COMMENT '是否启用分组字段',
  `IsShowSum` int(11) DEFAULT NULL COMMENT '是否显示合计？',
  `IsShowIdx` int(11) DEFAULT NULL COMMENT '是否显示序号？',
  `IsCopyNDData` int(11) DEFAULT NULL COMMENT '是否允许copy节点数据',
  `IsHLDtl` int(11) DEFAULT NULL COMMENT '是否是合流汇总',
  `IsReadonly` int(11) DEFAULT NULL COMMENT '是否只读？',
  `IsShowTitle` int(11) DEFAULT NULL COMMENT '是否显示标题？',
  `IsView` int(11) DEFAULT NULL COMMENT '是否可见？',
  `IsInsert` int(11) DEFAULT NULL COMMENT '是否可以插入行？',
  `IsDelete` int(11) DEFAULT NULL COMMENT '是否可以删除行？',
  `IsUpdate` int(11) DEFAULT NULL COMMENT '是否可以更新？',
  `IsEnablePass` int(11) DEFAULT NULL COMMENT '是否启用通过审核功能?',
  `IsEnableAthM` int(11) DEFAULT NULL COMMENT '是否启用多附件',
  `IsEnableM2M` int(11) DEFAULT NULL COMMENT '是否启用M2M',
  `IsEnableM2MM` int(11) DEFAULT NULL COMMENT '是否启用M2M',
  `WhenOverSize` int(11) DEFAULT NULL COMMENT '  超出行数,枚举类型:0 不处理 1 向下顺增行 2 次页显示',
  `DtlOpenType` int(11) DEFAULT NULL COMMENT '  数据开放类型,枚举类型:0 操作员 1 工作ID 2 流程ID',
  `ListShowModel` int(11) DEFAULT NULL COMMENT ' 列表数据显示格式,枚举类型:0 表格 1 卡片',
  `EditModel` int(11) DEFAULT NULL COMMENT '  行数据显示格式,枚举类型:0 无 1 傻瓜表单 2 自由表单',
  `X` float DEFAULT NULL COMMENT '距左',
  `Y` float DEFAULT NULL COMMENT '距上',
  `H` float DEFAULT NULL COMMENT '高度',
  `W` float DEFAULT NULL COMMENT '宽度',
  `FrmW` float DEFAULT NULL COMMENT '表单宽度',
  `FrmH` float DEFAULT NULL COMMENT '表单高度',
  `MTR` varchar(4000) DEFAULT NULL COMMENT '请书写html标记,以《TR》开头，以《/TR》结尾。',
  `FilterSQLExp` varchar(200) DEFAULT NULL COMMENT '过滤数据SQL表达式',
  `GUID` varchar(128) DEFAULT NULL COMMENT 'GUID',
  `FK_Node` int(11) DEFAULT NULL COMMENT '节点(用户独立表单权限控制)',
  `IsExp` int(11) DEFAULT NULL COMMENT '是否可以导出？(导出到Excel,Txt,html类型文件.)',
  `ImpModel` int(11) DEFAULT NULL COMMENT '   导入方式,枚举类型:0 不导入 1 按SQL设置导入 2 按JSON模式导入 3 按照xls文件模版导入',
  `ImpSQLSearch` varchar(4000) DEFAULT NULL COMMENT '查询SQL(SQL里必须包含@Key关键字.)',
  `ImpSQLInit` varchar(4000) DEFAULT NULL COMMENT '初始化SQL(初始化表格的时候的SQL数据,可以为空)',
  `ImpSQLFullOneRow` varchar(4000) DEFAULT NULL COMMENT '数据填充一行数据的SQL(必须包含@Key关键字,为选择的主键)',
  `ImpSQLFull` varchar(500) DEFAULT NULL COMMENT '填充数据',
  `ColAutoExp` varchar(200) DEFAULT NULL COMMENT '列自动计算',
  `ShowCols` varchar(500) DEFAULT NULL COMMENT '显示的列',
  `PTableModel` int(11) DEFAULT NULL COMMENT '行数据显示格式',
  `AtPara` varchar(300) DEFAULT NULL COMMENT 'AtPara',
  `IsEnableLink` int(11) DEFAULT NULL COMMENT '是否启用超链接',
  `LinkLabel` varchar(50) DEFAULT NULL COMMENT '超连接标签',
  `LinkTarget` varchar(10) DEFAULT NULL COMMENT '连接目标',
  `LinkUrl` varchar(200) DEFAULT NULL COMMENT '连接URL',
  `SubThreadWorker` varchar(50) DEFAULT NULL COMMENT '子线程处理人字段',
  `SubThreadWorkerText` varchar(50) DEFAULT NULL COMMENT '子线程处理人字段',
  PRIMARY KEY (`No`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='明细';

-- ----------------------------
-- Records of sys_mapdtl
-- ----------------------------

-- ----------------------------
-- Table structure for `sys_mapext`
-- ----------------------------
DROP TABLE IF EXISTS `sys_mapext`;
CREATE TABLE `sys_mapext` (
  `MyPK` varchar(100) NOT NULL COMMENT 'MyPK - 主键',
  `FK_MapData` varchar(100) DEFAULT NULL COMMENT '主表',
  `ExtType` varchar(30) DEFAULT NULL COMMENT '类型',
  `DoWay` int(11) DEFAULT NULL COMMENT '执行方式',
  `AttrOfOper` varchar(30) DEFAULT NULL COMMENT '操作的Attr',
  `AttrsOfActive` varchar(900) DEFAULT NULL COMMENT '激活的字段',
  `FK_DBSrc` varchar(100) DEFAULT NULL COMMENT '数据源',
  `Doc` text COMMENT '内容',
  `Tag` varchar(2000) DEFAULT NULL COMMENT 'Tag',
  `Tag1` varchar(2000) DEFAULT NULL COMMENT 'Tag1',
  `Tag2` varchar(2000) DEFAULT NULL COMMENT 'Tag2',
  `Tag3` varchar(2000) DEFAULT NULL COMMENT 'Tag3',
  `Tag4` varchar(2000) DEFAULT NULL COMMENT 'Tag4',
  `AtPara` varchar(2000) DEFAULT NULL COMMENT '参数',
  `DBType` int(11) DEFAULT NULL COMMENT '数据类型',
  `DBSrc` varchar(20) DEFAULT NULL COMMENT '数据源',
  `H` int(11) DEFAULT NULL COMMENT '高度',
  `W` int(11) DEFAULT NULL COMMENT '宽度',
  `PRI` int(11) DEFAULT NULL COMMENT 'PRI',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='业务逻辑';

-- ----------------------------
-- Records of sys_mapext
-- ----------------------------

-- ----------------------------
-- Table structure for `sys_mapframe`
-- ----------------------------
DROP TABLE IF EXISTS `sys_mapframe`;
CREATE TABLE `sys_mapframe` (
  `MyPK` varchar(100) NOT NULL COMMENT 'MyPK - 主键',
  `FK_MapData` varchar(100) DEFAULT NULL COMMENT '表单ID',
  `Name` varchar(200) DEFAULT NULL COMMENT '名称',
  `URL` varchar(3000) DEFAULT NULL COMMENT 'URL',
  `Y` varchar(20) DEFAULT NULL COMMENT 'Y',
  `X` varchar(20) DEFAULT NULL COMMENT 'x',
  `W` varchar(20) DEFAULT NULL COMMENT '宽度',
  `H` varchar(20) DEFAULT NULL COMMENT '高度',
  `IsAutoSize` int(11) DEFAULT NULL COMMENT '是否自动设置大小',
  `EleType` varchar(50) DEFAULT NULL COMMENT '类型',
  `GUID` varchar(128) DEFAULT NULL COMMENT 'GUID',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='框架';

-- ----------------------------
-- Records of sys_mapframe
-- ----------------------------

-- ----------------------------
-- Table structure for `sys_mapm2m`
-- ----------------------------
DROP TABLE IF EXISTS `sys_mapm2m`;
CREATE TABLE `sys_mapm2m` (
  `MyPK` varchar(100) NOT NULL COMMENT 'MyPK - 主键',
  `FK_MapData` varchar(100) DEFAULT NULL COMMENT '主表',
  `NoOfObj` varchar(20) DEFAULT NULL COMMENT '编号',
  `Name` varchar(200) DEFAULT NULL COMMENT '名称',
  `DBOfLists` text COMMENT '列表数据源(对一对多对多模式有效）',
  `DBOfObjs` text COMMENT 'DBOfObjs',
  `DBOfGroups` text COMMENT 'DBOfGroups',
  `H` float DEFAULT NULL COMMENT 'H',
  `W` float DEFAULT NULL COMMENT 'W',
  `X` float DEFAULT NULL COMMENT 'X',
  `Y` float DEFAULT NULL COMMENT 'Y',
  `ShowWay` int(11) DEFAULT NULL COMMENT '显示方式',
  `M2MType` int(11) DEFAULT NULL COMMENT '类型',
  `RowIdx` int(11) DEFAULT NULL COMMENT '位置',
  `GroupID` int(11) DEFAULT NULL COMMENT '分组ID',
  `Cols` int(11) DEFAULT NULL COMMENT '记录呈现列数',
  `IsDelete` int(11) DEFAULT NULL COMMENT '可删除否',
  `IsInsert` int(11) DEFAULT NULL COMMENT '可插入否',
  `IsCheckAll` int(11) DEFAULT NULL COMMENT '是否显示选择全部',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='多选';

-- ----------------------------
-- Records of sys_mapm2m
-- ----------------------------

-- ----------------------------
-- Table structure for `sys_rptdept`
-- ----------------------------
DROP TABLE IF EXISTS `sys_rptdept`;
CREATE TABLE `sys_rptdept` (
  `FK_Rpt` varchar(15) NOT NULL COMMENT '报表 - 主键',
  `FK_Dept` varchar(100) NOT NULL COMMENT '部门,主外键:对应物理表:Port_Dept,表描述:部门',
  PRIMARY KEY (`FK_Rpt`,`FK_Dept`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='报表部门对应信息';

-- ----------------------------
-- Records of sys_rptdept
-- ----------------------------

-- ----------------------------
-- Table structure for `sys_rptemp`
-- ----------------------------
DROP TABLE IF EXISTS `sys_rptemp`;
CREATE TABLE `sys_rptemp` (
  `FK_Rpt` varchar(15) NOT NULL COMMENT '报表 - 主键',
  `FK_Emp` varchar(100) NOT NULL COMMENT '人员,主外键:对应物理表:Port_Emp,表描述:用户',
  PRIMARY KEY (`FK_Rpt`,`FK_Emp`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='报表人员对应信息';

-- ----------------------------
-- Records of sys_rptemp
-- ----------------------------

-- ----------------------------
-- Table structure for `sys_rptstation`
-- ----------------------------
DROP TABLE IF EXISTS `sys_rptstation`;
CREATE TABLE `sys_rptstation` (
  `FK_Rpt` varchar(15) NOT NULL COMMENT '报表 - 主键',
  `FK_Station` varchar(100) NOT NULL COMMENT '岗位,主外键:对应物理表:Port_Station,表描述:岗位',
  PRIMARY KEY (`FK_Rpt`,`FK_Station`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='报表岗位对应信息';

-- ----------------------------
-- Records of sys_rptstation
-- ----------------------------

-- ----------------------------
-- Table structure for `sys_rpttemplate`
-- ----------------------------
DROP TABLE IF EXISTS `sys_rpttemplate`;
CREATE TABLE `sys_rpttemplate` (
  `MyPK` varchar(100) NOT NULL COMMENT 'MyPK - 主键',
  `EnsName` varchar(500) DEFAULT NULL COMMENT '类名',
  `FK_Emp` varchar(20) DEFAULT NULL COMMENT '操作员',
  `D1` varchar(90) DEFAULT NULL COMMENT 'D1',
  `D2` varchar(90) DEFAULT NULL COMMENT 'D2',
  `D3` varchar(90) DEFAULT NULL COMMENT 'D3',
  `AlObjs` varchar(90) DEFAULT NULL COMMENT '要分析的对象',
  `Height` int(11) DEFAULT NULL COMMENT 'Height',
  `Width` int(11) DEFAULT NULL COMMENT 'Width',
  `IsSumBig` int(11) DEFAULT NULL COMMENT '是否显示大合计',
  `IsSumLittle` int(11) DEFAULT NULL COMMENT '是否显示小合计',
  `IsSumRight` int(11) DEFAULT NULL COMMENT '是否显示右合计',
  `PercentModel` int(11) DEFAULT NULL COMMENT '比率显示方式',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='报表模板';

-- ----------------------------
-- Records of sys_rpttemplate
-- ----------------------------

-- ----------------------------
-- Table structure for `sys_serial`
-- ----------------------------
DROP TABLE IF EXISTS `sys_serial`;
CREATE TABLE `sys_serial` (
  `CfgKey` varchar(100) NOT NULL COMMENT 'CfgKey - 主键',
  `IntVal` int(11) DEFAULT NULL COMMENT '属性',
  PRIMARY KEY (`CfgKey`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='序列号';

-- ----------------------------
-- Records of sys_serial
-- ----------------------------
INSERT INTO `sys_serial` VALUES ('BP.WF.Template.FlowSort', '102');
INSERT INTO `sys_serial` VALUES ('OID', '101');
INSERT INTO `sys_serial` VALUES ('UpdataCCFlowVer', '326153017');
INSERT INTO `sys_serial` VALUES ('Ver', '20171117');

-- ----------------------------
-- Table structure for `sys_sfdbsrc`
-- ----------------------------
DROP TABLE IF EXISTS `sys_sfdbsrc`;
CREATE TABLE `sys_sfdbsrc` (
  `No` varchar(20) NOT NULL COMMENT '数据源编号(必须是英文) - 主键',
  `Name` varchar(30) DEFAULT NULL COMMENT '数据源名称',
  `DBSrcType` int(11) DEFAULT NULL COMMENT '      数据源类型,枚举类型:0 应用系统主数据库(默认) 1 SQLServer数据库 2 Oracle数据库 3 MySQL数据库 4 Informix数据库 50 Dubbo服务 100 WebService数据源',
  `UserID` varchar(30) DEFAULT NULL COMMENT '数据库登录用户ID',
  `Password` varchar(30) DEFAULT NULL COMMENT '数据库登录用户密码',
  `IP` varchar(500) DEFAULT NULL COMMENT 'IP地址/数据库实例名',
  `DBName` varchar(30) DEFAULT NULL COMMENT '数据库名称/Oracle保持为空',
  PRIMARY KEY (`No`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='数据源';

-- ----------------------------
-- Records of sys_sfdbsrc
-- ----------------------------
INSERT INTO `sys_sfdbsrc` VALUES ('local', '本机数据源(默认)', '0', '', '', '', '');

-- ----------------------------
-- Table structure for `sys_sftable`
-- ----------------------------
DROP TABLE IF EXISTS `sys_sftable`;
CREATE TABLE `sys_sftable` (
  `No` varchar(200) NOT NULL COMMENT '表英文名称 - 主键',
  `Name` varchar(200) DEFAULT NULL COMMENT '表中文名称',
  `SrcType` int(11) DEFAULT NULL COMMENT '    数据表类型,枚举类型:0 本地的类 1 创建表 2 表或视图 3 SQL查询表 4 WebServices',
  `CodeStruct` int(11) DEFAULT NULL COMMENT '  字典表类型,枚举类型:0 普通的编码表(具有No,Name) 1 树结构(具有No,Name,ParentNo) 2 行政机构编码表(编码以两位编号标识级次树形关系)',
  `FK_Val` varchar(200) DEFAULT NULL COMMENT '默认创建的字段名',
  `TableDesc` varchar(200) DEFAULT NULL COMMENT '表描述',
  `DefVal` varchar(200) DEFAULT NULL COMMENT '默认值',
  `FK_SFDBSrc` varchar(100) DEFAULT NULL COMMENT '数据源,外键:对应物理表:Sys_SFDBSrc,表描述:数据源',
  `SrcTable` varchar(200) DEFAULT NULL COMMENT '数据源表',
  `ColumnValue` varchar(200) DEFAULT NULL COMMENT '显示的值(编号列)',
  `ColumnText` varchar(200) DEFAULT NULL COMMENT '显示的文字(名称列)',
  `ParentValue` varchar(200) DEFAULT NULL COMMENT '父级值(父级列)',
  `SelectStatement` varchar(1000) DEFAULT NULL COMMENT '查询语句',
  `RDT` varchar(50) DEFAULT NULL COMMENT '加入日期',
  `RootVal` varchar(200) DEFAULT NULL COMMENT '根目录值',
  PRIMARY KEY (`No`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='字典表';

-- ----------------------------
-- Records of sys_sftable
-- ----------------------------

-- ----------------------------
-- Table structure for `sys_sms`
-- ----------------------------
DROP TABLE IF EXISTS `sys_sms`;
CREATE TABLE `sys_sms` (
  `MyPK` varchar(100) NOT NULL COMMENT 'MyPK - 主键',
  `Sender` varchar(200) DEFAULT NULL COMMENT '发送人(可以为空)',
  `SendTo` varchar(200) DEFAULT NULL COMMENT '发送给(可以为空)',
  `RDT` varchar(50) DEFAULT NULL COMMENT '写入时间',
  `Mobile` varchar(30) DEFAULT NULL COMMENT '手机号(可以为空)',
  `MobileSta` int(11) DEFAULT NULL COMMENT '消息状态',
  `MobileInfo` varchar(1000) DEFAULT NULL COMMENT '短信信息',
  `Email` varchar(200) DEFAULT NULL COMMENT 'Email(可以为空)',
  `EmailSta` int(11) DEFAULT NULL COMMENT 'EmaiSta消息状态',
  `EmailTitle` varchar(3000) DEFAULT NULL COMMENT '标题',
  `EmailDoc` text COMMENT '内容',
  `SendDT` varchar(50) DEFAULT NULL COMMENT '发送时间',
  `IsRead` int(11) DEFAULT NULL COMMENT '是否读取?',
  `IsAlert` int(11) DEFAULT NULL COMMENT '是否提示?',
  `MsgFlag` varchar(200) DEFAULT NULL COMMENT '消息标记(用于防止发送重复)',
  `MsgType` varchar(200) DEFAULT NULL COMMENT '消息类型(CC抄送,Todolist待办,Return退回,Etc其他消息...)',
  `AtPara` varchar(500) DEFAULT NULL COMMENT 'AtPara',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='消息';

-- ----------------------------
-- Records of sys_sms
-- ----------------------------

-- ----------------------------
-- Table structure for `sys_userlogt`
-- ----------------------------
DROP TABLE IF EXISTS `sys_userlogt`;
CREATE TABLE `sys_userlogt` (
  `MyPK` varchar(100) NOT NULL COMMENT 'MyPK - 主键',
  `FK_Emp` varchar(30) DEFAULT NULL COMMENT '用户',
  `IP` varchar(200) DEFAULT NULL COMMENT 'IP',
  `LogFlag` varchar(300) DEFAULT NULL COMMENT 'Flag',
  `Docs` varchar(300) DEFAULT NULL COMMENT 'Docs',
  `RDT` varchar(20) DEFAULT NULL COMMENT '记录日期',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户日志';

-- ----------------------------
-- Records of sys_userlogt
-- ----------------------------
INSERT INTO `sys_userlogt` VALUES ('04ebc46e75c342489a0b6d6d890a20db', 'admin', '127.0.0.1', 'SignIn', '登录', '2018-03-31 11:52:31');
INSERT INTO `sys_userlogt` VALUES ('f90d14ef41084a338338cb8b183ea0cb', 'admin', '127.0.0.1', 'SignIn', '登录', '2018-03-31 11:53:40');

-- ----------------------------
-- Table structure for `sys_userregedit`
-- ----------------------------
DROP TABLE IF EXISTS `sys_userregedit`;
CREATE TABLE `sys_userregedit` (
  `MyPK` varchar(100) NOT NULL COMMENT 'MyPK - 主键',
  `FK_Emp` varchar(30) DEFAULT NULL COMMENT '用户',
  `CfgKey` varchar(200) DEFAULT NULL COMMENT '键',
  `Vals` varchar(2000) DEFAULT NULL COMMENT '值',
  `GenerSQL` varchar(2000) DEFAULT NULL COMMENT 'GenerSQL',
  `Paras` varchar(2000) DEFAULT NULL COMMENT 'Paras',
  `NumKey` varchar(300) DEFAULT NULL COMMENT '分析的Key',
  `OrderBy` varchar(300) DEFAULT NULL COMMENT 'OrderBy',
  `OrderWay` varchar(300) DEFAULT NULL COMMENT 'OrderWay',
  `SearchKey` varchar(300) DEFAULT NULL COMMENT 'SearchKey',
  `MVals` varchar(300) DEFAULT NULL COMMENT 'MVals',
  `IsPic` int(11) DEFAULT NULL COMMENT '是否图片',
  `DTFrom` varchar(20) DEFAULT NULL COMMENT '查询时间从',
  `DTTo` varchar(20) DEFAULT NULL COMMENT '到',
  `AtPara` text COMMENT 'AtPara',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户注册表';

-- ----------------------------
-- Records of sys_userregedit
-- ----------------------------

-- ----------------------------
-- Table structure for `sys_wfsealdata`
-- ----------------------------
DROP TABLE IF EXISTS `sys_wfsealdata`;
CREATE TABLE `sys_wfsealdata` (
  `MyPK` varchar(100) NOT NULL COMMENT 'MyPK - 主键',
  `OID` varchar(200) DEFAULT NULL COMMENT 'OID',
  `FK_Node` varchar(200) DEFAULT NULL COMMENT 'FK_Node',
  `FK_MapData` varchar(300) DEFAULT NULL COMMENT 'FK_MapData',
  `SealData` text COMMENT 'SealData',
  `RDT` varchar(20) DEFAULT NULL COMMENT '记录日期',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='签名信息';

-- ----------------------------
-- Records of sys_wfsealdata
-- ----------------------------

-- ----------------------------
-- Table structure for `v_todolist`
-- ----------------------------
DROP TABLE IF EXISTS `v_todolist`;
CREATE TABLE `v_todolist` (
  `MyPK` varchar(100) NOT NULL COMMENT 'MyPK - 主键',
  `FK_FlowSort` varchar(100) DEFAULT NULL COMMENT '类别,外键:对应物理表:WF_FlowSort,表描述:流程类别',
  `FK_Flow` varchar(100) DEFAULT NULL COMMENT '流程,外键:对应物理表:WF_Flow,表描述:流程',
  `TodoSta0` int(11) DEFAULT NULL COMMENT '待办中',
  `TodoSta1` int(11) DEFAULT NULL COMMENT '预警中',
  `TodoSta2` int(11) DEFAULT NULL COMMENT '逾期中',
  `TodoSta3` int(11) DEFAULT NULL COMMENT '正常办结',
  `TodoSta4` int(11) DEFAULT NULL COMMENT '超期办结',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='流程统计';

-- ----------------------------
-- Records of v_todolist
-- ----------------------------

-- ----------------------------
-- Table structure for `wf_accepterrole`
-- ----------------------------
DROP TABLE IF EXISTS `wf_accepterrole`;
CREATE TABLE `wf_accepterrole` (
  `OID` int(11) NOT NULL COMMENT 'OID - 主键',
  `Name` varchar(200) DEFAULT NULL COMMENT 'null',
  `FK_Node` varchar(100) DEFAULT NULL COMMENT '节点',
  `FK_Mode` int(11) DEFAULT NULL COMMENT '模式类型',
  `Tag0` varchar(999) DEFAULT NULL COMMENT 'Tag0',
  `Tag1` varchar(999) DEFAULT NULL COMMENT 'Tag1',
  `Tag2` varchar(999) DEFAULT NULL COMMENT 'Tag2',
  `Tag3` varchar(999) DEFAULT NULL COMMENT 'Tag3',
  `Tag4` varchar(999) DEFAULT NULL COMMENT 'Tag4',
  `Tag5` varchar(999) DEFAULT NULL COMMENT 'Tag5',
  PRIMARY KEY (`OID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='接受人规则';

-- ----------------------------
-- Records of wf_accepterrole
-- ----------------------------

-- ----------------------------
-- Table structure for `wf_bill`
-- ----------------------------
DROP TABLE IF EXISTS `wf_bill`;
CREATE TABLE `wf_bill` (
  `MyPK` varchar(100) NOT NULL COMMENT 'MyPK - 主键',
  `WorkID` int(11) DEFAULT NULL COMMENT '工作ID',
  `FID` int(11) DEFAULT NULL COMMENT 'FID',
  `FK_Flow` varchar(4) DEFAULT NULL COMMENT '流程',
  `FK_BillType` varchar(300) DEFAULT NULL COMMENT '单据类型',
  `Title` varchar(900) DEFAULT NULL COMMENT '标题',
  `FK_Starter` varchar(50) DEFAULT NULL COMMENT '发起人',
  `StartDT` varchar(50) DEFAULT NULL COMMENT '发起时间',
  `Url` varchar(2000) DEFAULT NULL COMMENT 'Url',
  `FullPath` varchar(2000) DEFAULT NULL COMMENT 'FullPath',
  `FK_Emp` varchar(100) DEFAULT NULL COMMENT '打印人,外键:对应物理表:Port_Emp,表描述:用户',
  `RDT` varchar(50) DEFAULT NULL COMMENT '打印时间',
  `FK_Dept` varchar(100) DEFAULT NULL COMMENT '隶属部门,外键:对应物理表:Port_Dept,表描述:部门',
  `FK_NY` varchar(100) DEFAULT NULL COMMENT '隶属年月,外键:对应物理表:Pub_NY,表描述:年月',
  `Emps` text COMMENT 'Emps',
  `FK_Node` varchar(30) DEFAULT NULL COMMENT '节点',
  `FK_Bill` varchar(500) DEFAULT NULL COMMENT 'FK_Bill',
  `MyNum` int(11) DEFAULT NULL COMMENT '个数',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='单据';

-- ----------------------------
-- Records of wf_bill
-- ----------------------------

-- ----------------------------
-- Table structure for `wf_billtemplate`
-- ----------------------------
DROP TABLE IF EXISTS `wf_billtemplate`;
CREATE TABLE `wf_billtemplate` (
  `No` varchar(190) NOT NULL COMMENT 'No - 主键',
  `Name` varchar(200) DEFAULT NULL COMMENT 'Name',
  `NodeID` int(11) DEFAULT NULL COMMENT 'NodeID',
  `BillFileType` int(11) DEFAULT NULL COMMENT '    生成的文件类型,枚举类型:0 Word 1 PDF 2 Excel(未完成) 3 Html(未完成) 5 锐浪报表',
  `FK_BillType` varchar(4) DEFAULT NULL COMMENT '单据类型',
  `IDX` varchar(200) DEFAULT NULL COMMENT 'IDX',
  `ExpField` varchar(800) DEFAULT NULL COMMENT '要排除的字段',
  `ReplaceVal` varchar(3000) DEFAULT NULL COMMENT '要替换的值',
  PRIMARY KEY (`No`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='单据模板';

-- ----------------------------
-- Records of wf_billtemplate
-- ----------------------------

-- ----------------------------
-- Table structure for `wf_billtype`
-- ----------------------------
DROP TABLE IF EXISTS `wf_billtype`;
CREATE TABLE `wf_billtype` (
  `No` varchar(2) NOT NULL COMMENT '编号 - 主键',
  `Name` varchar(50) DEFAULT NULL COMMENT '名称',
  `FK_Flow` varchar(50) DEFAULT NULL COMMENT '流程',
  `IDX` int(11) DEFAULT NULL COMMENT 'IDX',
  PRIMARY KEY (`No`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='单据类型';

-- ----------------------------
-- Records of wf_billtype
-- ----------------------------

-- ----------------------------
-- Table structure for `wf_ccdept`
-- ----------------------------
DROP TABLE IF EXISTS `wf_ccdept`;
CREATE TABLE `wf_ccdept` (
  `FK_Node` int(11) NOT NULL COMMENT '节点,主外键:对应物理表:WF_Node,表描述:节点',
  `FK_Dept` varchar(100) NOT NULL COMMENT '部门,主外键:对应物理表:Port_Dept,表描述:部门',
  PRIMARY KEY (`FK_Node`,`FK_Dept`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='抄送部门';

-- ----------------------------
-- Records of wf_ccdept
-- ----------------------------

-- ----------------------------
-- Table structure for `wf_ccemp`
-- ----------------------------
DROP TABLE IF EXISTS `wf_ccemp`;
CREATE TABLE `wf_ccemp` (
  `FK_Node` int(11) NOT NULL COMMENT '节点 - 主键',
  `FK_Emp` varchar(100) NOT NULL COMMENT '人员,主外键:对应物理表:Port_Emp,表描述:用户',
  PRIMARY KEY (`FK_Node`,`FK_Emp`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='抄送人员';

-- ----------------------------
-- Records of wf_ccemp
-- ----------------------------

-- ----------------------------
-- Table structure for `wf_cclist`
-- ----------------------------
DROP TABLE IF EXISTS `wf_cclist`;
CREATE TABLE `wf_cclist` (
  `MyPK` varchar(100) NOT NULL COMMENT 'MyPK - 主键',
  `Title` varchar(500) DEFAULT NULL COMMENT '标题',
  `FK_Flow` varchar(3) DEFAULT NULL COMMENT '流程编号',
  `FlowName` varchar(200) DEFAULT NULL COMMENT '流程名称',
  `FK_Node` int(11) DEFAULT NULL COMMENT '节点',
  `NodeName` varchar(500) DEFAULT NULL COMMENT '节点名称',
  `WorkID` int(11) DEFAULT NULL COMMENT '工作ID',
  `FID` int(11) DEFAULT NULL COMMENT 'FID',
  `Doc` text COMMENT '内容',
  `Rec` varchar(50) DEFAULT NULL COMMENT '抄送人员',
  `RecDept` varchar(50) DEFAULT NULL COMMENT '抄送人员部门',
  `RDT` varchar(50) DEFAULT NULL COMMENT '记录日期',
  `Sta` int(11) DEFAULT NULL COMMENT '状态',
  `CCTo` varchar(50) DEFAULT NULL COMMENT '抄送给',
  `CCToDept` varchar(50) DEFAULT NULL COMMENT '抄送到部门',
  `CCToName` varchar(50) DEFAULT NULL COMMENT '抄送给(人员名称)',
  `CheckNote` varchar(600) DEFAULT NULL COMMENT '审核意见',
  `CDT` varchar(50) DEFAULT NULL COMMENT '打开时间',
  `PFlowNo` varchar(100) DEFAULT NULL COMMENT '父流程编号',
  `PWorkID` int(11) DEFAULT NULL COMMENT '父流程WorkID',
  `InEmpWorks` int(11) DEFAULT NULL COMMENT '是否加入待办列表',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='抄送列表';

-- ----------------------------
-- Records of wf_cclist
-- ----------------------------

-- ----------------------------
-- Table structure for `wf_ccstation`
-- ----------------------------
DROP TABLE IF EXISTS `wf_ccstation`;
CREATE TABLE `wf_ccstation` (
  `FK_Node` int(11) NOT NULL COMMENT '节点,主外键:对应物理表:WF_Node,表描述:节点',
  `FK_Station` varchar(100) NOT NULL COMMENT '工作岗位,主外键:对应物理表:Port_Station,表描述:岗位',
  PRIMARY KEY (`FK_Node`,`FK_Station`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='抄送岗位';

-- ----------------------------
-- Records of wf_ccstation
-- ----------------------------

-- ----------------------------
-- Table structure for `wf_ch`
-- ----------------------------
DROP TABLE IF EXISTS `wf_ch`;
CREATE TABLE `wf_ch` (
  `MyPK` varchar(100) NOT NULL COMMENT 'MyPK - 主键',
  `WorkID` int(11) DEFAULT NULL COMMENT '工作ID',
  `FID` int(11) DEFAULT NULL COMMENT 'FID',
  `Title` varchar(900) DEFAULT NULL COMMENT '标题',
  `FK_Flow` varchar(100) DEFAULT NULL COMMENT '流程',
  `FK_FlowT` varchar(200) DEFAULT NULL COMMENT '流程名称',
  `FK_Node` int(11) DEFAULT NULL COMMENT '节点',
  `FK_NodeT` varchar(200) DEFAULT NULL COMMENT '节点名称',
  `Sender` varchar(200) DEFAULT NULL COMMENT '发送人',
  `SenderT` varchar(200) DEFAULT NULL COMMENT '发送人名称',
  `FK_Emp` varchar(100) DEFAULT NULL COMMENT '当事人',
  `FK_EmpT` varchar(200) DEFAULT NULL COMMENT '当事人名称',
  `GroupEmps` varchar(400) DEFAULT NULL COMMENT '相关当事人',
  `GroupEmpsNames` varchar(900) DEFAULT NULL COMMENT '相关当事人名称',
  `GroupEmpsNum` int(11) DEFAULT NULL COMMENT '相关当事人数量',
  `DTFrom` varchar(50) DEFAULT NULL COMMENT '任务下达时间',
  `DTTo` varchar(50) DEFAULT NULL COMMENT '任务处理时间',
  `SDT` varchar(50) DEFAULT NULL COMMENT '应完成日期',
  `FK_Dept` varchar(100) DEFAULT NULL COMMENT '隶属部门',
  `FK_DeptT` varchar(500) DEFAULT NULL COMMENT '部门名称',
  `FK_NY` varchar(100) DEFAULT NULL COMMENT '隶属月份',
  `DTSWay` int(11) DEFAULT NULL COMMENT '  考核方式,枚举类型:0 不考核 1 按照时效考核 2 按照工作量考核',
  `TimeLimit` varchar(50) DEFAULT NULL COMMENT '规定限期',
  `OverMinutes` float DEFAULT NULL COMMENT '逾期分钟',
  `UseDays` float DEFAULT NULL COMMENT '实际使用天',
  `OverDays` float DEFAULT NULL COMMENT '逾期天',
  `CHSta` int(11) DEFAULT NULL COMMENT '状态',
  `WeekNum` int(11) DEFAULT NULL COMMENT '第几周',
  `Points` float DEFAULT NULL COMMENT '总扣分',
  `MyNum` int(11) DEFAULT NULL COMMENT '个数',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='时效考核';

-- ----------------------------
-- Records of wf_ch
-- ----------------------------

-- ----------------------------
-- Table structure for `wf_cheval`
-- ----------------------------
DROP TABLE IF EXISTS `wf_cheval`;
CREATE TABLE `wf_cheval` (
  `MyPK` varchar(100) NOT NULL COMMENT 'MyPK - 主键',
  `Title` varchar(500) DEFAULT NULL COMMENT '标题',
  `FK_Flow` varchar(7) DEFAULT NULL COMMENT '流程编号',
  `FlowName` varchar(100) DEFAULT NULL COMMENT '流程名称',
  `WorkID` int(11) DEFAULT NULL COMMENT '工作ID',
  `FK_Node` int(11) DEFAULT NULL COMMENT '评价节点',
  `NodeName` varchar(100) DEFAULT NULL COMMENT '节点名称',
  `Rec` varchar(50) DEFAULT NULL COMMENT '评价人',
  `RecName` varchar(50) DEFAULT NULL COMMENT '评价人名称',
  `RDT` varchar(50) DEFAULT NULL COMMENT '评价日期',
  `EvalEmpNo` varchar(50) DEFAULT NULL COMMENT '被考核的人员编号',
  `EvalEmpName` varchar(50) DEFAULT NULL COMMENT '被考核的人员名称',
  `EvalCent` varchar(20) DEFAULT NULL COMMENT '评价分值',
  `EvalNote` varchar(20) DEFAULT NULL COMMENT '评价内容',
  `FK_Dept` varchar(50) DEFAULT NULL COMMENT '部门',
  `DeptName` varchar(100) DEFAULT NULL COMMENT '部门名称',
  `FK_NY` varchar(7) DEFAULT NULL COMMENT '年月',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='工作质量评价';

-- ----------------------------
-- Records of wf_cheval
-- ----------------------------

-- ----------------------------
-- Table structure for `wf_cond`
-- ----------------------------
DROP TABLE IF EXISTS `wf_cond`;
CREATE TABLE `wf_cond` (
  `MyPK` varchar(100) NOT NULL COMMENT 'MyPK - 主键',
  `CondType` int(11) DEFAULT NULL COMMENT '条件类型',
  `DataFrom` int(11) DEFAULT NULL COMMENT '条件数据来源0表单,1岗位(对方向条件有效)',
  `FK_Flow` varchar(60) DEFAULT NULL COMMENT '流程',
  `NodeID` int(11) DEFAULT NULL COMMENT '发生的事件MainNode',
  `FK_Node` int(11) DEFAULT NULL COMMENT '节点ID',
  `FK_Attr` varchar(80) DEFAULT NULL COMMENT '属性',
  `AttrKey` varchar(60) DEFAULT NULL COMMENT '属性键',
  `AttrName` varchar(500) DEFAULT NULL COMMENT '中文名称',
  `FK_Operator` varchar(60) DEFAULT NULL COMMENT '运算符号',
  `OperatorValue` text COMMENT '要运算的值',
  `OperatorValueT` text COMMENT '要运算的值T',
  `ToNodeID` int(11) DEFAULT NULL COMMENT 'ToNodeID（对方向条件有效）',
  `ConnJudgeWay` int(11) DEFAULT NULL COMMENT ' 条件关系,枚举类型:0 or 1 and',
  `MyPOID` int(11) DEFAULT NULL COMMENT 'MyPOID',
  `PRI` int(11) DEFAULT NULL COMMENT '计算优先级',
  `CondOrAnd` int(11) DEFAULT NULL COMMENT '方向条件类型',
  `AtPara` varchar(2000) DEFAULT NULL COMMENT 'AtPara',
  `Note` varchar(500) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='流程条件';

-- ----------------------------
-- Records of wf_cond
-- ----------------------------

-- ----------------------------
-- Table structure for `wf_dataapply`
-- ----------------------------
DROP TABLE IF EXISTS `wf_dataapply`;
CREATE TABLE `wf_dataapply` (
  `MyPK` varchar(100) NOT NULL COMMENT 'MyPK - 主键',
  `WorkID` int(11) DEFAULT NULL COMMENT '工作ID',
  `NodeId` int(11) DEFAULT NULL COMMENT 'NodeId',
  `RunState` int(11) DEFAULT NULL COMMENT '运行状态0,没有提交，1，提交申请执行审批中，2，审核完毕。',
  `ApplyDays` int(11) DEFAULT NULL COMMENT '申请天数',
  `ApplyData` varchar(50) DEFAULT NULL COMMENT '申请日期',
  `Applyer` varchar(100) DEFAULT NULL COMMENT '申请人,外键:对应物理表:Port_Emp,表描述:用户',
  `ApplyNote1` text COMMENT '申请原因',
  `ApplyNote2` text COMMENT '申请备注',
  `Checker` varchar(100) DEFAULT NULL COMMENT '审批人,外键:对应物理表:Port_Emp,表描述:用户',
  `CheckerData` varchar(50) DEFAULT NULL COMMENT '审批日期',
  `CheckerDays` int(11) DEFAULT NULL COMMENT '批准天数',
  `CheckerNote1` text COMMENT '审批意见',
  `CheckerNote2` text COMMENT '审批备注',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='追加时间申请';

-- ----------------------------
-- Records of wf_dataapply
-- ----------------------------

-- ----------------------------
-- Table structure for `wf_deptflowsearch`
-- ----------------------------
DROP TABLE IF EXISTS `wf_deptflowsearch`;
CREATE TABLE `wf_deptflowsearch` (
  `MyPK` varchar(100) NOT NULL COMMENT 'MyPK - 主键',
  `FK_Emp` varchar(50) DEFAULT NULL COMMENT '操作员',
  `FK_Flow` varchar(50) DEFAULT NULL COMMENT '流程编号',
  `FK_Dept` varchar(100) DEFAULT NULL COMMENT '部门编号',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='流程部门数据查询权限';

-- ----------------------------
-- Records of wf_deptflowsearch
-- ----------------------------

-- ----------------------------
-- Table structure for `wf_direction`
-- ----------------------------
DROP TABLE IF EXISTS `wf_direction`;
CREATE TABLE `wf_direction` (
  `MyPK` varchar(100) NOT NULL COMMENT 'MyPK - 主键',
  `FK_Flow` varchar(10) DEFAULT NULL COMMENT '流程',
  `Node` int(11) DEFAULT NULL COMMENT '从节点',
  `ToNode` int(11) DEFAULT NULL COMMENT '到节点',
  `DirType` int(11) DEFAULT NULL COMMENT '类型0前进1返回',
  `IsCanBack` int(11) DEFAULT NULL COMMENT '是否可以原路返回(对后退线有效)',
  `Dots` varchar(300) DEFAULT NULL COMMENT '轨迹信息',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='节点方向信息';

-- ----------------------------
-- Records of wf_direction
-- ----------------------------

-- ----------------------------
-- Table structure for `wf_emp`
-- ----------------------------
DROP TABLE IF EXISTS `wf_emp`;
CREATE TABLE `wf_emp` (
  `No` varchar(50) NOT NULL COMMENT 'No - 主键',
  `Name` varchar(50) DEFAULT NULL COMMENT 'Name',
  `FK_Dept` varchar(100) DEFAULT NULL COMMENT 'FK_Dept',
  `OrgNo` varchar(100) DEFAULT NULL COMMENT '组织,外键:对应物理表:Port_Inc,表描述:独立组织',
  `UseSta` int(11) DEFAULT NULL COMMENT '用户状态0禁用,1正常.',
  `UserType` int(11) DEFAULT NULL COMMENT ' 用户状态,枚举类型:0 普通用户 1 管理员用户',
  `RootOfFlow` varchar(100) DEFAULT NULL COMMENT '流程权限节点,外键:对应物理表:WF_FlowSort,表描述:流程类别',
  `RootOfForm` varchar(100) DEFAULT NULL COMMENT '表单权限节点,外键:对应物理表:Sys_FormTree,表描述:表单树',
  `RootOfDept` varchar(100) DEFAULT NULL COMMENT '组织结构权限节点,外键:对应物理表:Port_Dept,表描述:部门',
  `Tel` varchar(50) DEFAULT NULL COMMENT 'Tel',
  `Email` varchar(50) DEFAULT NULL COMMENT 'Email',
  `TM` varchar(50) DEFAULT NULL COMMENT '即时通讯号',
  `AlertWay` int(11) DEFAULT NULL COMMENT '      收听方式,枚举类型:0 不接收 1 短信 2 邮件 3 内部消息 4 QQ消息 5 RTX消息 6 MSN消息',
  `Author` varchar(50) DEFAULT NULL COMMENT '授权人',
  `AuthorDate` varchar(50) DEFAULT NULL COMMENT '授权日期',
  `AuthorWay` int(11) DEFAULT NULL COMMENT '授权方式',
  `AuthorToDate` varchar(50) DEFAULT NULL COMMENT '授权到日期',
  `AuthorFlows` varchar(1000) DEFAULT NULL COMMENT '可以执行的授权流程',
  `Stas` varchar(3000) DEFAULT NULL COMMENT '岗位s',
  `Depts` varchar(100) DEFAULT NULL COMMENT 'Deptss',
  `FtpUrl` varchar(50) DEFAULT NULL COMMENT 'FtpUrl',
  `Msg` text COMMENT 'Msg',
  `Style` text COMMENT 'Style',
  `StartFlows` text COMMENT '可以发起的流程',
  `Idx` int(11) DEFAULT NULL COMMENT 'Idx',
  PRIMARY KEY (`No`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='操作员';

-- ----------------------------
-- Records of wf_emp
-- ----------------------------
INSERT INTO `wf_emp` VALUES ('admin', 'admin', '100', null, '1', null, null, null, null, '0531-82374939', 'zhoupeng@ccflow.org', '', '3', '', '', '0', '', '', '', '', '', '', '', '', '0');
INSERT INTO `wf_emp` VALUES ('fuhui', '福惠', '1003', null, '1', null, null, null, null, '0531-82374939', 'fuhui@ccflow.org', '', '3', '', '', '0', '', '', '', '', '', '', '', '', '0');
INSERT INTO `wf_emp` VALUES ('guobaogeng', '郭宝庚', '1004', null, '1', null, null, null, null, '0531-82374939', 'guobaogeng@ccflow.org', '', '3', '', '', '0', '', '', '', '', '', '', '', '', '0');
INSERT INTO `wf_emp` VALUES ('guoxiangbin', '郭祥斌', '1003', null, '1', null, null, null, null, '0531-82374939', 'guoxiangbin@ccflow.org', '', '3', '', '', '0', '', '', '', '', '', '', '', '', '0');
INSERT INTO `wf_emp` VALUES ('liping', '李萍', '1005', null, '1', null, null, null, null, '0531-82374939', 'liping@ccflow.org', '', '3', '', '', '0', '', '', '', '', '', '', '', '', '0');
INSERT INTO `wf_emp` VALUES ('liyan', '李言', '1005', null, '1', null, null, null, null, '0531-82374939', 'liyan@ccflow.org', '', '3', '', '', '0', '', '', '', '', '', '', '', '', '0');
INSERT INTO `wf_emp` VALUES ('qifenglin', '祁凤林', '1002', null, '1', null, null, null, null, '0531-82374939', 'qifenglin@ccflow.org', '', '3', '', '', '0', '', '', '', '', '', '', '', '', '0');
INSERT INTO `wf_emp` VALUES ('yangyilei', '杨依雷', '1004', null, '1', null, null, null, null, '0531-82374939', 'yangyilei@ccflow.org', '', '3', '', '', '0', '', '', '', '', '', '', '', '', '0');
INSERT INTO `wf_emp` VALUES ('zhanghaicheng', '张海成', '1001', null, '1', null, null, null, null, '0531-82374939', 'zhanghaicheng@ccflow.org', '', '3', '', '', '0', '', '', '', '', '', '', '', '', '0');
INSERT INTO `wf_emp` VALUES ('zhangyifan', '张一帆', '1001', null, '1', null, null, null, null, '0531-82374939', 'zhangyifan@ccflow.org', '', '3', '', '', '0', '', '', '', '', '', '', '', '', '0');
INSERT INTO `wf_emp` VALUES ('zhoupeng', '周朋', '100', null, '1', null, null, null, null, '0531-82374939', 'zhoupeng@ccflow.org', '', '3', '', '', '0', '', '', '', '', '', '', '', '', '0');
INSERT INTO `wf_emp` VALUES ('zhoushengyu', '周升雨', '1001', null, '1', null, null, null, null, '0531-82374939', 'zhoushengyu@ccflow.org', '', '3', '', '', '0', '', '', '', '', '', '', '', '', '0');
INSERT INTO `wf_emp` VALUES ('zhoutianjiao', '周天娇', '1002', null, '1', null, null, null, null, '0531-82374939', 'zhoutianjiao@ccflow.org', '', '3', '', '', '0', '', '', '', '', '', '', '', '', '0');

-- ----------------------------
-- Table structure for `wf_findworkerrole`
-- ----------------------------
DROP TABLE IF EXISTS `wf_findworkerrole`;
CREATE TABLE `wf_findworkerrole` (
  `OID` int(11) NOT NULL COMMENT 'OID - 主键',
  `Name` varchar(200) DEFAULT NULL COMMENT 'Name',
  `FK_Node` int(11) DEFAULT NULL COMMENT '节点ID',
  `SortVal0` varchar(200) DEFAULT NULL COMMENT 'SortVal0',
  `SortText0` varchar(200) DEFAULT NULL COMMENT 'SortText0',
  `SortVal1` varchar(200) DEFAULT NULL COMMENT 'SortVal1',
  `SortText1` varchar(200) DEFAULT NULL COMMENT 'SortText1',
  `SortVal2` varchar(200) DEFAULT NULL COMMENT 'SortText2',
  `SortText2` varchar(200) DEFAULT NULL COMMENT 'SortText2',
  `SortVal3` varchar(200) DEFAULT NULL COMMENT 'SortVal3',
  `SortText3` varchar(200) DEFAULT NULL COMMENT 'SortText3',
  `TagVal0` varchar(1000) DEFAULT NULL COMMENT 'TagVal0',
  `TagVal1` varchar(1000) DEFAULT NULL COMMENT 'TagVal1',
  `TagVal2` varchar(1000) DEFAULT NULL COMMENT 'TagVal2',
  `TagVal3` varchar(1000) DEFAULT NULL COMMENT 'TagVal3',
  `TagText0` varchar(1000) DEFAULT NULL COMMENT 'TagText0',
  `TagText1` varchar(1000) DEFAULT NULL COMMENT 'TagText1',
  `TagText2` varchar(1000) DEFAULT NULL COMMENT 'TagText2',
  `TagText3` varchar(1000) DEFAULT NULL COMMENT 'TagText3',
  `IsEnable` int(11) DEFAULT NULL COMMENT '是否可用',
  `Idx` int(11) DEFAULT NULL COMMENT 'IDX',
  PRIMARY KEY (`OID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='找人规则';

-- ----------------------------
-- Records of wf_findworkerrole
-- ----------------------------

-- ----------------------------
-- Table structure for `wf_flow`
-- ----------------------------
DROP TABLE IF EXISTS `wf_flow`;
CREATE TABLE `wf_flow` (
  `No` varchar(200) NOT NULL COMMENT '编号 - 主键',
  `FK_FlowSort` varchar(100) DEFAULT NULL COMMENT '流程类别,外键:对应物理表:WF_FlowSort,表描述:流程类别',
  `Name` varchar(500) DEFAULT NULL COMMENT '名称',
  `FlowMark` varchar(150) DEFAULT NULL COMMENT '流程标记',
  `FlowEventEntity` varchar(150) DEFAULT NULL COMMENT '流程事件实体',
  `TitleRole` varchar(150) DEFAULT NULL COMMENT '标题生成规则',
  `BillNoFormat` varchar(50) DEFAULT NULL COMMENT '单据编号格式',
  `ChartType` int(11) DEFAULT NULL COMMENT ' 节点图形类型,枚举类型:0 几何图形 1 肖像图片',
  `IsCanStart` int(11) DEFAULT NULL COMMENT '可以独立启动否？(独立启动的流程可以显示在发起流程列表里)',
  `IsStartInMobile` int(11) DEFAULT NULL COMMENT '是否可以在手机里启用？(如果发起表单特别复杂就不要在手机里启用了)',
  `IsMD5` int(11) DEFAULT NULL COMMENT '是否是数据加密流程(MD5数据加密防篡改)',
  `IsFullSA` int(11) DEFAULT NULL COMMENT '是否自动计算未来的处理人？',
  `IsAutoSendSubFlowOver` int(11) DEFAULT NULL COMMENT '(为子流程时)在流程结束时，是否检查所有子流程完成后，让父流程自动发送到下一步。',
  `IsGuestFlow` int(11) DEFAULT NULL COMMENT '是否外部用户参与流程(非组织结构人员参与的流程)',
  `IsBatchStart` int(11) DEFAULT NULL COMMENT '是否可以批量发起流程？(如果是就要设置发起的需要填写的字段,多个用逗号分开)',
  `BatchStartFields` varchar(500) DEFAULT NULL COMMENT '发起字段s',
  `FlowAppType` int(11) DEFAULT NULL COMMENT '  流程应用类型,枚举类型:0 业务流程 1 工程类(项目组流程) 2 公文流程(VSTO)',
  `TimelineRole` int(11) DEFAULT NULL COMMENT ' 时效性规则,枚举类型:0 按节点(由节点属性来定义) 1 按发起人(开始节点SysSDTOfFlow字段计算)',
  `Draft` int(11) DEFAULT NULL COMMENT '  草稿规则,枚举类型:0 无(不设草稿) 1 保存到待办 2 保存到草稿箱',
  `DataStoreModel` int(11) DEFAULT NULL COMMENT ' 流程数据存储模式,枚举类型:0 数据轨迹模式 1 数据合并模式',
  `PTable` varchar(30) DEFAULT NULL COMMENT '流程数据存储表',
  `HistoryFields` varchar(500) DEFAULT NULL COMMENT '历史查看字段',
  `FlowNoteExp` varchar(500) DEFAULT NULL COMMENT '备注的表达式',
  `Note` varchar(100) DEFAULT NULL COMMENT '流程描述',
  `HelpUrl` varchar(300) DEFAULT NULL COMMENT '帮助文档',
  `IsResetData` int(11) DEFAULT NULL COMMENT '是否启用开始节点数据重置按钮？',
  `IsLoadPriData` int(11) DEFAULT NULL COMMENT '是否自动装载上一笔数据？',
  `IsDBTemplate` int(11) DEFAULT NULL COMMENT '是否启用数据模版？',
  `SysType` varchar(100) DEFAULT NULL COMMENT '类型类型',
  `FlowDeleteRole` int(11) DEFAULT NULL COMMENT '   流程实例删除规则,枚举类型:0 超级管理员可以删除 1 分级管理员可以删除 2 发起人可以删除 3 节点启动删除按钮的操作员',
  `Tester` varchar(300) DEFAULT NULL COMMENT '设置流程发起测试人',
  `DesignerNo` varchar(50) DEFAULT NULL COMMENT '设计者编号',
  `DesignerName` varchar(100) DEFAULT NULL COMMENT '设计者名称',
  `FlowRunWay` int(11) DEFAULT NULL COMMENT '   启动方式,枚举类型:0 手工启动 1 指定人员定时启动 2 定时访问数据集自动启动 3 触发式启动',
  `RunObj` varchar(4000) DEFAULT NULL COMMENT '运行内容',
  `RunSQL` varchar(2000) DEFAULT NULL COMMENT '流程结束执行后执行的SQL',
  `NumOfBill` int(11) DEFAULT NULL COMMENT '是否有单据',
  `NumOfDtl` int(11) DEFAULT NULL COMMENT 'NumOfDtl',
  `AvgDay` float(50,2) DEFAULT NULL COMMENT '平均运行用天',
  `Idx` int(11) DEFAULT NULL COMMENT '显示顺序号(在发起列表中)',
  `Paras` varchar(400) DEFAULT NULL COMMENT '参数',
  `DRCtrlType` int(11) DEFAULT NULL COMMENT '部门查询权限控制方式',
  `StartLimitRole` int(11) DEFAULT NULL COMMENT '        启动限制规则,枚举类型:0 不限制 1 每人每天一次 2 每人每周一次 3 每人每月一次 4 每人每季一次 5 每人每年一次 6 发起的列不能重复,(多个列可以用逗号分开) 7 设置的SQL数据源为空,或者返回结果为零时可以启动. 8 设置的SQL数据源为空,或者返回结果为零时不可以启动.',
  `StartLimitPara` varchar(500) DEFAULT NULL COMMENT '规则参数',
  `StartLimitAlert` varchar(4000) DEFAULT NULL COMMENT '限制提示',
  `StartLimitWhen` int(11) DEFAULT NULL COMMENT '提示时间',
  `StartGuideWay` int(11) DEFAULT NULL COMMENT '       前置导航方式,枚举类型:0 无 1 按系统的URL-(父子流程)单条模式 2 按系统的URL-(子父流程)多条模式 3 按系统的URL-(实体记录,未完成)单条模式 4 按系统的URL-(实体记录,未完成)多条模式 5 从开始节点Copy数据 10 按自定义的Url 11 按用户输入参数',
  `StartGuideLink` varchar(200) DEFAULT NULL COMMENT '右侧的连接',
  `StartGuideLab` varchar(200) DEFAULT NULL COMMENT '连接标签',
  `StartGuidePara1` varchar(500) DEFAULT NULL COMMENT '参数1',
  `StartGuidePara2` varchar(500) DEFAULT NULL COMMENT '参数2',
  `StartGuidePara3` varchar(500) DEFAULT NULL COMMENT '参数3',
  `Ver` varchar(20) DEFAULT NULL COMMENT '版本号',
  `DType` int(11) DEFAULT NULL COMMENT '设计类型0=ccbpm,1=bpmn',
  `AtPara` varchar(1000) DEFAULT NULL COMMENT 'AtPara',
  `DTSWay` int(11) DEFAULT NULL COMMENT '  同步方式,枚举类型:0 不考核 1 按照时效考核 2 按照工作量考核',
  `DTSDBSrc` varchar(200) DEFAULT NULL COMMENT '数据库,外键:对应物理表:Sys_SFDBSrc,表描述:数据源',
  `DTSBTable` varchar(200) DEFAULT NULL COMMENT '业务表名',
  `DTSBTablePK` varchar(50) DEFAULT NULL COMMENT '业务表主键',
  `DTSTime` int(11) DEFAULT NULL COMMENT '  执行同步时间点,枚举类型:0 所有的节点发送后 1 指定的节点发送后 2 当流程结束时',
  `DTSSpecNodes` varchar(200) DEFAULT NULL COMMENT '指定的节点ID',
  `DTSField` int(11) DEFAULT NULL COMMENT ' 要同步的字段计算方式,枚举类型:0 字段名相同 1 按设置的字段匹配',
  `DTSFields` varchar(2000) DEFAULT NULL COMMENT '要同步的字段s,中间用逗号分开.',
  `MyDeptRole` int(11) DEFAULT NULL COMMENT '  本部门发起的流程,枚举类型:0 仅部门领导可以查看 1 部门下所有的人都可以查看 2 本部门里指定岗位的人可以查看',
  `PStarter` int(11) DEFAULT NULL COMMENT '发起人可看(必选)',
  `PWorker` int(11) DEFAULT NULL COMMENT '参与人可看(必选)',
  `PCCer` int(11) DEFAULT NULL COMMENT '被抄送人可看(必选)',
  `PMyDept` int(11) DEFAULT NULL COMMENT '本部门人可看',
  `PPMyDept` int(11) DEFAULT NULL COMMENT '直属上级部门可看(比如:我是)',
  `PPDept` int(11) DEFAULT NULL COMMENT '上级部门可看',
  `PSameDept` int(11) DEFAULT NULL COMMENT '平级部门可看',
  `PSpecDept` int(11) DEFAULT NULL COMMENT '指定部门可看',
  `PSpecDeptExt` varchar(200) DEFAULT NULL COMMENT '部门编号',
  `PSpecSta` int(11) DEFAULT NULL COMMENT '指定的岗位可看',
  `PSpecStaExt` varchar(200) DEFAULT NULL COMMENT '岗位编号',
  `PSpecGroup` int(11) DEFAULT NULL COMMENT '指定的权限组可看',
  `PSpecGroupExt` varchar(200) DEFAULT NULL COMMENT '权限组',
  `PSpecEmp` int(11) DEFAULT NULL COMMENT '指定的人员可看',
  `PSpecEmpExt` varchar(200) DEFAULT NULL COMMENT '指定的人员编号',
  `FlowJson` mediumblob,
  PRIMARY KEY (`No`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='节点标签';

-- ----------------------------
-- Records of wf_flow
-- ----------------------------

-- ----------------------------
-- Table structure for `wf_flowemp`
-- ----------------------------
DROP TABLE IF EXISTS `wf_flowemp`;
CREATE TABLE `wf_flowemp` (
  `FK_Flow` varchar(100) NOT NULL COMMENT 'FK_Flow,主外键:对应物理表:WF_Flow,表描述:流程',
  `FK_Emp` varchar(100) NOT NULL COMMENT '人员,主外键:对应物理表:Port_Emp,表描述:用户',
  PRIMARY KEY (`FK_Flow`,`FK_Emp`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='流程岗位属性信息';

-- ----------------------------
-- Records of wf_flowemp
-- ----------------------------

-- ----------------------------
-- Table structure for `wf_flowformtree`
-- ----------------------------
DROP TABLE IF EXISTS `wf_flowformtree`;
CREATE TABLE `wf_flowformtree` (
  `No` varchar(10) NOT NULL COMMENT '编号 - 主键',
  `Name` varchar(100) DEFAULT NULL COMMENT '名称',
  `ParentNo` varchar(100) DEFAULT NULL COMMENT '父节点No',
  `TreeNo` varchar(100) DEFAULT NULL COMMENT 'TreeNo',
  `Idx` int(11) DEFAULT NULL COMMENT 'Idx',
  `IsDir` int(11) DEFAULT NULL COMMENT '是否是目录?',
  `FK_Flow` varchar(20) DEFAULT NULL COMMENT '流程编号',
  PRIMARY KEY (`No`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='独立表单树';

-- ----------------------------
-- Records of wf_flowformtree
-- ----------------------------

-- ----------------------------
-- Table structure for `wf_flownode`
-- ----------------------------
DROP TABLE IF EXISTS `wf_flownode`;
CREATE TABLE `wf_flownode` (
  `FK_Flow` varchar(20) NOT NULL COMMENT '流程编号 - 主键',
  `FK_Node` varchar(20) NOT NULL COMMENT '节点 - 主键',
  PRIMARY KEY (`FK_Flow`,`FK_Node`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='流程抄送节点';

-- ----------------------------
-- Records of wf_flownode
-- ----------------------------

-- ----------------------------
-- Table structure for `wf_flowsort`
-- ----------------------------
DROP TABLE IF EXISTS `wf_flowsort`;
CREATE TABLE `wf_flowsort` (
  `No` varchar(10) NOT NULL COMMENT '编号 - 主键',
  `Name` varchar(100) DEFAULT NULL COMMENT '名称',
  `ParentNo` varchar(100) DEFAULT NULL COMMENT '父节点No',
  `TreeNo` varchar(100) DEFAULT NULL COMMENT 'TreeNo',
  `OrgNo` varchar(50) DEFAULT NULL COMMENT '组织编号(0为系统组织)',
  `Idx` int(11) DEFAULT NULL COMMENT 'Idx',
  `IsDir` int(11) DEFAULT NULL COMMENT 'IsDir',
  PRIMARY KEY (`No`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='流程类别';

-- ----------------------------
-- Records of wf_flowsort
-- ----------------------------
INSERT INTO `wf_flowsort` VALUES ('01', '流程树', '0', '01', '0', '0', '1');
INSERT INTO `wf_flowsort` VALUES ('100', '日常办公类', '01', '00', '0', '0', '0');
INSERT INTO `wf_flowsort` VALUES ('101', '财务类', '01', '1.0', '0', '0', '0');
INSERT INTO `wf_flowsort` VALUES ('102', '人力资源类', '01', '2.0', '0', '0', '0');

-- ----------------------------
-- Table structure for `wf_frmnode`
-- ----------------------------
DROP TABLE IF EXISTS `wf_frmnode`;
CREATE TABLE `wf_frmnode` (
  `MyPK` varchar(100) NOT NULL COMMENT 'MyPK - 主键',
  `FK_Frm` varchar(100) DEFAULT NULL COMMENT '表单ID',
  `FK_Node` int(11) DEFAULT NULL COMMENT '节点编号',
  `FK_Flow` varchar(20) DEFAULT NULL COMMENT '流程编号',
  `OfficeOpenLab` varchar(50) DEFAULT NULL COMMENT '打开本地标签',
  `OfficeOpenEnable` int(11) DEFAULT NULL COMMENT '是否启用',
  `OfficeOpenTemplateLab` varchar(50) DEFAULT NULL COMMENT '打开模板标签',
  `OfficeOpenTemplateEnable` int(11) DEFAULT NULL COMMENT '是否启用',
  `OfficeSaveLab` varchar(50) DEFAULT NULL COMMENT '保存标签',
  `OfficeSaveEnable` int(11) DEFAULT NULL COMMENT '是否启用',
  `OfficeAcceptLab` varchar(50) DEFAULT NULL COMMENT '接受修订标签',
  `OfficeAcceptEnable` int(11) DEFAULT NULL COMMENT '是否启用',
  `OfficeRefuseLab` varchar(50) DEFAULT NULL COMMENT '拒绝修订标签',
  `OfficeRefuseEnable` int(11) DEFAULT NULL COMMENT '是否启用',
  `OfficeOverLab` varchar(50) DEFAULT NULL COMMENT '套红按钮标签',
  `OfficeOverEnable` int(11) DEFAULT NULL COMMENT '是否启用',
  `OfficeMarksEnable` int(11) DEFAULT NULL COMMENT '是否查看用户留痕',
  `OfficePrintLab` varchar(50) DEFAULT NULL COMMENT '打印按钮标签',
  `OfficePrintEnable` int(11) DEFAULT NULL COMMENT '是否启用',
  `OfficeSealLab` varchar(50) DEFAULT NULL COMMENT '签章按钮标签',
  `OfficeSealEnable` int(11) DEFAULT NULL COMMENT '是否启用',
  `OfficeInsertFlowLab` varchar(50) DEFAULT NULL COMMENT '插入流程标签',
  `OfficeInsertFlowEnable` int(11) DEFAULT NULL COMMENT '是否启用',
  `OfficeNodeInfo` int(11) DEFAULT NULL COMMENT '是否记录节点信息',
  `OfficeReSavePDF` int(11) DEFAULT NULL COMMENT '是否该自动保存为PDF',
  `OfficeDownLab` varchar(50) DEFAULT NULL COMMENT '下载按钮标签',
  `OfficeDownEnable` int(11) DEFAULT NULL COMMENT '是否启用',
  `OfficeIsMarks` int(11) DEFAULT NULL COMMENT '是否进入留痕模式',
  `OfficeTemplate` varchar(100) DEFAULT NULL COMMENT '指定文档模板',
  `OfficeIsParent` int(11) DEFAULT NULL COMMENT '是否使用父流程的文档',
  `OfficeTHEnable` int(11) DEFAULT NULL COMMENT '是否自动套红',
  `OfficeTHTemplate` varchar(200) DEFAULT NULL COMMENT '自动套红模板',
  `FrmType` varchar(20) DEFAULT NULL COMMENT '表单类型',
  `IsPrint` int(11) DEFAULT NULL COMMENT '是否可以打印',
  `IsEnableLoadData` int(11) DEFAULT NULL COMMENT '是否启用装载填充事件',
  `Idx` int(11) DEFAULT NULL COMMENT '顺序号',
  `FrmSln` int(11) DEFAULT NULL COMMENT '表单控制方案',
  `WhoIsPK` int(11) DEFAULT NULL COMMENT '谁是主键？',
  `Is1ToN` int(11) DEFAULT NULL COMMENT '是否1变N？',
  `HuiZong` varchar(300) DEFAULT NULL COMMENT '子线程要汇总的数据表',
  `FrmEnableRole` int(11) DEFAULT NULL COMMENT '表单启用规则',
  `FrmEnableExp` varchar(4000) DEFAULT NULL COMMENT '启用的表达式',
  `TempleteFile` varchar(500) DEFAULT NULL COMMENT '模版文件',
  `IsEnable` int(11) DEFAULT NULL COMMENT '是否显示',
  `GuanJianZiDuan` varchar(20) DEFAULT NULL COMMENT '关键字段',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='节点表单';

-- ----------------------------
-- Records of wf_frmnode
-- ----------------------------

-- ----------------------------
-- Table structure for `wf_generfh`
-- ----------------------------
DROP TABLE IF EXISTS `wf_generfh`;
CREATE TABLE `wf_generfh` (
  `OID` int(11) NOT NULL COMMENT '流程ID - 主键',
  `FID` int(11) DEFAULT NULL COMMENT '流程ID - 主键',
  `Title` text COMMENT '标题',
  `GroupKey` varchar(3000) DEFAULT NULL COMMENT '分组主键',
  `FK_Flow` varchar(500) DEFAULT NULL COMMENT '流程',
  `ToEmpsMsg` text COMMENT '接受人员',
  `FK_Node` int(11) DEFAULT NULL COMMENT '停留节点',
  `WFState` int(11) DEFAULT NULL COMMENT 'WFState',
  `RDT` varchar(50) DEFAULT NULL COMMENT 'RDT',
  PRIMARY KEY (`OID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='分合流程控制';

-- ----------------------------
-- Records of wf_generfh
-- ----------------------------

-- ----------------------------
-- Table structure for `wf_generworkerlist`
-- ----------------------------
DROP TABLE IF EXISTS `wf_generworkerlist`;
CREATE TABLE `wf_generworkerlist` (
  `WorkID` int(11) NOT NULL COMMENT '工作ID - 主键',
  `FK_Emp` varchar(20) NOT NULL COMMENT '人员 - 主键',
  `FK_Node` int(11) NOT NULL COMMENT '节点ID - 主键',
  `FID` int(11) DEFAULT NULL COMMENT '流程ID',
  `FK_EmpText` varchar(30) DEFAULT NULL COMMENT '人员名称',
  `FK_NodeText` varchar(100) DEFAULT NULL COMMENT '节点名称',
  `FK_Flow` varchar(3) DEFAULT NULL COMMENT '流程',
  `FK_Dept` varchar(100) DEFAULT NULL COMMENT '使用部门',
  `SDT` varchar(50) DEFAULT NULL COMMENT '应完成日期',
  `DTOfWarning` varchar(50) DEFAULT NULL COMMENT '警告日期',
  `WarningDays` float DEFAULT NULL COMMENT '预警天',
  `RDT` varchar(50) DEFAULT NULL COMMENT '记录时间',
  `CDT` varchar(50) DEFAULT NULL COMMENT '完成时间',
  `IsEnable` int(11) DEFAULT NULL COMMENT '是否可用',
  `IsRead` int(11) DEFAULT NULL COMMENT '是否读取',
  `IsPass` int(11) DEFAULT NULL COMMENT '是否通过(对合流节点有效)',
  `WhoExeIt` int(11) DEFAULT NULL COMMENT '谁执行它',
  `Sender` varchar(200) DEFAULT NULL COMMENT '发送人',
  `PRI` int(11) DEFAULT NULL COMMENT '优先级',
  `PressTimes` int(11) DEFAULT NULL COMMENT '催办次数',
  `DTOfHungUp` varchar(50) DEFAULT NULL COMMENT '挂起时间',
  `DTOfUnHungUp` varchar(50) DEFAULT NULL COMMENT '预计解除挂起时间',
  `HungUpTimes` int(11) DEFAULT NULL COMMENT '挂起次数',
  `GuestNo` varchar(30) DEFAULT NULL COMMENT '外部用户编号',
  `GuestName` varchar(100) DEFAULT NULL COMMENT '外部用户名称',
  `AtPara` text COMMENT 'AtPara',
  PRIMARY KEY (`WorkID`,`FK_Emp`,`FK_Node`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='工作者';

-- ----------------------------
-- Records of wf_generworkerlist
-- ----------------------------

-- ----------------------------
-- Table structure for `wf_generworkflow`
-- ----------------------------
DROP TABLE IF EXISTS `wf_generworkflow`;
CREATE TABLE `wf_generworkflow` (
  `WorkID` int(11) NOT NULL COMMENT 'WorkID - 主键',
  `StarterName` varchar(200) DEFAULT NULL COMMENT '发起人名称',
  `Title` varchar(1000) DEFAULT NULL COMMENT '标题',
  `WFSta` int(11) DEFAULT NULL COMMENT '  状态,枚举类型:0 运行中 1 已完成 2 其他',
  `WFState` int(11) DEFAULT NULL COMMENT '           流程状态,枚举类型:0 空白 1 草稿 2 运行中 3 已完成 4 挂起 5 退回 6 转发 7 删除 8 加签 9 冻结 10 批处理 11 加签回复状态',
  `NodeName` varchar(100) DEFAULT NULL COMMENT '节点名称',
  `RDT` varchar(50) DEFAULT NULL COMMENT '记录日期',
  `BillNo` varchar(100) DEFAULT NULL COMMENT '单据编号',
  `FlowNote` text COMMENT '备注',
  `FK_FlowSort` varchar(100) DEFAULT NULL COMMENT '流程类别',
  `FK_Flow` varchar(100) DEFAULT NULL COMMENT '流程',
  `FK_Dept` varchar(100) DEFAULT NULL COMMENT '部门',
  `FID` int(11) DEFAULT NULL COMMENT '流程ID',
  `FK_Node` int(11) DEFAULT NULL COMMENT '节点',
  `FK_NY` varchar(100) DEFAULT NULL COMMENT '年月',
  `MyNum` int(11) DEFAULT NULL COMMENT '个数',
  `FlowName` varchar(100) DEFAULT NULL COMMENT '流程名称',
  `Starter` varchar(200) DEFAULT NULL COMMENT '发起人',
  `Sender` varchar(200) DEFAULT NULL COMMENT '发送人',
  `DeptName` varchar(100) DEFAULT NULL COMMENT '部门名称',
  `PRI` int(11) DEFAULT NULL COMMENT '优先级',
  `SDTOfNode` varchar(50) DEFAULT NULL COMMENT '节点应完成时间',
  `SDTOfFlow` varchar(50) DEFAULT NULL COMMENT '流程应完成时间',
  `PFlowNo` varchar(3) DEFAULT NULL COMMENT '父流程编号',
  `PWorkID` int(11) DEFAULT NULL COMMENT '父流程ID',
  `PNodeID` int(11) DEFAULT NULL COMMENT '父流程调用节点',
  `PFID` int(11) DEFAULT NULL COMMENT '父流程调用的PFID',
  `PEmp` varchar(32) DEFAULT NULL COMMENT '子流程的调用人',
  `CFlowNo` varchar(3) DEFAULT NULL COMMENT '延续流程编号',
  `CWorkID` int(11) DEFAULT NULL COMMENT '延续流程ID',
  `GuestNo` varchar(100) DEFAULT NULL COMMENT '客户编号',
  `GuestName` varchar(100) DEFAULT NULL COMMENT '客户名称',
  `TodoEmps` text COMMENT '待办人员',
  `TodoEmpsNum` int(11) DEFAULT NULL COMMENT '待办人员数量',
  `TaskSta` int(11) DEFAULT NULL COMMENT '共享状态',
  `AtPara` varchar(2000) DEFAULT NULL COMMENT '参数(流程运行设置临时存储的参数)',
  `Emps` text COMMENT '参与人',
  `GUID` varchar(36) DEFAULT NULL COMMENT 'GUID',
  `SysType` varchar(100) DEFAULT NULL COMMENT '系统类别',
  `WeekNum` int(11) DEFAULT NULL COMMENT '周次',
  `TSpan` int(11) DEFAULT NULL COMMENT '时间间隔',
  `TodoSta` int(11) DEFAULT NULL COMMENT '待办状态',
  PRIMARY KEY (`WorkID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='流程实例';

-- ----------------------------
-- Records of wf_generworkflow
-- ----------------------------

-- ----------------------------
-- Table structure for `wf_hungup`
-- ----------------------------
DROP TABLE IF EXISTS `wf_hungup`;
CREATE TABLE `wf_hungup` (
  `MyPK` varchar(100) NOT NULL COMMENT 'MyPK - 主键',
  `FK_Node` int(11) DEFAULT NULL COMMENT '节点ID',
  `WorkID` int(11) DEFAULT NULL COMMENT 'WorkID',
  `HungUpWay` int(11) DEFAULT NULL COMMENT '  挂起方式,枚举类型:0 无限挂起 1 按指定的时间解除挂起并通知我自己 2 按指定的时间解除挂起并通知所有人',
  `Note` text COMMENT '挂起原因(标题与内容支持变量)',
  `Rec` varchar(50) DEFAULT NULL COMMENT '挂起人',
  `DTOfHungUp` varchar(50) DEFAULT NULL COMMENT '挂起时间',
  `DTOfUnHungUp` varchar(50) DEFAULT NULL COMMENT '实际解除挂起时间',
  `DTOfUnHungUpPlan` varchar(50) DEFAULT NULL COMMENT '预计解除挂起时间',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='挂起';

-- ----------------------------
-- Records of wf_hungup
-- ----------------------------

-- ----------------------------
-- Table structure for `wf_labnote`
-- ----------------------------
DROP TABLE IF EXISTS `wf_labnote`;
CREATE TABLE `wf_labnote` (
  `MyPK` varchar(100) NOT NULL COMMENT 'MyPK - 主键',
  `Name` varchar(3000) DEFAULT NULL COMMENT 'null',
  `FK_Flow` varchar(100) DEFAULT NULL COMMENT '流程',
  `X` int(11) DEFAULT NULL COMMENT 'X坐标',
  `Y` int(11) DEFAULT NULL COMMENT 'Y坐标',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='标签';

-- ----------------------------
-- Records of wf_labnote
-- ----------------------------

-- ----------------------------
-- Table structure for `wf_node`
-- ----------------------------
DROP TABLE IF EXISTS `wf_node`;
CREATE TABLE `wf_node` (
  `NodeID` int(11) NOT NULL COMMENT 'NodeID - 主键',
  `Step` int(11) DEFAULT NULL COMMENT '步骤(无计算意义)',
  `FK_Flow` varchar(10) DEFAULT NULL COMMENT '流程编号',
  `Name` varchar(200) DEFAULT NULL COMMENT '节点名称',
  `Tip` varchar(100) DEFAULT NULL COMMENT '操作提示',
  `WhoExeIt` int(11) DEFAULT NULL COMMENT '  谁执行它,枚举类型:0 操作员执行 1 机器执行 2 混合执行',
  `CondModel` int(11) DEFAULT NULL COMMENT '  方向条件控制规则,枚举类型:0 由连接线条件控制 1 让用户手工选择 2 发送按钮旁下拉框选择',
  `CancelRole` int(11) DEFAULT NULL COMMENT '   撤销规则,枚举类型:0 上一步可以撤销 1 不能撤销 2 上一步与开始节点可以撤销 3 指定的节点可以撤销',
  `IsTask` int(11) DEFAULT NULL COMMENT '允许分配工作否?',
  `IsRM` int(11) DEFAULT NULL COMMENT '是否启用投递路径自动记忆功能?',
  `DTFrom` varchar(50) DEFAULT NULL COMMENT '生命周期从',
  `DTTo` varchar(50) DEFAULT NULL COMMENT '生命周期到',
  `IsBUnit` int(11) DEFAULT NULL COMMENT '是否是节点模版（业务单元）?',
  `FocusField` varchar(50) DEFAULT NULL COMMENT '焦点字段',
  `SaveModel` int(11) DEFAULT NULL COMMENT ' 保存方式,枚举类型:0 仅节点表 1 节点表与Rpt表',
  `IsGuestNode` int(11) DEFAULT NULL COMMENT '是否是外部用户执行的节点(非组织结构人员参与处理工作的节点)?',
  `SelfParas` varchar(500) DEFAULT NULL COMMENT '自定义参数',
  `RunModel` int(11) DEFAULT NULL COMMENT '    节点类型,枚举类型:0 普通 1 合流 2 分流 3 分合流 4 子线程',
  `SubThreadType` int(11) DEFAULT NULL COMMENT ' 子线程类型,枚举类型:0 同表单 1 异表单',
  `PassRate` float DEFAULT NULL COMMENT '完成通过率',
  `SubFlowStartWay` int(11) DEFAULT NULL COMMENT '  子线程启动方式,枚举类型:0 不启动 1 指定的字段启动 2 按明细表启动',
  `SubFlowStartParas` varchar(100) DEFAULT NULL COMMENT '启动参数',
  `ThreadIsCanDel` int(11) DEFAULT NULL COMMENT '是否可以删除子线程(当前节点已经发送出去的线程，并且当前节点是分流，或者分合流有效，在子线程退回后的操作)？',
  `ThreadIsCanShift` int(11) DEFAULT NULL COMMENT '是否可以移交子线程(当前节点已经发送出去的线程，并且当前节点是分流，或者分合流有效，在子线程退回后的操作)？',
  `TodolistModel` int(11) DEFAULT NULL COMMENT '    待办处理模式,枚举类型:0 抢办模式 1 协作模式 2 队列模式 3 共享模式 4 协作组长模式',
  `IsAllowRepeatEmps` int(11) DEFAULT NULL COMMENT '是否允许子线程接受人员重复(仅当分流点向子线程发送时有效)?',
  `AutoRunEnable` int(11) DEFAULT NULL COMMENT '是否启用自动运行？(仅当分流点向子线程发送时有效)',
  `AutoRunParas` varchar(100) DEFAULT NULL COMMENT '自动运行SQL',
  `AutoJumpRole0` int(11) DEFAULT NULL COMMENT '处理人就是发起人',
  `AutoJumpRole1` int(11) DEFAULT NULL COMMENT '处理人已经出现过',
  `AutoJumpRole2` int(11) DEFAULT NULL COMMENT '处理人与上一步相同',
  `WhenNoWorker` int(11) DEFAULT NULL COMMENT '(是)找不到人就跳转,(否)提示错误.',
  `SendLab` varchar(50) DEFAULT NULL COMMENT '发送按钮标签',
  `SendJS` varchar(999) DEFAULT NULL COMMENT '按钮JS函数',
  `SaveLab` varchar(50) DEFAULT NULL COMMENT '保存按钮标签',
  `SaveEnable` int(11) DEFAULT NULL COMMENT '是否启用',
  `ThreadLab` varchar(50) DEFAULT NULL COMMENT '子线程按钮标签',
  `ThreadEnable` int(11) DEFAULT NULL COMMENT '是否启用',
  `ThreadKillRole` int(11) DEFAULT NULL COMMENT '  子线程删除方式,枚举类型:0 不能删除 1 手工删除 2 自动删除',
  `SubFlowLab` varchar(50) DEFAULT NULL COMMENT '子流程按钮标签',
  `SubFlowEnable` int(11) DEFAULT NULL COMMENT '是否启用',
  `JumpWayLab` varchar(50) DEFAULT NULL COMMENT '跳转按钮标签',
  `JumpWay` int(11) DEFAULT NULL COMMENT '    跳转规则,枚举类型:0 不能跳转 1 只能向后跳转 2 只能向前跳转 3 任意节点跳转 4 按指定规则跳转',
  `JumpToNodes` varchar(200) DEFAULT NULL COMMENT '可跳转的节点',
  `ReturnLab` varchar(50) DEFAULT NULL COMMENT '退回按钮标签',
  `ReturnRole` int(11) DEFAULT NULL COMMENT '    退回规则,枚举类型:0 不能退回 1 只能退回上一个节点 2 可退回以前任意节点 3 可退回指定的节点 4 由流程图设计的退回路线决定',
  `ReturnAlert` varchar(999) DEFAULT NULL COMMENT '被退回后信息提示',
  `IsBackTracking` int(11) DEFAULT NULL COMMENT '是否可以原路返回(启用退回功能才有效)',
  `ReturnField` varchar(50) DEFAULT NULL COMMENT '退回信息填写字段',
  `ReturnReasonsItems` varchar(999) DEFAULT NULL COMMENT '退回原因',
  `CCLab` varchar(50) DEFAULT NULL COMMENT '抄送按钮标签',
  `CCRole` int(11) DEFAULT NULL COMMENT '     抄送规则,枚举类型:0 不能抄送 1 手工抄送 2 自动抄送 3 手工与自动 4 按表单SysCCEmps字段计算 5 在发送前打开抄送窗口',
  `CCWriteTo` int(11) DEFAULT NULL COMMENT '  抄送写入规则,枚举类型:0 写入抄送列表 1 写入待办 2 写入待办与抄送列表',
  `ShiftLab` varchar(50) DEFAULT NULL COMMENT '移交按钮标签',
  `ShiftEnable` int(11) DEFAULT NULL COMMENT '是否启用',
  `DelLab` varchar(50) DEFAULT NULL COMMENT '删除按钮标签',
  `DelEnable` int(11) DEFAULT NULL COMMENT '    删除规则,枚举类型:0 不能删除 1 逻辑删除 2 记录日志方式删除 3 彻底删除 4 让用户决定删除方式',
  `EndFlowLab` varchar(50) DEFAULT NULL COMMENT '结束流程按钮标签',
  `EndFlowEnable` int(11) DEFAULT NULL COMMENT '是否启用',
  `PrintDocLab` varchar(50) DEFAULT NULL COMMENT '打印单据按钮标签',
  `PrintDocEnable` int(11) DEFAULT NULL COMMENT '   打印方式,枚举类型:0 不打印 1 打印网页 2 打印RTF模板 3 打印Word模版',
  `PrintHtmlLab` varchar(50) DEFAULT NULL COMMENT '打印Html标签',
  `PrintHtmlEnable` int(11) DEFAULT NULL COMMENT '是否启用',
  `PrintPDFLab` varchar(50) DEFAULT NULL COMMENT '打印pdf标签',
  `PrintPDFEnable` int(11) DEFAULT NULL COMMENT '是否启用',
  `PrintZipLab` varchar(50) DEFAULT NULL COMMENT '打包下载zip按钮标签',
  `PrintZipEnable` int(11) DEFAULT NULL COMMENT '是否启用',
  `TrackLab` varchar(50) DEFAULT NULL COMMENT '轨迹按钮标签',
  `TrackEnable` int(11) DEFAULT NULL COMMENT '是否启用',
  `HungLab` varchar(50) DEFAULT NULL COMMENT '挂起按钮标签',
  `HungEnable` int(11) DEFAULT NULL COMMENT '是否启用',
  `SelectAccepterLab` varchar(50) DEFAULT NULL COMMENT '接受人按钮标签',
  `SelectAccepterEnable` int(11) DEFAULT NULL COMMENT '   工作方式,枚举类型:0 不启用 1 单独启用 2 在发送前打开 3 转入新页面',
  `SearchLab` varchar(50) DEFAULT NULL COMMENT '查询按钮标签',
  `SearchEnable` int(11) DEFAULT NULL COMMENT '是否启用',
  `WorkCheckLab` varchar(50) DEFAULT NULL COMMENT '审核按钮标签',
  `WorkCheckEnable` int(11) DEFAULT NULL COMMENT '是否启用',
  `BatchLab` varchar(50) DEFAULT NULL COMMENT '批处理按钮标签',
  `BatchEnable` int(11) DEFAULT NULL COMMENT '是否启用',
  `AskforLab` varchar(50) DEFAULT NULL COMMENT '加签按钮标签',
  `AskforEnable` int(11) DEFAULT NULL COMMENT '是否启用',
  `HuiQianLab` varchar(50) DEFAULT NULL COMMENT '会签标签',
  `HuiQianRole` int(11) DEFAULT NULL COMMENT '  会签模式,枚举类型:0 不启用 1 协作模式 4 组长模式',
  `TCLab` varchar(50) DEFAULT NULL COMMENT '流转自定义',
  `TCEnable` int(11) DEFAULT NULL COMMENT '是否启用',
  `WebOffice` varchar(50) DEFAULT NULL COMMENT '文档按钮标签',
  `WebOfficeEnable` int(11) DEFAULT NULL COMMENT '   文档启用方式,枚举类型:0 不启用 1 按钮方式 2 标签页置后方式 3 标签页置前方式',
  `PRILab` varchar(50) DEFAULT NULL COMMENT '重要性',
  `PRIEnable` int(11) DEFAULT NULL COMMENT '是否启用',
  `CHLab` varchar(50) DEFAULT NULL COMMENT '节点时限',
  `CHEnable` int(11) DEFAULT NULL COMMENT '是否启用',
  `FocusLab` varchar(50) DEFAULT NULL COMMENT '关注',
  `FocusEnable` int(11) DEFAULT NULL COMMENT '是否启用',
  `FWCSta` int(11) DEFAULT NULL COMMENT '  审核组件状态,枚举类型:0 禁用 1 启用 2 只读',
  `FWCShowModel` int(11) DEFAULT NULL COMMENT ' 显示方式,枚举类型:0 表格方式 1 自由模式',
  `FWCType` int(11) DEFAULT NULL COMMENT '   工作方式,枚举类型:0 审核组件 1 日志组件 2 周报组件 3 月报组件',
  `FWCNodeName` varchar(100) DEFAULT NULL COMMENT '节点意见名称',
  `FWCAth` int(11) DEFAULT NULL COMMENT '   附件上传,枚举类型:0 不启用 1 多附件 2 单附件(暂不支持) 3 图片附件(暂不支持)',
  `FWCTrackEnable` int(11) DEFAULT NULL COMMENT '轨迹图是否显示？',
  `FWCListEnable` int(11) DEFAULT NULL COMMENT '历史审核信息是否显示？(否,仅出现意见框)',
  `FWCIsShowAllStep` int(11) DEFAULT NULL COMMENT '在轨迹表里是否显示所有的步骤？',
  `SigantureEnabel` int(11) DEFAULT NULL COMMENT '操作人是否显示为图片签名？',
  `FWCIsFullInfo` int(11) DEFAULT NULL COMMENT '如果用户未审核是否按照默认意见填充？',
  `FWCOpLabel` varchar(50) DEFAULT NULL COMMENT '操作名词(审核/审阅/批示)',
  `FWCDefInfo` varchar(50) DEFAULT NULL COMMENT '默认审核信息',
  `FWC_H` float DEFAULT NULL COMMENT '高度',
  `FWC_W` float DEFAULT NULL COMMENT '宽度',
  `FWCFields` varchar(50) DEFAULT NULL COMMENT '审批格式化字段',
  `FWCIsShowTruck` int(11) DEFAULT NULL COMMENT '是否显示未审核的轨迹？',
  `MPhone_WorkModel` int(11) DEFAULT NULL COMMENT '  手机工作模式,枚举类型:0 原生态 1 浏览器 2 禁用',
  `MPhone_SrcModel` int(11) DEFAULT NULL COMMENT '  手机屏幕模式,枚举类型:0 强制横屏 1 强制竖屏 2 由重力感应决定',
  `MPad_WorkModel` int(11) DEFAULT NULL COMMENT '  平板工作模式,枚举类型:0 原生态 1 浏览器 2 禁用',
  `MPad_SrcModel` int(11) DEFAULT NULL COMMENT '  平板屏幕模式,枚举类型:0 强制横屏 1 强制竖屏 2 由重力感应决定',
  `SFSta` int(11) DEFAULT NULL COMMENT '  控件状态,枚举类型:0 禁用 1 启用 2 只读',
  `SFShowModel` int(11) DEFAULT NULL COMMENT ' 显示方式,枚举类型:0 表格方式 1 自由模式',
  `SFCaption` varchar(100) DEFAULT NULL COMMENT '标题(显示控件头部)',
  `SFDefInfo` varchar(50) DEFAULT NULL COMMENT '可手工启动的子流程',
  `SFActiveFlows` varchar(100) DEFAULT NULL COMMENT '可自动触发的子流程',
  `SF_X` float(50,2) DEFAULT NULL COMMENT '位置X',
  `SF_Y` float(50,2) DEFAULT NULL COMMENT '位置Y',
  `SF_H` float(50,2) DEFAULT NULL COMMENT '高度',
  `SF_W` float(50,2) DEFAULT NULL COMMENT '宽度',
  `SFFields` varchar(50) DEFAULT NULL COMMENT '显示的字段列',
  `SFShowCtrl` int(11) DEFAULT NULL COMMENT ' 显示控制方式,枚举类型:0 可以看所有的子流程 1 仅仅可以看自己发起的子流程',
  `FWC_X` float(50,2) DEFAULT NULL COMMENT '位置X',
  `FWC_Y` float(50,2) DEFAULT NULL COMMENT '位置Y',
  `CheckNodes` varchar(800) DEFAULT NULL COMMENT '工作节点s',
  `DeliveryWay` int(11) DEFAULT NULL COMMENT '                 节点访问规则,枚举类型:0 01.按岗位智能计算 1 02.按节点绑定的部门计算 2 03.按设置的SQL获取接受人计算 3 04.按节点绑定的人员计算 4 05.由上一节点发送人通过“人员选择器”选择接受人 5 06.按上一节点表单指定的字段值作为本步骤的接受人 6 07.与上一节点处理人员相同 7 08.与开始节点处理人相同 8 09.与指定节点处理人相同 9 10.按绑定的岗位与部门交集计算 10 11.按绑定的岗位计算并且以绑定的部门集合为纬度 11 12.按指定节点的工作人员或者指定字段人员的岗位计算 12 13.按SQL确定子线程接受人与数据源 13 14.由上一节点的明细表来决定子线程的接受人 14 15.仅按绑定的岗位计算 15 16.由FEE来决定 16 17.按绑定部门计算,该部门一人处理标识该工作结束(子线程). 100 18.按ccflow的BPM模式处理',
  `ICON` varchar(70) DEFAULT NULL COMMENT '节点ICON图片路径',
  `NodeWorkType` int(11) DEFAULT NULL COMMENT '节点类型',
  `FlowName` varchar(100) DEFAULT NULL COMMENT '流程名',
  `FrmAttr` varchar(300) DEFAULT NULL COMMENT 'FrmAttr',
  `FWCOrderModel` int(11) DEFAULT NULL COMMENT ' 协作模式下操作员显示顺序,枚举类型:0 按审批时间先后排序 1 按照接受人员列表先后顺序(官职大小)',
  `TimeLimit` float(50,2) DEFAULT NULL COMMENT '限期(天)',
  `TSpanHour` float(50,2) DEFAULT NULL COMMENT '小时',
  `TWay` int(11) DEFAULT NULL COMMENT '时间计算方式',
  `TAlertRole` int(11) DEFAULT NULL COMMENT '逾期提醒规则',
  `TAlertWay` int(11) DEFAULT NULL COMMENT '逾期提醒方式',
  `WarningDay` float(50,2) DEFAULT NULL COMMENT '工作预警(天)',
  `WAlertRole` int(11) DEFAULT NULL COMMENT '预警提醒规则',
  `WAlertWay` int(11) DEFAULT NULL COMMENT '预警提醒方式',
  `TCent` float(50,2) DEFAULT NULL COMMENT '扣分(每延期1小时)',
  `CHWay` int(11) DEFAULT NULL COMMENT '  考核方式,枚举类型:0 不考核 1 按时效 2 按工作量',
  `IsEval` int(11) DEFAULT NULL COMMENT '是否质量考核点',
  `OutTimeDeal` int(11) DEFAULT NULL COMMENT '      超时处理,枚举类型:0 不处理 1 自动向下运动 2 自动跳转指定的节点 3 自动移交给指定的人员 4 向指定的人员发消息 5 删除流程 6 执行SQL',
  `DoOutTime` varchar(300) DEFAULT NULL COMMENT '处理内容',
  `Doc` varchar(100) DEFAULT NULL COMMENT '描述',
  `IsExpSender` int(11) DEFAULT NULL COMMENT '本节点接收人不允许包含上一步发送人?',
  `DeliveryParas` varchar(600) DEFAULT NULL COMMENT '访问规则设置内容',
  `NodeFrmID` varchar(50) DEFAULT NULL COMMENT '节点表单ID',
  `IsCanRpt` int(11) DEFAULT NULL COMMENT '是否可以查看工作报告?',
  `IsCanOver` int(11) DEFAULT NULL COMMENT '是否可以终止流程',
  `IsSecret` int(11) DEFAULT NULL COMMENT '是否是保密步骤',
  `IsCanDelFlow` int(11) DEFAULT NULL COMMENT '是否可以删除流程',
  `TeamLeaderConfirmRole` int(11) DEFAULT NULL COMMENT '组长确认规则',
  `TeamLeaderConfirmDoc` varchar(100) DEFAULT NULL COMMENT '组长确认设置内容',
  `IsHandOver` int(11) DEFAULT NULL COMMENT '是否可以移交',
  `BlockModel` int(11) DEFAULT NULL COMMENT '    发送阻塞模式,枚举类型:0 不阻塞 1 当前节点有未完成的子流程时 2 按约定格式阻塞未完成子流程 3 按照SQL阻塞 4 按照表达式阻塞',
  `BlockExp` varchar(700) DEFAULT NULL COMMENT '阻塞表达式',
  `BlockAlert` varchar(700) DEFAULT NULL COMMENT '被阻塞时提示信息',
  `ReadReceipts` int(11) DEFAULT NULL COMMENT '   已读回执,枚举类型:0 不回执 1 自动回执 2 由上一节点表单字段决定 3 由SDK开发者参数决定',
  `BatchRole` int(11) DEFAULT NULL COMMENT '  工作批处理,枚举类型:0 不可以批处理 1 批量审核 2 分组批量审核',
  `BatchListCount` int(11) DEFAULT NULL COMMENT '批处理数量',
  `BatchParas` varchar(500) DEFAULT NULL COMMENT '批处理参数',
  `FormType` int(11) DEFAULT NULL COMMENT '          节点表单方案,枚举类型:0 傻瓜表单(ccflow6取消支持) 1 自由表单 2 嵌入式表单 3 SDK表单 4 SL表单(ccflow6取消支持) 5 表单树 6 动态表单树 7 公文表单(WebOffice) 8 Excel表单(测试中) 9 Word表单(测试中) 100 禁用(对多表单流程有效)',
  `FormUrl` varchar(200) DEFAULT NULL COMMENT '表单URL',
  `TurnToDeal` int(11) DEFAULT NULL COMMENT '   发送后转向,枚举类型:0 提示ccflow默认信息 1 提示指定信息 2 转向指定的url 3 按照条件转向',
  `TurnToDealDoc` varchar(200) DEFAULT NULL COMMENT '转向处理内容',
  `NodePosType` int(11) DEFAULT NULL COMMENT '位置',
  `IsCCFlow` int(11) DEFAULT NULL COMMENT '是否有流程完成条件',
  `HisStas` varchar(3000) DEFAULT NULL COMMENT '岗位',
  `HisDeptStrs` varchar(3000) DEFAULT NULL COMMENT '部门',
  `HisToNDs` varchar(50) DEFAULT NULL COMMENT '转到的节点',
  `HisBillIDs` varchar(50) DEFAULT NULL COMMENT '单据IDs',
  `HisSubFlows` varchar(30) DEFAULT NULL COMMENT 'HisSubFlows',
  `PTable` varchar(100) DEFAULT NULL COMMENT '物理表',
  `ShowSheets` varchar(100) DEFAULT NULL COMMENT '显示的表单',
  `GroupStaNDs` varchar(100) DEFAULT NULL COMMENT '岗位分组节点',
  `X` int(11) DEFAULT NULL COMMENT 'X坐标',
  `Y` int(11) DEFAULT NULL COMMENT 'Y坐标',
  `RefOneFrmTreeType` varchar(100) DEFAULT NULL COMMENT '独立表单类型',
  `AtPara` varchar(500) DEFAULT NULL COMMENT 'AtPara',
  `AllotLab` varchar(50) DEFAULT NULL COMMENT '分配按钮标签',
  `AllotEnable` int(11) DEFAULT NULL COMMENT '是否启用',
  `ConfirmLab` varchar(50) DEFAULT NULL COMMENT '确认按钮标签',
  `ConfirmEnable` int(11) DEFAULT NULL COMMENT '是否启用',
  `OfficeOpenLab` varchar(50) DEFAULT NULL COMMENT '打开本地标签',
  `OfficeOpenEnable` int(11) DEFAULT NULL COMMENT '是否启用',
  `OfficeOpenTemplateLab` varchar(50) DEFAULT NULL COMMENT '打开模板标签',
  `OfficeOpenTemplateEnable` int(11) DEFAULT NULL COMMENT '是否启用',
  `OfficeSaveLab` varchar(50) DEFAULT NULL COMMENT '保存标签',
  `OfficeSaveEnable` int(11) DEFAULT NULL COMMENT '是否启用',
  `OfficeAcceptLab` varchar(50) DEFAULT NULL COMMENT '接受修订标签',
  `OfficeAcceptEnable` int(11) DEFAULT NULL COMMENT '是否启用',
  `OfficeRefuseLab` varchar(50) DEFAULT NULL COMMENT '拒绝修订标签',
  `OfficeRefuseEnable` int(11) DEFAULT NULL COMMENT '是否启用',
  `OfficeOverLab` varchar(50) DEFAULT NULL COMMENT '套红标签',
  `OfficeOverEnable` int(11) DEFAULT NULL COMMENT '是否启用',
  `OfficeMarksEnable` int(11) DEFAULT NULL COMMENT '是否查看用户留痕',
  `OfficePrintLab` varchar(50) DEFAULT NULL COMMENT '打印标签',
  `OfficePrintEnable` int(11) DEFAULT NULL COMMENT '是否启用',
  `OfficeSealLab` varchar(50) DEFAULT NULL COMMENT '签章标签',
  `OfficeSealEnable` int(11) DEFAULT NULL COMMENT '是否启用',
  `OfficeInsertFlowLab` varchar(50) DEFAULT NULL COMMENT '插入流程标签',
  `OfficeInsertFlowEnable` int(11) DEFAULT NULL COMMENT '是否启用',
  `OfficeNodeInfo` int(11) DEFAULT NULL COMMENT '是否记录节点信息',
  `OfficeReSavePDF` int(11) DEFAULT NULL COMMENT '是否该自动保存为PDF',
  `OfficeDownLab` varchar(50) DEFAULT NULL COMMENT '下载按钮标签',
  `OfficeDownEnable` int(11) DEFAULT NULL COMMENT '是否启用',
  `OfficeIsMarks` int(11) DEFAULT NULL COMMENT '是否进入留痕模式',
  `OfficeTemplate` varchar(100) DEFAULT NULL COMMENT '指定文档模板',
  `OfficeIsParent` int(11) DEFAULT NULL COMMENT '是否使用父流程的文档',
  `OfficeTHEnable` int(11) DEFAULT NULL COMMENT '是否自动套红',
  `OfficeTHTemplate` varchar(200) DEFAULT NULL COMMENT '自动套红模板',
  `OfficeOpen` varchar(50) DEFAULT NULL COMMENT '打开本地标签',
  `OfficeOpenTemplate` varchar(50) DEFAULT NULL COMMENT '打开模板标签',
  `OfficeSave` varchar(50) DEFAULT NULL COMMENT '保存标签',
  `OfficeAccept` varchar(50) DEFAULT NULL COMMENT '接受修订标签',
  `OfficeRefuse` varchar(50) DEFAULT NULL COMMENT '拒绝修订标签',
  `OfficeOver` varchar(50) DEFAULT NULL COMMENT '套红按钮标签',
  `OfficeMarks` int(11) DEFAULT NULL COMMENT '是否查看用户留痕',
  `OfficeReadOnly` int(11) DEFAULT NULL COMMENT '是否只读',
  `OfficePrint` varchar(50) DEFAULT NULL COMMENT '打印按钮标签',
  `OfficeSeal` varchar(50) DEFAULT NULL COMMENT '签章按钮标签',
  `OfficeInsertFlow` varchar(50) DEFAULT NULL COMMENT '插入流程标签',
  `OfficeIsTrueTH` int(11) DEFAULT NULL COMMENT '是否自动套红',
  `WebOfficeFrmModel` int(11) DEFAULT NULL COMMENT '表单工作方式,枚举类型:',
  `CCIsStations` int(11) DEFAULT NULL COMMENT '按照岗位抄送',
  `CCIsDepts` int(11) DEFAULT NULL COMMENT '按照部门抄送',
  `CCIsEmps` int(11) DEFAULT NULL COMMENT '按照人员抄送',
  `CCIsSQLs` int(11) DEFAULT NULL COMMENT '按照SQL抄送',
  `CCSQL` varchar(100) DEFAULT NULL COMMENT 'SQL表达式',
  `CCTitle` varchar(100) DEFAULT NULL COMMENT '抄送标题',
  `CCDoc` text COMMENT '抄送内容(标题与内容支持变量)',
  `FWCLab` varchar(200) DEFAULT NULL COMMENT '显示标签',
  `SFLab` varchar(200) DEFAULT NULL COMMENT '显示标签',
  `SFOpenType` int(11) DEFAULT NULL COMMENT ' 打开子流程显示,枚举类型:0 工作查看器 1 傻瓜表单轨迹查看器',
  `FrmThreadLab` varchar(200) DEFAULT NULL COMMENT '显示标签',
  `FrmThreadSta` int(11) DEFAULT NULL COMMENT ' 组件状态,枚举类型:0 禁用 1 启用',
  `FrmThread_X` float(50,2) DEFAULT NULL COMMENT '位置X',
  `FrmThread_Y` float(50,2) DEFAULT NULL COMMENT '位置Y',
  `FrmThread_H` float(50,2) DEFAULT NULL COMMENT '高度',
  `FrmThread_W` float(50,2) DEFAULT NULL COMMENT '宽度',
  `FrmTrackLab` varchar(200) DEFAULT NULL COMMENT '显示标签',
  `FrmTrackSta` int(11) DEFAULT NULL COMMENT '   组件状态,枚举类型:0 禁用 1 标准风格 2 华东院风格 3 华夏银行风格',
  `FrmTrack_X` float(50,2) DEFAULT NULL COMMENT '位置X',
  `FrmTrack_Y` float(50,2) DEFAULT NULL COMMENT '位置Y',
  `FrmTrack_H` float(50,2) DEFAULT NULL COMMENT '高度',
  `FrmTrack_W` float(50,2) DEFAULT NULL COMMENT '宽度',
  `FTCLab` varchar(50) DEFAULT NULL COMMENT '显示标签',
  `FTCSta` int(11) DEFAULT NULL COMMENT '  组件状态,枚举类型:0 禁用 1 只读 2 可设置人员',
  `FTCWorkModel` int(11) DEFAULT NULL COMMENT ' 工作模式,枚举类型:0 简洁模式 1 高级模式',
  `FTC_X` float(50,2) DEFAULT NULL COMMENT '位置X',
  `FTC_Y` float(50,2) DEFAULT NULL COMMENT '位置Y',
  `FTC_H` float(50,2) DEFAULT NULL COMMENT '高度',
  `FTC_W` float(50,2) DEFAULT NULL COMMENT '宽度',
  `WarningHour` float(50,2) DEFAULT NULL COMMENT '工作预警(小时)',
  `DoOutTimeCond` varchar(100) DEFAULT NULL COMMENT '执行超时条件',
  `OfficeIsDown` int(11) DEFAULT NULL COMMENT '是否启用',
  `SelectorModel` int(11) DEFAULT NULL COMMENT '       显示方式,枚举类型:0 按岗位 1 按部门 2 按人员 3 按SQL 4 按SQL模版计算 5 使用通用人员选择器 6 部门与岗位的交集 7 自定义Url',
  `FK_SQLTemplate` varchar(50) DEFAULT NULL COMMENT 'SQL模版',
  `FK_SQLTemplateText` varchar(50) DEFAULT NULL COMMENT 'SQL模版',
  `IsAutoLoadEmps` int(11) DEFAULT NULL COMMENT '是否自动加载上一次选择的人员？',
  `IsSimpleSelector` int(11) DEFAULT NULL COMMENT '是否单项选择(只能选择一个人)？',
  `SelectorP1` text COMMENT '分组参数:可以为空,比如:SELECT No,Name,ParentNo FROM  Port_Dept',
  `SelectorP2` text COMMENT '操作员数据源:比如:SELECT No,Name,FK_Dept FROM  Port_Emp',
  `SelectorP3` text COMMENT '默认选择的数据源:比如:SELECT FK_Emp FROM  WF_GenerWorkerList WHERE FK_Node=102 AND WorkID=@WorkID',
  `SelectorP4` text COMMENT '强制选择的数据源:比如:SELECT FK_Emp FROM  WF_GenerWorkerList WHERE FK_Node=102 AND WorkID=@WorkID',
  PRIMARY KEY (`NodeID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='选择器';

-- ----------------------------
-- Records of wf_node
-- ----------------------------

-- ----------------------------
-- Table structure for `wf_nodecancel`
-- ----------------------------
DROP TABLE IF EXISTS `wf_nodecancel`;
CREATE TABLE `wf_nodecancel` (
  `FK_Node` int(11) NOT NULL COMMENT '节点 - 主键',
  `CancelTo` int(11) NOT NULL COMMENT '撤销到 - 主键',
  PRIMARY KEY (`FK_Node`,`CancelTo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='可撤销的节点';

-- ----------------------------
-- Records of wf_nodecancel
-- ----------------------------

-- ----------------------------
-- Table structure for `wf_nodedept`
-- ----------------------------
DROP TABLE IF EXISTS `wf_nodedept`;
CREATE TABLE `wf_nodedept` (
  `FK_Node` int(11) NOT NULL COMMENT '节点 - 主键',
  `FK_Dept` varchar(100) NOT NULL COMMENT '部门,主外键:对应物理表:Port_Dept,表描述:部门',
  PRIMARY KEY (`FK_Node`,`FK_Dept`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='节点部门';

-- ----------------------------
-- Records of wf_nodedept
-- ----------------------------

-- ----------------------------
-- Table structure for `wf_nodeemp`
-- ----------------------------
DROP TABLE IF EXISTS `wf_nodeemp`;
CREATE TABLE `wf_nodeemp` (
  `FK_Node` int(11) NOT NULL COMMENT 'Node - 主键',
  `FK_Emp` varchar(100) NOT NULL COMMENT '到人员,主外键:对应物理表:Port_Emp,表描述:用户',
  PRIMARY KEY (`FK_Node`,`FK_Emp`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='节点人员';

-- ----------------------------
-- Records of wf_nodeemp
-- ----------------------------

-- ----------------------------
-- Table structure for `wf_nodeflow`
-- ----------------------------
DROP TABLE IF EXISTS `wf_nodeflow`;
CREATE TABLE `wf_nodeflow` (
  `FK_Node` int(11) NOT NULL COMMENT '节点 - 主键',
  `FK_Flow` varchar(100) NOT NULL COMMENT '子流程,主外键:对应物理表:WF_Flow,表描述:流程',
  PRIMARY KEY (`FK_Node`,`FK_Flow`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='节点调用子流程';

-- ----------------------------
-- Records of wf_nodeflow
-- ----------------------------

-- ----------------------------
-- Table structure for `wf_nodereturn`
-- ----------------------------
DROP TABLE IF EXISTS `wf_nodereturn`;
CREATE TABLE `wf_nodereturn` (
  `FK_Node` int(11) NOT NULL COMMENT '节点 - 主键',
  `ReturnTo` int(11) NOT NULL COMMENT '退回到 - 主键',
  `Dots` varchar(300) DEFAULT NULL COMMENT '轨迹信息',
  PRIMARY KEY (`FK_Node`,`ReturnTo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='可退回的节点';

-- ----------------------------
-- Records of wf_nodereturn
-- ----------------------------

-- ----------------------------
-- Table structure for `wf_nodestation`
-- ----------------------------
DROP TABLE IF EXISTS `wf_nodestation`;
CREATE TABLE `wf_nodestation` (
  `FK_Node` int(11) NOT NULL COMMENT '节点 - 主键',
  `FK_Station` varchar(100) NOT NULL COMMENT '工作岗位,主外键:对应物理表:Port_Station,表描述:岗位',
  PRIMARY KEY (`FK_Node`,`FK_Station`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='节点岗位';

-- ----------------------------
-- Records of wf_nodestation
-- ----------------------------

-- ----------------------------
-- Table structure for `wf_nodesubflow`
-- ----------------------------
DROP TABLE IF EXISTS `wf_nodesubflow`;
CREATE TABLE `wf_nodesubflow` (
  `OID` int(11) NOT NULL COMMENT 'OID - 主键',
  `FK_Node` int(11) DEFAULT NULL COMMENT '节点',
  `FK_Flow` varchar(100) DEFAULT NULL COMMENT '延续子流程,外键:对应物理表:WF_Flow,表描述:流程',
  `Idx` int(11) DEFAULT NULL COMMENT '显示顺序',
  `ExpType` int(11) DEFAULT NULL COMMENT ' 表达式类型,枚举类型:3 按照SQL计算 4 按照参数计算',
  `CondExp` varchar(500) DEFAULT NULL COMMENT '条件表达式',
  `YBFlowReturnRole` int(11) DEFAULT NULL COMMENT '    退回方式,枚举类型:0 不能退回 1 退回到父流程的开始节点 2 退回到父流程的任何节点 3 退回父流程的启动节点 4 可退回到指定的节点',
  `ReturnToNode` varchar(50) DEFAULT NULL COMMENT '要退回的节点',
  `ReturnToNodeText` varchar(50) DEFAULT NULL COMMENT '要退回的节点',
  PRIMARY KEY (`OID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='延续子流程';

-- ----------------------------
-- Records of wf_nodesubflow
-- ----------------------------

-- ----------------------------
-- Table structure for `wf_nodetoolbar`
-- ----------------------------
DROP TABLE IF EXISTS `wf_nodetoolbar`;
CREATE TABLE `wf_nodetoolbar` (
  `OID` int(11) NOT NULL COMMENT 'OID - 主键',
  `Title` varchar(100) DEFAULT NULL COMMENT '标题',
  `Target` varchar(100) DEFAULT NULL COMMENT '目标',
  `Url` varchar(500) DEFAULT NULL COMMENT '连接',
  `ShowWhere` int(11) DEFAULT NULL COMMENT ' 显示位置,枚举类型:0 树形表单 1 工具栏',
  `Idx` int(11) DEFAULT NULL COMMENT '显示顺序',
  `FK_Node` int(11) DEFAULT NULL COMMENT '节点',
  `MyFileName` varchar(100) DEFAULT NULL COMMENT '图标',
  `MyFilePath` varchar(100) DEFAULT NULL COMMENT 'MyFilePath',
  `MyFileExt` varchar(10) DEFAULT NULL COMMENT 'MyFileExt',
  `WebPath` varchar(200) DEFAULT NULL COMMENT 'WebPath',
  `MyFileH` int(11) DEFAULT NULL COMMENT 'MyFileH',
  `MyFileW` int(11) DEFAULT NULL COMMENT 'MyFileW',
  `MyFileSize` float DEFAULT NULL COMMENT 'MyFileSize',
  PRIMARY KEY (`OID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='自定义工具栏';

-- ----------------------------
-- Records of wf_nodetoolbar
-- ----------------------------

-- ----------------------------
-- Table structure for `wf_pushmsg`
-- ----------------------------
DROP TABLE IF EXISTS `wf_pushmsg`;
CREATE TABLE `wf_pushmsg` (
  `MyPK` varchar(100) NOT NULL COMMENT 'MyPK - 主键',
  `FK_Node` int(11) DEFAULT NULL COMMENT '节点',
  `FK_Event` varchar(15) DEFAULT NULL COMMENT '事件类型',
  `PushWay` int(11) DEFAULT NULL COMMENT '     推送方式,枚举类型:0 按照指定节点的工作人员 1 按照指定的工作人员 2 按照指定的工作岗位 3 按照指定的部门 4 按照指定的SQL 5 按照系统指定的字段',
  `PushDoc` text COMMENT '推送保存内容',
  `Tag` varchar(500) DEFAULT NULL COMMENT 'Tag',
  `SMSPushWay` int(11) DEFAULT NULL COMMENT '短信发送方式',
  `SMSField` varchar(100) DEFAULT NULL COMMENT '短信字段',
  `SMSDoc` text COMMENT '短信内容模版',
  `MailPushWay` int(11) DEFAULT NULL COMMENT '邮件发送方式',
  `MailAddress` varchar(100) DEFAULT NULL COMMENT '邮件字段',
  `MailTitle` varchar(200) DEFAULT NULL COMMENT '邮件标题模版',
  `MailDoc` text COMMENT '邮件内容模版',
  `FK_Flow` varchar(3) DEFAULT NULL COMMENT '流程',
  `SMSNodes` varchar(100) DEFAULT NULL COMMENT 'SMS节点s',
  `MailNodes` varchar(100) DEFAULT NULL COMMENT 'Mail节点s',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='消息推送';

-- ----------------------------
-- Records of wf_pushmsg
-- ----------------------------

-- ----------------------------
-- Table structure for `wf_rememberme`
-- ----------------------------
DROP TABLE IF EXISTS `wf_rememberme`;
CREATE TABLE `wf_rememberme` (
  `MyPK` varchar(100) NOT NULL COMMENT 'MyPK - 主键',
  `FK_Node` int(11) DEFAULT NULL COMMENT '节点',
  `FK_Emp` varchar(30) DEFAULT NULL COMMENT '当前操作人员',
  `Objs` text COMMENT '分配人员',
  `ObjsExt` text COMMENT '分配人员Ext',
  `Emps` text COMMENT '所有的工作人员',
  `EmpsExt` text COMMENT '工作人员Ext',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='记忆我';

-- ----------------------------
-- Records of wf_rememberme
-- ----------------------------

-- ----------------------------
-- Table structure for `wf_returnwork`
-- ----------------------------
DROP TABLE IF EXISTS `wf_returnwork`;
CREATE TABLE `wf_returnwork` (
  `MyPK` varchar(100) NOT NULL COMMENT 'MyPK - 主键',
  `WorkID` int(11) DEFAULT NULL COMMENT 'WorkID',
  `ReturnNode` int(11) DEFAULT NULL COMMENT '退回节点',
  `ReturnNodeName` varchar(200) DEFAULT NULL COMMENT '退回节点名称',
  `Returner` varchar(20) DEFAULT NULL COMMENT '退回人',
  `ReturnerName` varchar(200) DEFAULT NULL COMMENT '退回人名称',
  `ReturnToNode` int(11) DEFAULT NULL COMMENT 'ReturnToNode',
  `ReturnToEmp` text COMMENT '退回给',
  `Note` text COMMENT '退回原因',
  `RDT` varchar(50) DEFAULT NULL COMMENT '退回日期',
  `IsBackTracking` int(11) DEFAULT NULL COMMENT '是否要原路返回?',
  `BeiZhu` text COMMENT '退回原因',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='退回轨迹';

-- ----------------------------
-- Records of wf_returnwork
-- ----------------------------

-- ----------------------------
-- Table structure for `wf_selectaccper`
-- ----------------------------
DROP TABLE IF EXISTS `wf_selectaccper`;
CREATE TABLE `wf_selectaccper` (
  `MyPK` varchar(100) NOT NULL COMMENT 'MyPK - 主键',
  `FK_Node` int(11) DEFAULT NULL COMMENT '接受人节点',
  `WorkID` int(11) DEFAULT NULL COMMENT 'WorkID',
  `FK_Emp` varchar(20) DEFAULT NULL COMMENT 'FK_Emp',
  `EmpName` varchar(20) DEFAULT NULL COMMENT 'EmpName',
  `DeptName` varchar(400) DEFAULT NULL COMMENT '部门名称',
  `AccType` int(11) DEFAULT NULL COMMENT '类型(@0=接受人@1=抄送人)',
  `Rec` varchar(20) DEFAULT NULL COMMENT '记录人',
  `Info` varchar(200) DEFAULT NULL COMMENT '办理意见信息',
  `IsRemember` int(11) DEFAULT NULL COMMENT '以后发送是否按本次计算',
  `Idx` int(11) DEFAULT NULL COMMENT '顺序号(可以用于流程队列审核模式)',
  `Tag` varchar(200) DEFAULT NULL COMMENT '维度信息Tag',
  `TimeLimit` int(11) DEFAULT NULL COMMENT '时限-天',
  `TSpanHour` float DEFAULT NULL COMMENT '时限-小时',
  `ADT` varchar(50) DEFAULT NULL COMMENT '到达日期(计划)',
  `SDT` varchar(50) DEFAULT NULL COMMENT '应完成日期(计划)',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='选择接受/抄送人信息';

-- ----------------------------
-- Records of wf_selectaccper
-- ----------------------------

-- ----------------------------
-- Table structure for `wf_selectinfo`
-- ----------------------------
DROP TABLE IF EXISTS `wf_selectinfo`;
CREATE TABLE `wf_selectinfo` (
  `MyPK` varchar(100) NOT NULL COMMENT 'MyPK - 主键',
  `AcceptNodeID` int(11) DEFAULT NULL COMMENT '接受节点',
  `WorkID` int(11) DEFAULT NULL COMMENT '工作ID',
  `InfoLeft` varchar(200) DEFAULT NULL COMMENT 'InfoLeft',
  `InfoCenter` varchar(200) DEFAULT NULL COMMENT 'InfoCenter',
  `InfoRight` varchar(200) DEFAULT NULL COMMENT 'InfoLeft',
  `AccType` int(11) DEFAULT NULL COMMENT '类型(@0=接受人@1=抄送人)',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='选择接受/抄送人节点信息';

-- ----------------------------
-- Records of wf_selectinfo
-- ----------------------------

-- ----------------------------
-- Table structure for `wf_shiftwork`
-- ----------------------------
DROP TABLE IF EXISTS `wf_shiftwork`;
CREATE TABLE `wf_shiftwork` (
  `MyPK` varchar(100) NOT NULL COMMENT 'MyPK - 主键',
  `WorkID` int(11) DEFAULT NULL COMMENT '工作ID',
  `FK_Node` int(11) DEFAULT NULL COMMENT 'FK_Node',
  `FK_Emp` varchar(40) DEFAULT NULL COMMENT '移交人',
  `FK_EmpName` varchar(40) DEFAULT NULL COMMENT '移交人名称',
  `ToEmp` varchar(40) DEFAULT NULL COMMENT '移交给',
  `ToEmpName` varchar(40) DEFAULT NULL COMMENT '移交给名称',
  `RDT` varchar(50) DEFAULT NULL COMMENT '移交时间',
  `Note` varchar(2000) DEFAULT NULL COMMENT '移交原因',
  `IsRead` int(11) DEFAULT NULL COMMENT '是否读取？',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='移交记录';

-- ----------------------------
-- Records of wf_shiftwork
-- ----------------------------

-- ----------------------------
-- Table structure for `wf_sqltemplate`
-- ----------------------------
DROP TABLE IF EXISTS `wf_sqltemplate`;
CREATE TABLE `wf_sqltemplate` (
  `No` varchar(3) NOT NULL COMMENT '编号 - 主键',
  `SQLType` int(11) DEFAULT NULL COMMENT '     模版SQL类型,枚举类型:0 方向条件 1 接受人规则 2 下拉框数据过滤 3 级联下拉框 4 PopVal开窗返回值 5 人员选择器人员选择范围',
  `Name` varchar(200) DEFAULT NULL COMMENT 'SQL说明',
  `Docs` text COMMENT 'SQL模版',
  PRIMARY KEY (`No`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='SQL模板';

-- ----------------------------
-- Records of wf_sqltemplate
-- ----------------------------

-- ----------------------------
-- Table structure for `wf_task`
-- ----------------------------
DROP TABLE IF EXISTS `wf_task`;
CREATE TABLE `wf_task` (
  `MyPK` varchar(100) NOT NULL COMMENT 'MyPK - 主键',
  `FK_Flow` varchar(200) DEFAULT NULL COMMENT '流程编号',
  `Starter` varchar(200) DEFAULT NULL COMMENT '发起人',
  `Paras` text COMMENT '参数',
  `TaskSta` int(11) DEFAULT NULL COMMENT '任务状态',
  `Msg` text COMMENT '消息',
  `StartDT` varchar(20) DEFAULT NULL COMMENT '发起时间',
  `RDT` varchar(20) DEFAULT NULL COMMENT '插入数据时间',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='任务';

-- ----------------------------
-- Records of wf_task
-- ----------------------------

-- ----------------------------
-- Table structure for `wf_track`
-- ----------------------------
DROP TABLE IF EXISTS `wf_track`;
CREATE TABLE `wf_track` (
  `MyPK` int(11) NOT NULL COMMENT 'MyPK - 主键',
  `ActionType` int(11) DEFAULT NULL COMMENT '类型',
  `ActionTypeText` varchar(30) DEFAULT NULL COMMENT '类型(名称)',
  `FID` int(11) DEFAULT NULL COMMENT '流程ID',
  `WorkID` int(11) DEFAULT NULL COMMENT '工作ID',
  `NDFrom` int(11) DEFAULT NULL COMMENT '从节点',
  `NDFromT` varchar(300) DEFAULT NULL COMMENT '从节点(名称)',
  `NDTo` int(11) DEFAULT NULL COMMENT '到节点',
  `NDToT` varchar(999) DEFAULT NULL COMMENT '到节点(名称)',
  `EmpFrom` varchar(20) DEFAULT NULL COMMENT '从人员',
  `EmpFromT` varchar(30) DEFAULT NULL COMMENT '从人员(名称)',
  `EmpTo` varchar(2000) DEFAULT NULL COMMENT '到人员',
  `EmpToT` varchar(2000) DEFAULT NULL COMMENT '到人员(名称)',
  `RDT` varchar(20) DEFAULT NULL COMMENT '日期',
  `WorkTimeSpan` float DEFAULT NULL COMMENT '时间跨度(天)',
  `Msg` text COMMENT '消息',
  `NodeData` text COMMENT '节点数据(日志信息)',
  `Tag` varchar(300) DEFAULT NULL COMMENT '参数',
  `Exer` varchar(200) DEFAULT NULL COMMENT '执行人',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='轨迹表';

-- ----------------------------
-- Records of wf_track
-- ----------------------------

-- ----------------------------
-- Table structure for `wf_transfercustom`
-- ----------------------------
DROP TABLE IF EXISTS `wf_transfercustom`;
CREATE TABLE `wf_transfercustom` (
  `MyPK` varchar(100) NOT NULL COMMENT 'MyPK - 主键',
  `WorkID` int(11) DEFAULT NULL COMMENT 'WorkID',
  `FK_Node` int(11) DEFAULT NULL COMMENT '节点ID',
  `Worker` varchar(200) DEFAULT NULL COMMENT '处理人(多个人用逗号分开)',
  `WorkerName` varchar(200) DEFAULT NULL COMMENT '处理人(多个人用逗号分开)',
  `SubFlowNo` varchar(3) DEFAULT NULL COMMENT '要经过的子流程编号',
  `PlanDT` varchar(50) DEFAULT NULL COMMENT '计划完成日期',
  `Idx` int(11) DEFAULT NULL COMMENT '顺序号',
  `TodolistModel` int(11) DEFAULT NULL COMMENT '多人工作处理模式',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='自定义运行路径';

-- ----------------------------
-- Records of wf_transfercustom
-- ----------------------------

-- ----------------------------
-- Table structure for `wf_turnto`
-- ----------------------------
DROP TABLE IF EXISTS `wf_turnto`;
CREATE TABLE `wf_turnto` (
  `MyPK` varchar(100) NOT NULL COMMENT 'MyPK - 主键',
  `TurnToType` int(11) DEFAULT NULL COMMENT '条件类型',
  `FK_Flow` varchar(60) DEFAULT NULL COMMENT '流程',
  `FK_Node` int(11) DEFAULT NULL COMMENT '节点ID',
  `FK_Attr` varchar(80) DEFAULT NULL COMMENT '属性外键Sys_MapAttr',
  `AttrKey` varchar(80) DEFAULT NULL COMMENT '键值',
  `AttrT` varchar(80) DEFAULT NULL COMMENT '属性名称',
  `FK_Operator` varchar(60) DEFAULT NULL COMMENT '运算符号',
  `OperatorValue` varchar(60) DEFAULT NULL COMMENT '要运算的值',
  `OperatorValueT` varchar(60) DEFAULT NULL COMMENT '要运算的值T',
  `TurnToURL` varchar(700) DEFAULT NULL COMMENT '要转向的URL',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='转向条件';

-- ----------------------------
-- Records of wf_turnto
-- ----------------------------

-- ----------------------------
-- Table structure for `wf_workflowdeletelog`
-- ----------------------------
DROP TABLE IF EXISTS `wf_workflowdeletelog`;
CREATE TABLE `wf_workflowdeletelog` (
  `OID` int(11) NOT NULL COMMENT 'OID - 主键',
  `FID` int(11) DEFAULT NULL COMMENT 'FID',
  `FK_Dept` varchar(100) DEFAULT NULL COMMENT '部门,外键:对应物理表:Port_Dept,表描述:部门',
  `Title` varchar(100) DEFAULT NULL COMMENT '标题',
  `FlowStarter` varchar(100) DEFAULT NULL COMMENT '发起人',
  `FlowStartRDT` varchar(50) DEFAULT NULL COMMENT '发起时间',
  `FK_NY` varchar(100) DEFAULT NULL COMMENT '年月,外键:对应物理表:Pub_NY,表描述:年月',
  `FK_Flow` varchar(100) DEFAULT NULL COMMENT '流程,外键:对应物理表:WF_Flow,表描述:流程',
  `FlowEnderRDT` varchar(50) DEFAULT NULL COMMENT '最后处理时间',
  `FlowEndNode` int(11) DEFAULT NULL COMMENT '结束节点',
  `FlowDaySpan` int(11) DEFAULT NULL COMMENT '跨度(天)',
  `MyNum` int(11) DEFAULT NULL COMMENT '个数',
  `FlowEmps` varchar(100) DEFAULT NULL COMMENT '参与人',
  `Oper` varchar(20) DEFAULT NULL COMMENT '删除人员',
  `OperDept` varchar(20) DEFAULT NULL COMMENT '删除人员部门',
  `OperDeptName` varchar(200) DEFAULT NULL COMMENT '删除人员名称',
  `DeleteNote` text COMMENT '删除原因',
  `DeleteDT` varchar(50) DEFAULT NULL COMMENT '删除日期',
  PRIMARY KEY (`OID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='流程删除日志';

-- ----------------------------
-- Records of wf_workflowdeletelog
-- ----------------------------

-- ----------------------------
-- View structure for `v_flowstarter`
-- ----------------------------
DROP VIEW IF EXISTS `v_flowstarter`;
CREATE VIEW `v_flowstarter` AS select `a`.`FK_Flow` AS `FK_Flow`,`a`.`FlowName` AS `FlowName`,`c`.`FK_Emp` AS `FK_Emp` from ((`wf_node` `a` join `wf_nodestation` `b`) join `port_empstation` `c`) where ((`a`.`NodePosType` = 0) and ((`a`.`WhoExeIt` = 0) or (`a`.`WhoExeIt` = 2)) and (`a`.`NodeID` = `b`.`FK_Node`) and (`b`.`FK_Station` = `c`.`FK_Station`) and ((`a`.`DeliveryWay` = 0) or (`a`.`DeliveryWay` = 14))) union select `a`.`FK_Flow` AS `FK_Flow`,`a`.`FlowName` AS `FlowName`,`c`.`No` AS `No` from ((`wf_node` `a` join `wf_nodedept` `b`) join `port_emp` `c`) where ((`a`.`NodePosType` = 0) and ((`a`.`WhoExeIt` = 0) or (`a`.`WhoExeIt` = 2)) and (`a`.`NodeID` = `b`.`FK_Node`) and (`b`.`FK_Dept` = `c`.`FK_Dept`) and (`a`.`DeliveryWay` = 1)) union select `a`.`FK_Flow` AS `FK_Flow`,`a`.`FlowName` AS `FlowName`,`b`.`FK_Emp` AS `FK_Emp` from (`wf_node` `a` join `wf_nodeemp` `b`) where ((`a`.`NodePosType` = 0) and ((`a`.`WhoExeIt` = 0) or (`a`.`WhoExeIt` = 2)) and (`a`.`NodeID` = `b`.`FK_Node`) and (`a`.`DeliveryWay` = 3)) union select `a`.`FK_Flow` AS `FK_Flow`,`a`.`FlowName` AS `FlowName`,`b`.`No` AS `FK_Emp` from (`wf_node` `a` join `port_emp` `b`) where ((`a`.`NodePosType` = 0) and ((`a`.`WhoExeIt` = 0) or (`a`.`WhoExeIt` = 2)) and (`a`.`DeliveryWay` = 4)) ;

-- ----------------------------
-- View structure for `v_flowstarterbpm`
-- ----------------------------
DROP VIEW IF EXISTS `v_flowstarterbpm`;
CREATE VIEW `v_flowstarterbpm` AS select `a`.`FK_Flow` AS `FK_Flow`,`a`.`FlowName` AS `FlowName`,`c`.`FK_Emp` AS `FK_Emp` from ((`wf_node` `a` join `wf_nodestation` `b`) join `port_deptempstation` `c`) where ((`a`.`NodePosType` = 0) and ((`a`.`WhoExeIt` = 0) or (`a`.`WhoExeIt` = 2)) and (`a`.`NodeID` = `b`.`FK_Node`) and (`b`.`FK_Station` = `c`.`FK_Station`) and ((`a`.`DeliveryWay` = 0) or (`a`.`DeliveryWay` = 14))) union select `a`.`FK_Flow` AS `FK_Flow`,`a`.`FlowName` AS `FlowName`,`c`.`FK_Emp` AS `FK_Emp` from ((`wf_node` `a` join `wf_nodedept` `b`) join `port_deptemp` `c`) where ((`a`.`NodePosType` = 0) and ((`a`.`WhoExeIt` = 0) or (`a`.`WhoExeIt` = 2)) and (`a`.`NodeID` = `b`.`FK_Node`) and (`b`.`FK_Dept` = `c`.`FK_Dept`) and (`a`.`DeliveryWay` = 1)) union select `a`.`FK_Flow` AS `FK_Flow`,`a`.`FlowName` AS `FlowName`,`b`.`FK_Emp` AS `FK_Emp` from (`wf_node` `a` join `wf_nodeemp` `b`) where ((`a`.`NodePosType` = 0) and ((`a`.`WhoExeIt` = 0) or (`a`.`WhoExeIt` = 2)) and (`a`.`NodeID` = `b`.`FK_Node`) and (`a`.`DeliveryWay` = 3)) union select `a`.`FK_Flow` AS `FK_Flow`,`a`.`FlowName` AS `FlowName`,`b`.`No` AS `FK_Emp` from (`wf_node` `a` join `port_emp` `b`) where ((`a`.`NodePosType` = 0) and ((`a`.`WhoExeIt` = 0) or (`a`.`WhoExeIt` = 2)) and (`a`.`DeliveryWay` = 4)) ;

-- ----------------------------
-- View structure for `v_totalch`
-- ----------------------------
DROP VIEW IF EXISTS `v_totalch`;
CREATE VIEW `v_totalch` AS select `wf_ch`.`FK_Emp` AS `FK_Emp`,(select count(`a`.`MyPK`) AS `Num` from `wf_ch` `a` where (`a`.`FK_Emp` = `wf_ch`.`FK_Emp`)) AS `AllNum`,(select count(`a`.`MyPK`) AS `Num` from `wf_ch` `a` where ((`a`.`CHSta` <= 1) and (`a`.`FK_Emp` = `wf_ch`.`FK_Emp`))) AS `ASNum`,(select count(`a`.`MyPK`) AS `Num` from `wf_ch` `a` where ((`a`.`CHSta` >= 2) and (`a`.`FK_Emp` = `wf_ch`.`FK_Emp`))) AS `CSNum`,(select count(`a`.`MyPK`) AS `Num` from `wf_ch` `a` where ((`a`.`CHSta` = 0) and (`a`.`FK_Emp` = `wf_ch`.`FK_Emp`))) AS `JiShi`,(select count(`a`.`MyPK`) AS `Num` from `wf_ch` `a` where ((`a`.`CHSta` = 1) and (`a`.`FK_Emp` = `wf_ch`.`FK_Emp`))) AS `ANQI`,(select count(`a`.`MyPK`) AS `Num` from `wf_ch` `a` where ((`a`.`CHSta` = 2) and (`a`.`FK_Emp` = `wf_ch`.`FK_Emp`))) AS `YuQi`,(select count(`a`.`MyPK`) AS `Num` from `wf_ch` `a` where ((`a`.`CHSta` = 3) and (`a`.`FK_Emp` = `wf_ch`.`FK_Emp`))) AS `ChaoQi`,round((((select cast(count(`a`.`MyPK`) as decimal(10,0)) AS `Num` from `wf_ch` `a` where ((`a`.`CHSta` <= 1) and (`a`.`FK_Emp` = `wf_ch`.`FK_Emp`))) / (select count(`a`.`MyPK`) AS `Num` from `wf_ch` `a` where (`a`.`FK_Emp` = `wf_ch`.`FK_Emp`))) * 100),2) AS `WCRate` from `wf_ch` group by `wf_ch`.`FK_Emp` ;

-- ----------------------------
-- View structure for `v_totalchweek`
-- ----------------------------
DROP VIEW IF EXISTS `v_totalchweek`;
CREATE VIEW `v_totalchweek` AS select `wf_ch`.`FK_Emp` AS `FK_Emp`,`wf_ch`.`WeekNum` AS `WeekNum`,`wf_ch`.`FK_NY` AS `FK_NY`,(select count(`a`.`MyPK`) AS `Num` from `wf_ch` `a` where ((`a`.`FK_Emp` = `wf_ch`.`FK_Emp`) and (`a`.`WeekNum` = `wf_ch`.`WeekNum`))) AS `AllNum`,(select count(`a`.`MyPK`) AS `Num` from `wf_ch` `a` where ((`a`.`CHSta` <= 1) and (`a`.`FK_Emp` = `wf_ch`.`FK_Emp`) and (`a`.`WeekNum` = `wf_ch`.`WeekNum`))) AS `ASNum`,(select count(`a`.`MyPK`) AS `Num` from `wf_ch` `a` where ((`a`.`CHSta` >= 2) and (`a`.`FK_Emp` = `wf_ch`.`FK_Emp`) and (`a`.`WeekNum` = `wf_ch`.`WeekNum`))) AS `CSNum`,(select count(`a`.`MyPK`) AS `Num` from `wf_ch` `a` where ((`a`.`CHSta` = 0) and (`a`.`FK_Emp` = `wf_ch`.`FK_Emp`) and (`a`.`WeekNum` = `wf_ch`.`WeekNum`))) AS `JiShi`,(select count(`a`.`MyPK`) AS `Num` from `wf_ch` `a` where ((`a`.`CHSta` = 1) and (`a`.`FK_Emp` = `wf_ch`.`FK_Emp`) and (`a`.`WeekNum` = `wf_ch`.`WeekNum`))) AS `AnQi`,(select count(`a`.`MyPK`) AS `Num` from `wf_ch` `a` where ((`a`.`CHSta` = 2) and (`a`.`FK_Emp` = `wf_ch`.`FK_Emp`) and (`a`.`WeekNum` = `wf_ch`.`WeekNum`))) AS `YuQi`,(select count(`a`.`MyPK`) AS `Num` from `wf_ch` `a` where ((`a`.`CHSta` = 3) and (`a`.`FK_Emp` = `wf_ch`.`FK_Emp`) and (`a`.`WeekNum` = `wf_ch`.`WeekNum`))) AS `ChaoQi`,round((((select cast(count(`a`.`MyPK`) as decimal(10,0)) AS `Num` from `wf_ch` `a` where ((`a`.`CHSta` <= 1) and (`a`.`FK_Emp` = `wf_ch`.`FK_Emp`) and (`a`.`WeekNum` = `wf_ch`.`WeekNum`))) / (select count(`a`.`MyPK`) AS `Num` from `wf_ch` `a` where ((`a`.`FK_Emp` = `wf_ch`.`FK_Emp`) and (`a`.`WeekNum` = `wf_ch`.`WeekNum`)))) * 100),2) AS `WCRate` from `wf_ch` group by `wf_ch`.`FK_Emp`,`wf_ch`.`WeekNum`,`wf_ch`.`FK_NY` ;

-- ----------------------------
-- View structure for `v_totalchyf`
-- ----------------------------
DROP VIEW IF EXISTS `v_totalchyf`;
CREATE VIEW `v_totalchyf` AS select `wf_ch`.`FK_Emp` AS `FK_Emp`,`wf_ch`.`FK_NY` AS `FK_NY`,(select count(`a`.`MyPK`) AS `Num` from `wf_ch` `a` where ((`a`.`FK_Emp` = `wf_ch`.`FK_Emp`) and (`a`.`FK_NY` = `wf_ch`.`FK_NY`))) AS `AllNum`,(select count(`a`.`MyPK`) AS `Num` from `wf_ch` `a` where ((`a`.`CHSta` <= 1) and (`a`.`FK_Emp` = `wf_ch`.`FK_Emp`) and (`a`.`FK_NY` = `wf_ch`.`FK_NY`))) AS `ASNum`,(select count(`a`.`MyPK`) AS `Num` from `wf_ch` `a` where ((`a`.`CHSta` >= 2) and (`a`.`FK_Emp` = `wf_ch`.`FK_Emp`) and (`a`.`FK_NY` = `wf_ch`.`FK_NY`))) AS `CSNum`,(select count(`a`.`MyPK`) AS `Num` from `wf_ch` `a` where ((`a`.`CHSta` = 0) and (`a`.`FK_Emp` = `wf_ch`.`FK_Emp`) and (`a`.`FK_NY` = `wf_ch`.`FK_NY`))) AS `JiShi`,(select count(`a`.`MyPK`) AS `Num` from `wf_ch` `a` where ((`a`.`CHSta` = 1) and (`a`.`FK_Emp` = `wf_ch`.`FK_Emp`) and (`a`.`FK_NY` = `wf_ch`.`FK_NY`))) AS `AnQi`,(select count(`a`.`MyPK`) AS `Num` from `wf_ch` `a` where ((`a`.`CHSta` = 2) and (`a`.`FK_Emp` = `wf_ch`.`FK_Emp`) and (`a`.`FK_NY` = `wf_ch`.`FK_NY`))) AS `YuQi`,(select count(`a`.`MyPK`) AS `Num` from `wf_ch` `a` where ((`a`.`CHSta` = 3) and (`a`.`FK_Emp` = `wf_ch`.`FK_Emp`) and (`a`.`FK_NY` = `wf_ch`.`FK_NY`))) AS `ChaoQi`,round((((select cast(count(`a`.`MyPK`) as decimal(10,0)) AS `Num` from `wf_ch` `a` where ((`a`.`CHSta` <= 1) and (`a`.`FK_Emp` = `wf_ch`.`FK_Emp`) and (`a`.`FK_NY` = `wf_ch`.`FK_NY`))) / (select count(`a`.`MyPK`) AS `Num` from `wf_ch` `a` where ((`a`.`FK_Emp` = `wf_ch`.`FK_Emp`) and (`a`.`FK_NY` = `wf_ch`.`FK_NY`)))) * 100),2) AS `WCRate` from `wf_ch` group by `wf_ch`.`FK_Emp`,`wf_ch`.`FK_NY` ;

-- ----------------------------
-- View structure for `wf_empworks`
-- ----------------------------
DROP VIEW IF EXISTS `wf_empworks`;
CREATE VIEW `wf_empworks` AS select `a`.`PRI` AS `PRI`,`a`.`WorkID` AS `WorkID`,`b`.`IsRead` AS `IsRead`,`a`.`Starter` AS `Starter`,`a`.`StarterName` AS `StarterName`,`a`.`WFState` AS `WFState`,`a`.`FK_Dept` AS `FK_Dept`,`a`.`DeptName` AS `DeptName`,`a`.`FK_Flow` AS `FK_Flow`,`a`.`FlowName` AS `FlowName`,`a`.`PWorkID` AS `PWorkID`,`a`.`PFlowNo` AS `PFlowNo`,`b`.`FK_Node` AS `FK_Node`,`b`.`FK_NodeText` AS `NodeName`,`b`.`FK_Dept` AS `WorkerDept`,`a`.`Title` AS `Title`,`a`.`RDT` AS `RDT`,`b`.`RDT` AS `ADT`,`b`.`SDT` AS `SDT`,`b`.`FK_Emp` AS `FK_Emp`,`b`.`FID` AS `FID`,`a`.`FK_FlowSort` AS `FK_FlowSort`,`a`.`SysType` AS `SysType`,`a`.`SDTOfNode` AS `SDTOfNode`,`b`.`PressTimes` AS `PressTimes`,`a`.`GuestNo` AS `GuestNo`,`a`.`GuestName` AS `GuestName`,`a`.`BillNo` AS `BillNo`,`a`.`FlowNote` AS `FlowNote`,`a`.`TodoEmps` AS `TodoEmps`,`a`.`TodoEmpsNum` AS `TodoEmpsNum`,`a`.`TodoSta` AS `TodoSta`,`a`.`TaskSta` AS `TaskSta`,0 AS `ListType`,`a`.`Sender` AS `Sender`,`a`.`AtPara` AS `AtPara`,1 AS `MyNum` from (`wf_generworkflow` `a` join `wf_generworkerlist` `b`) where ((`b`.`IsEnable` = 1) and (`b`.`IsPass` = 0) and (`a`.`WorkID` = `b`.`WorkID`) and (`a`.`FK_Node` = `b`.`FK_Node`) and (`a`.`WFState` <> 0)) union select `a`.`PRI` AS `PRI`,`a`.`WorkID` AS `WorkID`,`b`.`Sta` AS `IsRead`,`a`.`Starter` AS `Starter`,`a`.`StarterName` AS `StarterName`,2 AS `WFState`,`a`.`FK_Dept` AS `FK_Dept`,`a`.`DeptName` AS `DeptName`,`a`.`FK_Flow` AS `FK_Flow`,`a`.`FlowName` AS `FlowName`,`a`.`PWorkID` AS `PWorkID`,`a`.`PFlowNo` AS `PFlowNo`,`b`.`FK_Node` AS `FK_Node`,`b`.`NodeName` AS `NodeName`,`b`.`CCToDept` AS `WorkerDept`,`a`.`Title` AS `Title`,`a`.`RDT` AS `RDT`,`b`.`RDT` AS `ADT`,`b`.`RDT` AS `SDT`,`b`.`CCTo` AS `FK_Emp`,`b`.`FID` AS `FID`,`a`.`FK_FlowSort` AS `FK_FlowSort`,`a`.`SysType` AS `SysType`,`a`.`SDTOfNode` AS `SDTOfNode`,0 AS `PressTimes`,`a`.`GuestNo` AS `GuestNo`,`a`.`GuestName` AS `GuestName`,`a`.`BillNo` AS `BillNo`,`a`.`FlowNote` AS `FlowNote`,`a`.`TodoEmps` AS `TodoEmps`,`a`.`TodoEmpsNum` AS `TodoEmpsNum`,0 AS `TodoSta`,0 AS `TaskSta`,1 AS `ListType`,`b`.`Rec` AS `Sender`,('@IsCC=1' or `a`.`AtPara`) AS `AtPara`,1 AS `MyNum` from (`wf_generworkflow` `a` join `wf_cclist` `b`) where ((`a`.`WorkID` = `b`.`WorkID`) and (`b`.`Sta` <= 1) and (`b`.`InEmpWorks` = 1) and (`a`.`WFState` <> 0)) ;
